﻿#include "stdafx.h"
#include "MBsoftware.h"




//include za media timer ( multimedia timer )
#include<mmsystem.h>
#pragma comment(lib, "winmm.lib")
     
MBsoftware*									MBsoftware::glob;
std::vector<CCamera*>						MBsoftware::cam;
std::vector<CMemoryBuffer*>					MBsoftware::images;
std::vector<Serial*>						MBsoftware::serial;
std::vector<SMCdrive*>						MBsoftware::smc;
std::vector<ControlCardMB*>					MBsoftware::controlCardMB;
std::vector<Pcie*>							MBsoftware::pcieDio;
std::vector<Types*>							MBsoftware::types[NR_STATIONS];
std::vector<ControlLPT*>					MBsoftware::lpt;
std::vector<TCP*>							MBsoftware::tcp;
History*									MBsoftware::history;
std::vector<StatusBox*>						MBsoftware::statusBox;
std::vector<LogWindow*>						MBsoftware::log;
Timer										MBsoftware::MMTimer;
Timer										MBsoftware::viewTimerTimer;
Timer										MBsoftware::viewTimerTimer2;
std::vector<Timer*>							MBsoftware::TestFun;
int											MBsoftware::mmCounter;
int											MBsoftware::viewCounter;
imageProcessing*							MBsoftware::imageProcess;
LoginWindow*								MBsoftware::login;
AboutUS*										MBsoftware::about;
int counter = 0;
int											MBsoftware::currentType = 0;
int											MBsoftware::prevType = 0;
int											MBsoftware::obdelanaStevec = 0;

Measurand									MBsoftware::measureObject; //object with mesurand parameters
GeneralSettings*							MBsoftware::settings; //generalSettings


QTcpSocket*									MBsoftware::client;
QFuture<int>								MBsoftware::future[4];

vector<int>						MBsoftware::clearLogIndex;
vector<int>						MBsoftware::logIndex;
vector<QString>						MBsoftware::logText;
QReadWriteLock					MBsoftware::lockReadWrite;
//QSerialPort*					serial;
MMRESULT						FTimerID;


void MBsoftware::closeEvent(QCloseEvent *event)
{

	disconnect(this, 0, 0, 0);

	//first you have to close all timers
	timeKillEvent(FTimerID);
	timeEndPeriod(1);
	OnTimer->stop();

	//ales novi test
	
	for (int i = 0; i < cam.size(); i++)
	{
		delete (cam[i]);
	}
	
	for (int i = 0; i < images.size(); i++)
	{
		delete (images[i]);
	}
	for (int i = 0; i < TestFun.size(); i++)
	{
		delete (TestFun[i]);
	}
	//lpt.clear();
		for (int i = 0; i < lpt.size(); i++)
		{
			lpt[i]->SetControlByteAllOFF();
			lpt[i]->SetDataByte(0x00);
			lpt[i]->close();

			//delete (lpt[i]);
		}
		lpt.clear();

		for (int i = 0; i < controlCardMB.size(); i++)
		{
			controlCardMB[i]->StopTimer(0);
			controlCardMB[i]->StopTimer(2);
			controlCardMB[i]->StopTimer(3);
			controlCardMB[i]->StopTimer(1);
			controlCardMB[i]->SetOutputByte(0, 0x00);
			controlCardMB[i]->SetOutputByte(1, 0x00);

			delete (controlCardMB[i]);
		}
		controlCardMB.clear();

		for (int i = 0; i < serial.size(); i++)
		{
			delete (serial[i]);
		}
		serial.clear();

		for (int i = 0; i < statusBox.size(); i++)
		{
			delete (statusBox[i]);
		}
		for (int i = 0; i < smc.size(); i++)
		{
			delete (smc[i]);
		}
		for (int i = 0; i < pcieDio.size(); i++)
		{
			delete (pcieDio[i]);
		}
		for (int i = 0; i < tcp.size(); i++)
		{
			delete (tcp[i]);
		}

		for (int i = 0; i < NR_STATIONS; i++) //izbrisemo kar je bilo prikazano na zaslonu 
		{
			qDeleteAll(showScene[i]->items());
			delete showScene[i];
		}
		//showScene.clear();
		for (int i = 0; i < NR_STATIONS; i++)
		{
			delete parametersTable[i];
			for (int j = 0; j < types[i].size(); j++)
			{
				delete types[i][j];
				
			}
		}
		//log[0]->AppendMessage("Application STOP.");

		for (int i = 0; i < log.size(); i++)
		{
			delete log[i];
		}
		log.clear();

		AddLog("Application stoped", 3);
		logText.clear();
		logIndex.clear();
		delete history;
		delete imageProcess;
		delete login;
		delete settings;
		delete about;
		
		

}

MBsoftware::MBsoftware(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	glob = this;


	QThread::currentThread()->setPriority(QThread::HighPriority);
	SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);


	ReadTypes();
	LoadVariables();
	
	about = new AboutUS();
	login = new LoginWindow();
	settings = new GeneralSettings();
	statusBox.push_back(new StatusBox);
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());
	TestFun.push_back(new Timer());

#if defined _DEBUG
	login->currentRights = 1;
	login->currentUser = "Administrator";
#else
	login->currentRights = 0;
	login->currentUser = "";
#endif



	AddLog("Application STARTED!", 3);
	isSceneAdded = false;


	int visina = 540;
	int sirina = 720;
	int numImages = 20;
	cam.push_back(new ImagingSource());
	cam.back()->serialNumber = "20910619";
	cam.back()->width = sirina;
	cam.back()->height = visina;

	cam.back()->depth = 1;
	//cam.back()->offsetX = 32;
	//cam.back()->offsetY = 340;
	cam.back()->FPS = 200;
	cam.back()->trigger = true;
	cam.back()->realTimeProcessing = true; //true -> kliče  v MBCameraView
	cam.back()->triggerOutput = 1;
	cam.back()->rotadedHorizontal = true;
	cam.back()->rotatedVertical = false;
	cam.back()->num_images = numImages;
	cam.back()->LoadReferenceSettings();
	QString videoFormat = QString("Y800 (%1x%2)").arg(cam.back()->width).arg(cam.back()->height);

	if (cam.back()->Init(cam.back()->serialNumber, videoFormat) == 1)
	{
		if (cam.back()->Start())
		{
			cam.back()->SetExposureAbsolute(cam.back()->exposure[0]);
			cam.back()->SetGainAbsolute(cam.back()->gain[0]);
			cam.back()->SetPartialOffsetX(cam.back()->savedOffsetX[0]);
			cam.back()->SetPartialOffsetY(cam.back()->savedOffsetY[0]);
			cam.back()->EnableStrobe(0);
		}
	}
	if (!cam.back()->isLive) //ce smo v testnem nacinu in kamera nif odprta, inicializiramo vse slike
		cam.back()->InitImages(cam.back()->width, cam.back()->height, cam.back()->depth);

	//kamera 1
	cam.push_back(new ImagingSource());
	cam.back()->serialNumber = "20910614";
	cam.back()->width = sirina;
	cam.back()->height = visina;

	cam.back()->depth = 1;
	//cam.back()->offsetX = 32;
	//cam.back()->offsetY = 340;
	cam.back()->FPS = 200;
	cam.back()->trigger = true;
	cam.back()->realTimeProcessing = true; //true -> kliče  v MBCameraView
	cam.back()->triggerOutput = 1;
	cam.back()->rotadedHorizontal = true;
	cam.back()->rotatedVertical = true;
	cam.back()->num_images = numImages;
	cam.back()->LoadReferenceSettings();
	videoFormat = QString("Y800 (%1x%2)").arg(cam.back()->width).arg(cam.back()->height);

	if (cam.back()->Init(cam.back()->serialNumber, videoFormat) == 1)
	{
		if (cam.back()->Start())
		{
			cam.back()->SetExposureAbsolute(cam.back()->exposure[0]);
			cam.back()->SetGainAbsolute(cam.back()->gain[0]);
			cam.back()->SetPartialOffsetX(cam.back()->savedOffsetX[0]);
			cam.back()->SetPartialOffsetY(cam.back()->savedOffsetY[0]);
			cam.back()->EnableStrobe(0);
		}
	}
	if (!cam.back()->isLive) //ce smo v testnem nacinu in kamera ni odprta, inicializiramo vse slike
		cam.back()->InitImages(cam.back()->width, cam.back()->height, cam.back()->depth);
	QString tmp;
	for (int i = 0; i < 18; i++)
	{
		tmp = QString("C:\\images\\%1.bmp").arg(i);
		cam[1]->image[i].LoadBuffer(tmp);
		cam[0]->image[i].LoadBuffer(tmp);
		//cam[0]->imageReady[i] = 1;
	}
	

	
	for (int i = 0; i < 10; i++)
	{
		//tmp = QString("C:\\images\\eti\\%1.bmp").arg(i);
		images.push_back(new CMemoryBuffer(sirina, visina, 1));
		//images[i]->LoadBuffer(tmp);
	}

	


	//images[0]->LoadBuffer(tmp);

	imageProcess = new imageProcessing();
	imageProcess->ConnectImages(cam, images);
	imageProcess->ConnectMeasurand(&measureObject);


	for (int i = 0; i < NR_STATIONS; i++)
	{
		imageProcess->ConnectTypes(i,types[i]);
	}
	imageProcess->LoadTypesProperty();//nalozimo nastavitve funkcij za vse tipe



	//timer za viewDisplay
	OnTimer = new QTimer(this);


	//dinamično dodajanje ikon v tabih
	CreateCameraTab();
	CreateSignalsTab();
	CreateMeasurementsTable();


	//Povezave SIGNAL-SLOT
	connect(OnTimer, SIGNAL(timeout()), this, SLOT(ViewTimer()));
	OnTimer->start(100);

	for (int i = 0; i < cam.size(); i++)
	{
		connect(cam[i], SIGNAL(frameReadySignal(int, int)), this, SLOT(OnFrameReady(int, int)));
	}

	connect(imageProcess, SIGNAL(imagesReady()), this, SLOT(ProcessImages()));
	connect(ui.buttonImageProcessing, SIGNAL(pressed()), this, SLOT(OnClickedShowImageProcessing()));
	connect(ui.buttonTypeSelect, SIGNAL(pressed()), this, SLOT(OnClickedSelectType()));
	connect(ui.buttonSetTolerance, SIGNAL(pressed()), this, SLOT(OnClickedEditTypes()));
	connect(ui.buttonAddType, SIGNAL(pressed()), this, SLOT(OnClickedAddTypes()));
	//connect(ui.buttonTeachType, SIGNAL(pressed()), this, SLOT(OnClickedTeachType()));
	connect(ui.buttonRemoveType, SIGNAL(pressed()), this, SLOT(OnClickedRemoveTypes()));
	connect(ui.buttonResetCounters, SIGNAL(pressed()), this, SLOT(OnClickedResetCounters()));
	connect(ui.buttonLogin, SIGNAL(pressed()), this, SLOT(OnClickedLogin()));
	connect(ui.buttonStatusBox, SIGNAL(pressed()), this, SLOT(OnClickedStatusBox()));
	connect(ui.buttonHistory, SIGNAL(pressed()), this, SLOT(OnClickedShowHistory()));
	connect(ui.buttonGeneralSettings, SIGNAL(pressed()), this, SLOT(OnClickedGeneralSettings()));
	connect(ui.buttonAboutUS, SIGNAL(pressed()), this, SLOT(OnClickedAboutUs()));
	
	


	//connect(ui.buttonSMCdialog, SIGNAL(pressed()), this, SLOT(OnClickedSmcButton()));
	////		MULTIMEDIA	TIMER		////
	UINT uDelay = 1;				// 1 = 1000Hz, 10 = 100Hz
	UINT uResolution = 1;
	DWORD dwUser = NULL;
	UINT fuEvent = TIME_PERIODIC;

	// timeBeginPeriod(1);
	FTimerID = timeSetEvent(uDelay, uResolution, OnMultimediaTimer, dwUser, fuEvent);
	

	DrawImageOnScreen();


	history = new History();
	SetHistory();
	connect(history, SIGNAL(callFromHistory(int , int)), this, SLOT(HistoryCall(int ,int)));

	for (int i = 0; i < 12; i++)
	{
		ui.listWidgetStatus->addItem("");
	}


	grabKeyboard();


}

void MBsoftware::OnFrameReady(int camera, int imageIndex)
{



}

void MBsoftware::OnTcpDataReady(int parse)
{

}

void MBsoftware::HistoryCall(int station, int currentPiece)
{

	if(currentPiece >= 0 )
	*cam[station]->image[0].buffer = history->cam0Image[station][currentPiece].clone();


}






void MBsoftware::MeasurePiece(int index)
{


	
}

void MBsoftware::MeasurePiece(int station, int index)
{

}

void MBsoftware::UpdateHistory(int station, int index)
{
	
		for (int j = 0; j < types[station][currentType]->parameterCounter; j++)
		{
			history->measuredValue[station][history->lastIn[station]][j] = types[station][currentType]->measuredValue[j];
			history->nominal[station][history->lastIn[station]][j] = types[station][currentType]->nominal[j];
			history->toleranceHigh[station][history->lastIn[station]][j] = types[station][currentType]->toleranceHigh[j];
			history->toleranceLow[station][history->lastIn[station]][j] = types[station][currentType]->toleranceLow[j];
			history->isConditional[station][history->lastIn[station]][j] = types[station][currentType]->isConditional[j];
			history->isActive[station][history->lastIn[station]][j] = types[station][currentType]->isActive[j];
			history->name[station][history->lastIn[station]][j] = types[station][currentType]->name[j];
		}
		history->cam0Image[station][history->lastIn[station]] = cam[station]->image[measureObject.imageIndex[station][index]].buffer->clone();



	history->currentPiece[history->lastIn[station]] = index;
	history->UpdateCounter(station);

}

void MBsoftware::SetHistory()
{
	//najprej pobrisemo stare vektorje
	for (int i = 0; i < NR_STATIONS; i++)
	{
		for (int j = 0; j < HISTORY_SIZE; j++)
		{

			history->measuredValue[i][j].clear();
			history->nominal[i][j].clear();
			history->toleranceHigh[i][j].clear();
			history->toleranceLow[i][j].clear();
			history->toleranceLowCond[i][j].clear();
			history->toleranceHighCond[i][j].clear();
			history->name[i][j].clear();
			history->isActive[i][j].clear();
			history->isConditional[i][j].clear();
			history->cam0Image[i].clear();
		}
		history->parametersCounter[i].clear();
	}


	for (int i = 0; i < NR_STATIONS; i++)
	{
		for (int j = 0; j < HISTORY_SIZE; j++)
		{
			for (int k = 0; k < types[i][currentType]->parameterCounter; k++)
			{
				history->measuredValue[i][j].push_back(types[i][currentType]->measuredValue[k]);
				
				history->nominal[i][j].push_back(types[i][currentType]->nominal[k]);
				history->toleranceHigh[i][j].push_back(types[i][currentType]->toleranceHigh[k]);
				history->toleranceLow[i][j].push_back(types[i][currentType]->toleranceLow[k]);
				history->toleranceLowCond[i][j].push_back(types[i][currentType]->toleranceLowCond[k]);
				history->toleranceHighCond[i][j].push_back(types[i][currentType]->toleranceHighCond[k]);
				history->isActive[i][j].push_back(types[i][currentType]->isActive[k]);
				history->isConditional[i][j].push_back(types[i][currentType]->isConditional[k]);
				history->name[i][j].push_back(types[i][currentType]->name[k]);
				

			}
			history->cam0Image[i].push_back(Mat(images[0]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
			//history->cam1Image[i].push_back(Mat(images[1]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
		//	history->cam2Image[i].push_back(Mat(images[2]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
		//	history->cam3Image[i].push_back(Mat(images[3]->buffer->rows, images[0]->buffer->cols, CV_8UC1));
			history->parametersCounter[i].push_back(types[i][currentType]->parameterCounter);
		}
	}
	history->CreateMeasurementsTable();


}


void MBsoftware::OnMultimediaTimer(UINT uTimerID, UINT, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2)
{
	imageProcess->currentType = currentType; //samo zacasno za dodajanje propBrowserja

	if (MMTimer.timeCounter >= 500)
	{
		MMTimer.SetStop();
		MMTimer.CalcFrequency(MMTimer.timeCounter);
		MMTimer.timeCounter = 0;
		MMTimer.SetStart();
	}

	MMTimer.timeCounter++;
	mmCounter++;


	//takt counter 


}



void MBsoftware::CreateCameraTab()
{

//QWidget *cameraWidget;
	QGroupBox *camGroup = new QGroupBox(ui.tabCameras);;
	QHBoxLayout *camLayout = new QHBoxLayout;
	QToolButton *camButton;
	QIcon camIcon;
	QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
	QSignalMapper* signalMapper = new QSignalMapper(this);
	QHBoxLayout *tabLayout = new QHBoxLayout;
	int k = 0;

	//QString fileP = QDir::currentPath() + QString("\\res\\cam%1.bmp").arg(k);
	QImage image;
	/*if (!image.isNull())
		camIcon.addPixmap(QPixmap::fromImage(image));*/

	for (int i = 0; i < cam.size(); i++)
	{
		image.load(QString(QDir::currentPath() + QString("\\res\\cam%1.bmp").arg(i)));
		if (!image.isNull())
			camIcon.addPixmap(QPixmap::fromImage(image));

		camButton = new QToolButton();
		camButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
		
		camButton->setText(QString("CAM%1").arg(i));
		camButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
		camButton->setIcon(camIcon);
		camButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
		camButton->setAutoRaise(true);
		camLayout->addWidget(camButton);
		connect(camButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
		signalMapper->setMapping(camButton, i);
	}


	
	tabLayout->addWidget(camGroup);
	tabLayout->addSpacerItem(spacer);
	ui.tabCameras->setLayout(tabLayout);
	camGroup->setLayout(camLayout);
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowCamButton(int)));

}

void MBsoftware::CreateSignalsTab()
{


	QHBoxLayout *lptLayout = new QHBoxLayout;
	QToolButton *lptButton;
	QHBoxLayout *smartCardLayout = new QHBoxLayout;
	QToolButton *smartCardButton;

	QHBoxLayout *tcpLayout = new QHBoxLayout;
	QToolButton *tcpButton;

	QIcon camIcon;
	QIcon smartIcon;
	QSpacerItem *spacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Expanding);
	QSignalMapper* signalMapper = new QSignalMapper(this);
	QSignalMapper* signalSmartCardMapper = new QSignalMapper(this);
	QHBoxLayout *tabLayout = new QHBoxLayout;
	
	int k = 0;

	
	QImage image;

	if (lpt.size() > 0)
	{
		QGroupBox *lptGroup = new QGroupBox(ui.tabSignals);
		lptGroup->setTitle("LPT");

		image.load(QString(QDir::currentPath() + QString("\\res\\lptIcon.bmp")));

		if (!image.isNull())
			camIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < lpt.size(); i++)
		{

			lptButton = new QToolButton();
			lptButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			lptButton->setText(QString("LPT%1").arg(i));
			lptButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			lptButton->setIcon(camIcon);
			lptButton->setAutoRaise(true);
			lptButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			lptLayout->addWidget(lptButton);
			connect(lptButton, SIGNAL(clicked()), signalMapper, SLOT(map()));
			signalMapper->setMapping(lptButton, i);
		}
		tabLayout->addWidget(lptGroup);

		ui.tabSignals->setLayout(tabLayout);
		lptLayout->setMargin(0);
		lptGroup->setLayout(lptLayout);
		connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowLptButton(int)));
	}
	
	if (controlCardMB.size() > 0)
	{
		QGroupBox *SmartCardGroup = new QGroupBox(ui.tabSignals);
		SmartCardGroup->setTitle("SmartCard");

		image.load(QString(QDir::currentPath() + QString("\\res\\serialPort.bmp")));

		if (!image.isNull())
			smartIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < controlCardMB.size(); i++)
		{
			smartCardButton = new QToolButton();
			smartCardButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			smartCardButton->setText(QString("SmartC%1").arg(i));
			smartCardButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			smartCardButton->setIcon(smartIcon);
			smartCardButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			smartCardButton->setAutoRaise(true);
			smartCardLayout->addWidget(smartCardButton);
			connect(smartCardButton, SIGNAL(clicked()), signalSmartCardMapper, SLOT(map()));
			signalSmartCardMapper->setMapping(smartCardButton, i);
		}

		tabLayout->addWidget(SmartCardGroup);
		smartCardLayout->setMargin(0);
		SmartCardGroup->setLayout(smartCardLayout);
		connect(signalSmartCardMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowSmartCardButton(int)));

	}
	if (tcp.size() > 0)
	{
		QGroupBox *tcpGroup = new QGroupBox(ui.tabSignals);
		tcpGroup->setTitle("TCP");
		image.load(QString(QDir::currentPath() + QString("\\res\\localNetwork.bmp")));

		if (!image.isNull())
			smartIcon.addPixmap(QPixmap::fromImage(image));

		for (int i = 0; i < tcp.size(); i++)
		{
			tcpButton = new QToolButton();
			tcpButton->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
			tcpButton->setText(QString("TCP%1").arg(i));
			tcpButton->setFixedSize(QSize(TAB_BUTTTON_SIZE, TAB_BUTTTON_SIZE));
			tcpButton->setIcon(smartIcon);
			tcpButton->setIconSize(QSize(TAB_ICON_SIZE, TAB_ICON_SIZE));
			tcpButton->setAutoRaise(true);
			tcpLayout->addWidget(tcpButton);

			connect(tcpButton, &QPushButton::clicked,
				[=](int index) { OnClickedTCPconnction(i); });

		}
		tabLayout->addWidget(tcpGroup);
		tcpLayout->setMargin(0);
		tcpGroup->setLayout(tcpLayout);
		//connect(signalSmartCardMapper, SIGNAL(mapped(int)), this, SLOT(OnClickedShowSmartCardButton(int)));
	}


	tabLayout->addSpacerItem(spacer);



}

void MBsoftware::CreateMeasurementsTable()
{
	int height = 0;
	for (int i = 0; i < NR_STATIONS; i++)
	{
		parametersTable[i] = new QTableWidget();



	//measurementsBox

	//parametersTable[0] = new QTableWidget();
	parametersTable[i]->setStyleSheet("QHeaderView::section { background-color:rgb(236, 236, 236); }");
	parametersTable[i]->setMinimumWidth(420);


	height = (types[i][0]->parameterCounter + 1) * 25;
	parametersTable[i]->setMinimumHeight(height + 25);
	parametersTable[i]->setMinimumWidth(420);

	parametersTable[i]->setEditTriggers(QAbstractItemView::NoEditTriggers); //diable edit
	parametersTable[i]->setSelectionMode(QAbstractItemView::NoSelection); //diable edit
	QStringList celice;
	parametersTable[i]->setColumnCount(4);
	celice << "Parameter" << "Measured" << "Min" << "Max";
	parametersTable[i]->setHorizontalHeaderLabels(celice);
	parametersTable[i]->setColumnWidth(0, 160);
	parametersTable[i]->setColumnWidth(1, 78);
	parametersTable[i]->setColumnWidth(2, 78);
	parametersTable[i]->setColumnWidth(3, 78);
	
	parametersTable[i]->setRowCount(types[i][0]->parameterCounter);
	parametersTable[i]->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	if (i == 0)
	{
		ui.frameMeasurements->setMinimumHeight(height + 60); //25 za naslov nad meritvami
		ui.layoutMeasurements->addWidget(parametersTable[0], Qt::AlignTop);
	}
	else if (i == 1)
	{
		//ui.frameMeasurements_2->setMinimumHeight(height + 60); //25 za naslov nad meritvami
		//ui.layoutMeasurements_2->addWidget(parametersTable[1], Qt::AlignTop);
	}
	}

}



void MBsoftware::AddLog(QString text, int type)
{
	logText.push_back(QString("%1:%2:%3 - %4").arg(QTime::currentTime().hour()).arg(QTime::currentTime().minute()).arg(QTime::currentTime().second()).arg(text));
	logIndex.push_back(type);

	EraseLastLOG();
}


void MBsoftware::EraseLastLOG()
{
	if (logIndex.size() > 10)
	{
		logIndex.erase(logIndex.begin());
		logText.erase(logText.begin());
	}
}


void MBsoftware::ReadTypes()
{
	QString filePath;
	QStringList values;
	int count = 0;
	QString typePath;
	QString typeName;
	QDir dir;
	filePath = QDir::currentPath() + QString("/%1/types").arg(REF_FOLDER_NAME);

	if (!QDir(filePath).exists())
		QDir().mkdir(filePath);

	for (int i = 0; i < NR_STATIONS; i++)
	{

		//filePath = "D:\\Git_MBvision\\MBsoftware32bit\\MBsoftware32Bit\\Win32\\Reference\\signali\\ControlCardMB0.ini";
		filePath = QDir::currentPath() + QString("/%1/types/types_Station%2").arg(REF_FOLDER_NAME).arg(i);
		QSettings settings(filePath, QSettings::IniFormat);

		if (!QDir(filePath).exists())
			QDir().mkdir(filePath);




		dir.setPath(filePath);
		dir.setNameFilters(QStringList() << "*.ini");
		count = dir.count();
		QStringList list = dir.entryList();
		//inputs
		for (int j = 0; j < count; j++)
		{
			typePath = filePath;
			typeName = list.at(j);
			typeName.chop(4);
			types[i].push_back(new Types());

			//	cam.push_back(new ImagingSource());
			types[i].back()->stationNumber = i;
			types[i].back()->Init(typeName, typePath);

		}
	}


}

void MBsoftware::ShowErrorMessage()
{
	for (int i = 0; i < logIndex.size(); i++)
	{
		if (logIndex[i] == 0) //status ok - zelena
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(150, 255, 150));
		}
		else if (logIndex[i] == 1) //napaka na napravi - rdeca
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(255, 150, 150));
		}
		else if (logIndex[i] == 2) //warning
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(Qt::yellow));
		}
		else //info - modra
		{
			ui.listWidgetStatus->item(i)->setText(QString("%1").arg(logText[i]));
			ui.listWidgetStatus->item(i)->setBackgroundColor(QColor(100, 150, 255));
		}
	}
	if (logIndex.size() > 6)
	{
		ui.listWidgetStatus->scrollToBottom();
	}
}

void MBsoftware::ClearErrorMessage(int index)
{
	ui.listWidgetStatus->item(index)->setBackgroundColor(QColor(236, 236, 236));
	ui.listWidgetStatus->item(index)->setText("");
	//log[0]->ClearWindowMessage(index);
}

void MBsoftware::AppendClearErrorMessageIndex(int index)
{
	int indexExists = 0;
	for (int i = 0; i < clearLogIndex.size(); i++)
	{
		if (clearLogIndex[i] == index)
		{
			indexExists = 1;
			break;
		}
	}
	if (indexExists == 0)
		clearLogIndex.push_back(index);
}


void MBsoftware::DrawMeasurements()
{



	 QColor bcolor;

	 //v prihodnosti dodaj se za vec postaj
	 ui.labelCurrentType->setText(QString("TYPE: %1").arg(types[0][currentType]->typeName));

		 for (int i = 0; i < NR_STATIONS; i++)
		 {

			 
			 parametersTable[i]->setRowCount(types[i][currentType]->parameterCounter);
			 for (int row = 0; row < parametersTable[i]->rowCount(); row++)
			 {
				 //ce se je dodal nov parameter, dodamo novo vrstico

					 for (int column = 0; column < parametersTable[i]->columnCount(); column++)
					 {
						 QTableWidgetItem *newItem;
						 switch (column)
						 {
						 case 0:
							 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->name[row]));
							 break;
						 case 1:
							 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->measuredValue[row]));
							 break;
						 case 2:
							 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->toleranceLow[row]));
							 break;

						 case 3:
							 newItem = new QTableWidgetItem(tr("%1").arg(types[i][currentType]->toleranceHigh[row]));
							 break;
						 default:
							 break;
						 }
						 parametersTable[i]->setItem(row, column, newItem);
					 }

			 }

			 //obarva vrstice dobre slabe
			 for (int row = 0; row < parametersTable[i]->rowCount(); row++)
			 {
				 types[i][currentType]->IsGood(row);
				 if (types[i][currentType]->isActive[row])
				 {
					 //measurements[i][vars->selectedType]->IsGood(row);

					 if (types[i][currentType]->isGood[row] == 1)
					 {
						 bcolor = QColor(150, 255, 150);
					 }
					 else if (types[i][currentType]->isGood[row] == 0)
						 bcolor = QColor(255, 150, 150);
					 else
						 bcolor = QColor(0, 170, 255);
				 }
				 else
				 {
					 bcolor = QColor(236, 236, 236);
				 }

				 parametersTable[i]->item(row, 0)->setBackground(bcolor);
				 parametersTable[i]->item(row, 1)->setBackground(bcolor);
				 parametersTable[i]->item(row, 2)->setBackground(bcolor);
				 parametersTable[i]->item(row, 3)->setBackground(bcolor);
				 //measurementsResoults->item(row, 1)->setText(QString("%1").arg(types[0][vars->selectedType]->measuredValue[row]));
			 }
		 }

}




void MBsoftware::PopulateStatusBox()
{
	int index = 0;

	index = 0;
	statusBox[0]->AddItem(index, QString("MM timer:  %1 Hz").arg(MMTimer.frequency));
	index++;
	statusBox[0]->AddItem(index, QString("View timer:  %1 Hz").arg(viewTimerTimer.frequency));
	index++;
	statusBox[0]->AddItem(index, QString("View timer2:  %1 Hz").arg(viewTimerTimer2.frequency));
	index++;
	for (int i = 0; i < cam.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("cam %1 FPS:  %2").arg(i).arg(cam[i]->frameReadyFreq.frequency));
		index++;
	}
	for (int i = 0; i < imageProcess->processingTimer.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("Obdelave%1 : %2 [ms]").arg(i).arg(imageProcess->processingTimer[i]->elapsed));
		index++;
	}
	for (int i = 0; i < TestFun.size(); i++)
	{
		statusBox[0]->AddItem(index, QString("GlavniStevec%1 : %2 [ms]").arg(i).arg(TestFun[i]->elapsed));
		index++;
	}


}



void MBsoftware::DrawImageOnScreen()
{
	if (!isSceneAdded)
	{
		for (int i = 0; i < NR_STATIONS; i++)
		{
			MBsoftware::showScene[i] = new QGraphicsScene(this);
		}
		ui.imageViewMainWindow_0->setScene(MBsoftware::showScene[0]);
	

		isSceneAdded = true;
	}
	for (int i = 0; i < NR_STATIONS; i++)
	{
		qDeleteAll(showScene[i]->items());
	}

	ui.imageViewMainWindow_0->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	

}

void MBsoftware::UpdateImageView(int station)
{

		qDeleteAll(showScene[station]->items());
		
			//QImage qimgOriginal((uchar*)imageProcess->displayLamela[station][index].data, imageProcess->displayLamela[station][index].cols, imageProcess->displayLamela[station][index].rows, imageProcess->displayLamela[station][index].step, QImage::Format_Grayscale8);

			//showScene[station]->addPixmap(QPixmap::fromImage(qimgOriginal));


}




void MBsoftware::ViewTimer()
{
	int bla = 10;

	QFont font;
	if (viewTimerTimer.timeCounter >= 10)
	{
		viewTimerTimer.SetStop();
		viewTimerTimer.CalcFrequency(viewTimerTimer.timeCounter);
		viewTimerTimer.timeCounter = 0;
		viewTimerTimer.SetStart();
	}

	viewTimerTimer.timeCounter++;

	if (isActiveWindow())
	{
		grabKeyboard();
	}
	else
		releaseKeyboard();




	if (login->currentRights > 0) //uporabnik prijavljen
	{
		ui.labelLogin->setText(QString("%1 Logged in!").arg(login->currentUser));
		ui.labelLogin->setStyleSheet(QStringLiteral("background-color: rgb(150, 255, 150);"));
	}
	else
	{
		ui.labelLogin->setText(QString("User logged out!"));
		ui.labelLogin->setStyleSheet(QStringLiteral("background-color: rgb(236, 236, 236);"));
	}


	PopulateStatusBox();
	DrawMeasurements();
	//ShowErrorMessage();

}


void MBsoftware::OnClickedShowLptButton(int lptNum)
{
	lpt[lptNum]->OnShowDialog();
}

void MBsoftware::OnClickedShowCamButton(int camNum)
{
	cam[camNum]->ShowDialog(login->currentRights);
}

void MBsoftware::OnClickedShowSmartCardButton(int cardNum)
{
	controlCardMB[cardNum]->ShowDialog();
}

void MBsoftware::OnClickedShowImageProcessing()
{
	QStringList list;
	QMessageBox::StandardButton reply;
	int stNum = 0;
	if (login->currentRights > 0)
	{
		imageProcess->ShowDialog(login->currentRights);
	}

	else
	{
		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"You need to be logged in! Do you want to login?",
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
	imageProcess->ShowDialog(login->currentRights);
}

void MBsoftware::OnClickedSelectType(void)
{
	QStringList list;
	int stNum;
	int result;
	QMessageBox::StandardButton reply;

	if (login->currentRights > 0)
	{
			stNum = 0;

		DlgTypes dlg(this, types[stNum].size());

		for (int i = 0; i < types[stNum].size(); i++)
		{
			dlg.types[i] = types[stNum][i]->typeName;


		}
		result = dlg.ShowDialogSelectType(currentType);
		if (result >= 0)
		{
			prevType = currentType;
			currentType = result;
			SaveVariables();
			AddLog("Type Changed!", 3);
		}
	}
	else
	{
		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"You need to be logged in! Do you want to login?",
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();

}  
void MBsoftware::OnClickedEditTypes(void)
{
	QMessageBox::StandardButton reply;
	QStringList list;
	int stNum = 0;
	if (login->currentRights > 0)
	{
		if (NR_STATIONS > 1)
		{
			for (int i = 0; i < NR_STATIONS; i++)
			{
				list.push_back(QString("%1").arg(i));
			}
			bool ok;
			QString text = QInputDialog::getItem(this, tr("select station"), tr("label"), list, 0, false, &ok);

			stNum = text.toInt();
		}
		else
			stNum = 0;
		types[stNum][currentType]->OnShowDialog(login->currentRights);
	}

	else
	{
		
		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"You need to be logged in! Do you want to login?",
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();

}
void MBsoftware::OnClickedRemoveTypes(void)
{
	bool ok;
	QMessageBox box;
	QInputDialog dialog;
	QStringList list;
	QString tip;
	QString destPath;
	QString sourcePath;
	QString sourcePropertyPath;
	QString sourcePath2;
	QString destPath2;
	QMessageBox::StandardButton reply;
	

	if (login->currentRights > 0)
	{

			if (types[0].size() > 1)
			{
				for (int i = 0; i < types[0].size(); i++)
					list.append(types[0][i]->typeName);

				dialog.setComboBoxItems(list);
				dialog.setWindowTitle("delete type");
				dialog.setLabelText("Select type to remove");
				if (dialog.exec() == IDOK)
				{
					for (int j = 0; j < NR_STATIONS; j++)
					{
						tip = dialog.textValue();
						destPath = QDir::currentPath() + QString("/%1/types/types_Station%2/DeletedTypes").arg(REF_FOLDER_NAME).arg(j);
						destPath2 = destPath;
						if (!QDir(destPath).exists())
							QDir().mkdir(destPath);

						destPath += QString("/%1_deleted.ini").arg(tip);
						destPath2 += QString("/counters_%1_deleted.txt").arg(tip);

						sourcePath = QDir::currentPath() + QString("/%1/types/types_Station%2").arg(REF_FOLDER_NAME).arg(j);
						sourcePath2 = sourcePath + QString("/counters_%1.txt").arg(tip);
						sourcePath += QString("/%1.ini").arg(tip);


						QFile::copy(sourcePath, destPath);
						QFile::copy(sourcePath2, destPath2);

						QFile::remove(sourcePath);
						QFile::remove(sourcePath2);

						int cur = 0;
						for (int i = 0; i < types[j].size(); i++)
						{
							if (tip == types[j][i]->typeName)
							{
								types[j].erase(types[j].begin() + i);
							}
						}
						//izbrisemo tudi dynamicVariables za property tipov 
						sourcePropertyPath = QDir::currentPath() + QString("/%1/dynamicVariables/types_Station%2/parameters_%3").arg(REF_FOLDER_NAME).arg(j).arg(tip);

						QDir propDir(sourcePropertyPath);
						propDir.removeRecursively();


						prevType = currentType;
						currentType = 0;
						types[j][currentType]->parametersChanged = true;
					}
				}

			}
			else
			{
				box.setStandardButtons(QMessageBox::Ok);
				box.setIcon(QMessageBox::Warning);
				box.setText("unable to delete all types!");
				box.exec();
			}
		

	}
	else
	{
	reply = QMessageBox::question(this, tr("QMessageBox::question()"),
		"You need to be logged in! Do you want to login?",
		QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();

}
void MBsoftware::OnClickedAddTypes(void)//zaenkrat kopira celoten TIP 
{
	bool ok;
	QInputDialog dialog;
	QStringList list;
	QString tip;
	QString filePath;
	QMessageBox::StandardButton reply;

	if (login->currentRights > 0)
	{

		dialog.setWindowTitle("Add new type");
		dialog.setLabelText("Insert type name:");

		if (dialog.exec() == IDOK)
		{
			tip = dialog.textValue();

			for (int j = 0; j < NR_STATIONS; j++)
			{
				filePath = QDir::currentPath() + QString("//%1/types/types_Station%2").arg(REF_FOLDER_NAME).arg(j);
				types[j].push_back(new Types);

				types[j].back()->stationNumber = 0;
				types[j].back()->Init(tip, filePath);

				for (int i = 0; i < types[j][currentType]->parameterCounter; i++)
				{
					types[j].back()->toleranceHigh.push_back(types[j][currentType]->toleranceHigh[i]);
					types[j].back()->toleranceLow.push_back(types[j][currentType]->toleranceLow[i]);
					types[j].back()->toleranceHighCond.push_back(types[j][currentType]->toleranceHighCond[i]);
					types[j].back()->toleranceLowCond.push_back(types[j][currentType]->toleranceLowCond[i]);
					types[j].back()->correctFactor.push_back(types[j][currentType]->correctFactor[i]);
					types[j].back()->offset.push_back(types[j][currentType]->offset[i]);
					types[j].back()->nominal.push_back(types[j][currentType]->nominal[i]);
					types[j].back()->isActive.push_back(types[j][currentType]->isActive[i]);
					types[j].back()->isConditional.push_back(types[j][currentType]->isConditional[i]);
					types[j].back()->name.push_back(types[j][currentType]->name[i]);

					types[j].back()->AddParameter(i);
					types[j].back()->measuredValue.push_back(0);
					types[j].back()->isGood.push_back(0);
					types[j].back()->conditional.push_back(0);
				}
				types[j].back()->parameterCounter = types[j][currentType]->parameterCounter;
				types[j].back()->WriteParameters();
				types[j].back()->WriteCounters();


				for (int i = 0; i < types[j][currentType]->prop.size(); i++)
				{
					types[j].back()->prop[i] = types[j][currentType]->prop[i];

				}
				imageProcess->ConnectTypes(j,types[j]);

				for (int i = 0; i < types[j][currentType]->prop.size(); i++)
				{
					imageProcess->SaveFunctionParametersOnNewType(j,types[j].size() - 1, i);
				}
			}

		}
	}
	else
	{
		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"You need to be logged in! Do you want to login?",
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();
}
void MBsoftware::OnClickedResetCounters(void)
{
	QMessageBox::StandardButton reply;
	reply = QMessageBox::question(this, tr("QMessageBox::question()"),
		"Reset piece Counter?",
		QMessageBox::Yes | QMessageBox::No);

	if (reply == QMessageBox::Yes)
	{
		for (int i = 0; i < NR_STATIONS; i++)
		{
			types[i][currentType]->ResetCounters();
			types[i][currentType]->WriteCounters();
		}


	}

}
void MBsoftware::OnClickedLogin(void)
{
	
	login->ShowDialog();
}
void MBsoftware::OnClickedStatusBox(void)
{
	statusBox[0]->ShowDialog();

}
void MBsoftware::OnClickedSmcButton(void)
{
	if(smc.size() > 0)
	smc[0]->ShowDialog();
}
void MBsoftware::OnClickedShowHistory(void)
{
	QStringList list;
	int stNum;
	if (NR_STATIONS > 1)
	{
		for (int i = 0; i < NR_STATIONS; i++)
		{
			list.push_back(QString("Station: %1").arg(i));
		}
		bool ok;
		QString text = QInputDialog::getItem(this, tr("select station"), tr("label"), list, 0, false, &ok);

		for (int i = 0; i < list.size(); i++)
		{
			if (text == list[i])
				stNum = i;
		}
	}
	else
		stNum = 0;

	history->OnShowDialog(stNum);
}
void MBsoftware::OnClickedGeneralSettings(void)
{

	QMessageBox::StandardButton reply;
	if ((login->currentRights == 1) || (login->currentRights == 2))
		settings->OnShowDialog();
	else
	{

		reply = QMessageBox::question(this, tr("QMessageBox::question()"),
			"You need Administrator or operaters rights to change settings!",
			QMessageBox::Yes | QMessageBox::No);
	}
	if (reply == QMessageBox::Yes)
		login->ShowDialog();
	
}

void MBsoftware::OnClickedAboutUs()
{
	about->OnShowDialog();
}
void MBsoftware::onClientReadyRead()
{
	QString msg = client->readAll();

}
void MBsoftware::OnClickedTCPconnction(int index)
{
	if (tcp.size() >= index)
		tcp[index]->OnShowDialog();
}
void MBsoftware::SaveVariables()
{
	QString fileName;
	QString line;
	QString dirPath;
	QDir dir;


	dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);

	if (!QDir(dirPath).exists())
		QDir().mkdir(dirPath);


	dir.setPath(dirPath);

	fileName = dirPath + "currentVariables.txt";
	QFile file(fileName);
	if (file.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream stream(&file);

		stream << currentType;
		stream << endl;

		file.close();
	}
}
void MBsoftware::LoadVariables()
{
	QString fileName;
	QString line;
	QString dirPath;
	QDir dir;


	dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/").arg(REF_FOLDER_NAME);

	if (!QDir(dirPath).exists())
		QDir().mkdir(dirPath);


	dir.setPath(dirPath);

	fileName = dirPath + "currentVariables.txt";
		QFile file(fileName);
		if (file.open(QIODevice::ReadOnly))
		{
			QTextStream stream(&file);

			line = stream.readLine();
			currentType = line.toInt();
			file.close();
		}
		else
		{
			currentType = 0;
		}

		if (currentType >= types[0].size())
			currentType = 0;
		//prevType = currentType;
}
void MBsoftware::mouseDoubleClickEvent(QMouseEvent * e)
{
	QRect containtRect;
	int windowPos = -1;
	int nrStation = -1;

	if (e->button() == Qt::LeftButton)
	{

		containtRect = ui.imageViewMainWindow_0->geometry();
		if (containtRect.contains(e->pos()) == true)
		{

				windowPos = 0;
				nrStation = 0;

		}
		
	}


}

void MBsoftware::keyPressEvent(QKeyEvent *event)
{	
 	qDebug() << event->key();
	QString fileP;
	CPointFloat points[3];
	int i = 0;
	QString name;

	switch (event->key())
	{
	case  Qt::Key_1:
		

		break;
	case  Qt::Key_2:

		break;

	case  Qt::Key_3:


		break;
	case  Qt::Key_4:

		break;
	case  Qt::Key_5:
		//MeasurePiece(0, 0);
		break;
	case  Qt::Key_6:
		//MeasurePiece(1, 0);
		break;
	case  Qt::Key_7:

		break;
	case  Qt::Key_8:
		
		break;

	case  Qt::Key_9:

		break;
	case  Qt::Key_F4:
		break;

	case  Qt::Key_F6:


		break;

	case  Qt::Key_A:
		cam[0]->DisableLive();
	
		break;
	case  Qt::Key_S:

		break;

	case  Qt::Key_R:



		break;

	case  Qt::Key_T:



		break;
	case  Qt::Key_E:


		break;
	case  Qt::Key_G:
		

		break;
	case  Qt::Key_I:
		



		break;
	case  Qt::Key_J:


		break;
	case  Qt::Key_K:

		break;

	case Qt::Key_Space:


			break;
	case  Qt::Key_Escape:
	this->	repaint();
	for (int i = 0; i < cam.size(); i++)
	{
		if (cam[i]->isVisible())
			cam[i]->close();
	}

	for (int i = 0; i < smc.size(); i++)
	{
		if (smc[i]->isVisible())
			smc[i]->close();
	}
	for (int i = 0; i < controlCardMB.size(); i++)
	{
		if (controlCardMB[i]->isVisible())
			controlCardMB[i]->close();
	}
	for (int i = 0; i < statusBox.size(); i++)
	{
		if (statusBox[i]->isVisible())
			statusBox[i]->close();
	}
	for (int i = 0; i < lpt.size(); i++)
	{
		if (lpt[i]->isVisible())
			lpt[i]->close();
	}
	imageProcess->close();
		break;

	default:

		break;
	}
}


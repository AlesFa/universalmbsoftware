#pragma once

class CProperty
{
public:
	CProperty();
	~CProperty();

public:
	QString propName;
	int propType;


	bool boolValue;


	int intValue;
	int intMaxValue;
	int intMinValue;

	double doubleMinValue;
	double doubleValue;
	double doubleMaxValue;

	QRect rectValue;

};

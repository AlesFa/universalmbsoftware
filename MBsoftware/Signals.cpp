#include "stdafx.h"
#include "Signals.h"


Signals::Signals()
{
	isActive = true;
	address = 0;
	value = 0;
	prevValue = 0;
	valueFloat = 0.0;
	prevValueFloat = 0.0;
	prevValueBool = false;
	valueBool = false;
	isBlinking = 0;
	type = 0;
	timerFrequency = 0;
	triggingFrequency = 0;
	triggerStartPos = 0;
	isWritten = false;
	delay = 0;
	bitNumber = 0;
	triggerDuration = 0;
	triggerEnable = 0;
	counter = 0;
	name = "name";
}
Signals::Signals(QString name, UCHAR address, bool isActive)
{
	value = 0;
	prevValue = 0;
	valueFloat = 0.0;
	prevValueFloat = 0.0;
	prevValueFloat = 0;
	valueBool = 0;
	isBlinking = 0;
	timerFrequency = 0;
	triggingFrequency = 0;
	triggerStartPos = 0;
	isWritten = false;
	delay = 0;
	bitNumber = 0;
	triggerDuration = 0;
	triggerEnable = 0;
	type = 0;

	this->name = name/*.GetBuffer()*/;
	this->address = address;
	this->isActive = isActive;

}

Signals::Signals(QString name, int address, int type, bool isActive)
{
	value = 0;
	counter = 0;
	prevValue = 0;
	valueFloat = 0.0;
	prevValueFloat = 0.0;
	prevValueBool = 0;
	valueBool = 0;
	isBlinking = 0;
	timerFrequency = 0;
	triggingFrequency = 0;
	triggerStartPos = 0;
	isWritten = false;
	delay = 0;
	bitNumber = 0;
	triggerDuration = 0;
	triggerEnable = 0;
	this->address = address;
	this->isActive = isActive;
	this->name = name;

	this->type = type;

}

Signals::Signals(bool input, QString name, UCHAR address, QString source)
{
	if (input)
		SetInput(name, address, source);
	else
		SetOutput(name, address, source);

	isWritten = false;
	delay = 0;
	bitNumber = 0;
	value = 0;
	prevValue = 0;
	valueFloat = 0.0;
	prevValueFloat = 0.0;
	isBlinking = 0;
	type = 0;

}

Signals::Signals(QString name, int address, int type, bool isActive, int bitnumber)
{

	value = 0;
	counter = 0;
	prevValue = 0;
	valueFloat = 0.0;
	prevValueFloat = 0.0;
	prevValueBool = 0;
	valueBool = 0;
	isBlinking = 0;
	timerFrequency = 0;
	triggingFrequency = 0;
	triggerStartPos = 0;
	delay = 0;
	isWritten = false;
	bitNumber = bitnumber;
	triggerDuration = 0;
	triggerEnable = 0;
	this->address = address;
	this->isActive = isActive;
	this->name = name;

	this->type = type;
}

Signals::Signals(const Signals& signal)
{
	bitNumber = signal.bitNumber;
	value = signal.value;
	prevValue = signal.prevValue;
	valueFloat = signal.valueFloat;
	prevValueFloat = signal.prevValueFloat;
	valueBool = signal.valueBool;
	prevValueBool = signal.prevValueBool;
	isBlinking = signal.isBlinking;
	name = signal.name;
	address = signal.address;
	isActive = signal.isActive;
	type = signal.type;
	delay = signal.delay;
	isWritten = signal.isWritten;

	timerFrequency = signal.timerFrequency;
	triggingFrequency = signal.triggingFrequency;
	triggerStartPos = signal.triggerStartPos;

	triggerDuration = signal.triggerDuration;
	triggerEnable = signal.triggerEnable;
	counter = signal.counter;


}

Signals::~Signals()
{
}

void Signals::SetTrigger(int cycle, int timerFrequency, int triggingFrequency, int triggerStartPos, int triggerDuration, int tableLength, int triggEnable)
{
	this->cycle = cycle;
	this->timerFrequency = timerFrequency;			// teoretical timer frequency - for ratio calculation
	this->triggingFrequency = triggingFrequency;	// working frequency of selected object (camera, valve, light ...)
	this->triggerStartPos = triggerStartPos;		// position inside cycle of the first trigger 
	this->triggerDuration = triggerDuration;		//
	this->triggerEnable = triggEnable;					//1 - enables trigging, 0 - disables trigging

	triggerPosition = triggerStartPos;
	type = 2;

	if (triggingFrequency > 0)
	{
		numberOfInterrupts = timerFrequency / triggingFrequency;

		if ((numberOfInterrupts > 0) && (cycle > 0))
			numberOfInterruptsInCycle = cycle / numberOfInterrupts;
	}

}
void Signals::SetTrigger(int triggerDuration, int tableLength)
{

}

void Signals::SetTrigger(int triggerDuration)
{

}


bool Signals::GetTriggerPulse(int counter)
{
	int interval;
	bool result = false;

	if (triggerEnable)
	{
		interval = (timerFrequency / triggingFrequency);

		if (triggerStartPos < interval)
		{
			if ((counter % interval) == triggerStartPos)
			{
				result = true;
			}
		}
	}

	return result;
}

void Signals::SetSignal(QString name, int address, int  isInput)//ales pri uporabi LPT za controlne bite kot inputo
{
	this->name = name;
	this->address = address;
	this->isInput = isInput;
}

void Signals::SetSignal(QString name, int address, bool isActive)
{
	this->name = name;
	this->address = address;
	this->isActive = isActive;
}

void Signals::SetSignal(QString name, int address, int type, bool isActive)
{
	this->name = name;
	this->address = address;
	this->type = type;
	this->isActive = isActive;
}

void Signals::SetInput(QString name, UCHAR address, QString source)
{
	this->name = name;
	this->address = address;
	this->isActive = true;
}

void Signals::SetOutput(QString name, UCHAR address, QString source)
{
	this->name = name;
	this->address = address;
	this->isActive = true;
}



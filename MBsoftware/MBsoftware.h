#pragma once

#include <QTextEdit>
#include <QtNetwork/qtcpserver.h>
#include <QtNetwork/qtcpsocket.h>
#include "LoginWindow.h"
#include "ui_MBsoftware.h"
#include "ui_imageProcessing.h"
#include "Serial.h"
#include "ControlCardMB.h"
#include "Camera.h"
#include "ImagingSource.h"
#include "PointGray.h"
#include "Timer.h"
#include "imageProcessing.h"
#include "Types.h"
#include "DlgTypes.h"
#include "SMCdrive.h"
#include "statusBox.h"
#include "LoginWindow.h"
#include "Measurand.h"
#include "Pcie.h"
#include "stdafx.h"
#include "CProperty.h"
#include "History.h"
#include <iostream>
#include "GeneralSettings.h"
#include "ControlLPT.h"
#include "TCP.h"
#include "LogWindow.h"
#include "AboutUS.h"

#include <QtCharts/QScatterSeries>
#include <QtCharts/QLineSeries>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QAreaSeries>
#include <QtCharts/QValueAxis>
#include <QtCharts/QPolarChart>
#include <QPolarChart>
#include <QChart>
#include "qpolarchart.h"
#include <QtCharts/QAbstractAxis>
#include <QtCharts/QValueAxis>
#include <QtCharts/QChartView>
#include <QtCharts/QPolarChart>
#include "qchart.h"
#include <QtCharts>



class MBsoftware : public QMainWindow
{
	Q_OBJECT

public:
	void closeEvent(QCloseEvent * event);
	MBsoftware(QWidget *parent = Q_NULLPTR);

	

//Deklaracija globalnih spremenljivk
public:
	QTimer * OnTimer;
	QTimer	*OnTimer2;
	static MBsoftware*						glob;
	static std::vector<CCamera*>			cam;
	static std::vector<CMemoryBuffer*>		images;
	static std::vector<Serial*>				serial;
	static std::vector<Pcie*>				pcieDio;
	static std::vector<SMCdrive*>			smc;
	static std::vector<ControlCardMB*>		controlCardMB;
	static std::vector<ControlLPT*>			lpt;
	static std::vector<TCP*>				tcp;
	static Timer							MMTimer;
	static Timer							viewTimerTimer;
	static Timer							viewTimerTimer2;
	static std::vector<Timer*>				TestFun;
	static std::vector<StatusBox*>			statusBox;
	static std::vector<LogWindow*>			log;
	static AboutUS*							about;
	static int								mmCounter;
	static int								viewCounter;
	static LoginWindow*						login;
	static GeneralSettings*					settings;
	static imageProcessing*					imageProcess;
	static Measurand						measureObject;
	static QTcpSocket*						client;
	//4 niti za 4 slike
	static QFuture<int>					future[4];

	static vector<int>						clearLogIndex;
	static vector<int>						logIndex;
	static vector<QString>					logText;


	static QReadWriteLock					lockReadWrite;

	static std::vector<Types*>		types[NR_STATIONS];
	static History*					history;
	
	QTableWidget  *parametersTable[NR_STATIONS];
	
	static int currentType;
	static int prevType;
	 QGraphicsScene * showScene[4];
	QGraphicsPixmapItem*	pixmapVideo[4];
	bool isSceneAdded;


	
	

private:
	Ui::MBsoftwareClass ui;

	//za kameraStatus widget
	QLabel *camStatusLabel[MAX_CAM]; //kvadratek pred kamero
	QLabel *camTextLabel[MAX_CAM]; // ime kamere //ali serijska stevilka

	//za serer status
	QLabel *serverStatusLabel; //kvadratek pred kamero
	QLabel *serverTextLabel; // ime kamere //ali serijska stevilka
	//QPOLARCHART_H
protected:
	static void __stdcall OnMultimediaTimer(UINT uTimerID, UINT, DWORD_PTR  dwUser, DWORD_PTR  dw1, DWORD_PTR  dw2);
	void keyPressEvent(QKeyEvent * event) ;
	void mouseDoubleClickEvent(QMouseEvent * e);


public:
	//izgled aplikacije
	void CreateCameraTab();
	void CreateSignalsTab();
	void CreateMeasurementsTable();
	void CreateChart();
	void ReadTypes();
	void ShowErrorMessage();
	void ClearErrorMessage(int index);
	void AppendClearErrorMessageIndex(int index);
	void DrawImageOnScreen();
	void UpdateImageView(int station);


	void DrawMeasurements();
	void PopulateStatusBox();

	

private slots:
	void ViewTimer(); //timer za izris zaslona
	void OnFrameReady(int, int); //frameready signal se izvede kadar je nova slika pripravljena
	void OnTcpDataReady(int parse); //frameready signal se izvede kadar je nova slika pripravljena
	void HistoryCall(int station, int currentPiece); //iz zgodovine so priklicane slike za izbran kos
	
	void OnClickedShowLptButton(int);
	void OnClickedShowCamButton(int);
	void OnClickedShowSmartCardButton(int);
	void OnClickedShowImageProcessing(void);
	void OnClickedSelectType(void);//prikaz dialoga za izbiro drugega tipa
	void OnClickedEditTypes(void);//prikaz dialoga za urejenje toleranc
	void OnClickedRemoveTypes(void);
	void OnClickedAddTypes(void);
	void OnClickedResetCounters(void);
	void OnClickedLogin(void);
	void OnClickedStatusBox(void);
	void OnClickedSmcButton(void);
	void OnClickedShowHistory(void);
	void OnClickedGeneralSettings(void);
	void OnClickedAboutUs();
	void onClientReadyRead();
	void OnClickedTCPconnction(int);

	void SaveVariables();	//spremenljivke se shranijo ob spremembi ali ob zaprtju programa //currentType!!!
	void LoadVariables();

	void AddLog(QString text, int type);
	void EraseLastLOG();

public:
	static int obdelanaStevec;
	void MeasurePiece(int index);
	void MeasurePiece(int station, int index);
	void UpdateHistory(int station, int index);
	void SetHistory();



 signals:

	
};

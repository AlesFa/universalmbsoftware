/********************************************************************************
** Form generated from reading UI file 'SMCdrive.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SMCDRIVE_H
#define UI_SMCDRIVE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SMCdrive
{
public:
    QToolButton *buttonMotorOn;
    QToolButton *buttonReset;
    QToolButton *buttonOrigin;
    QComboBox *comboBoxID;
    QLabel *label;
    QLabel *labelCommOK;
    QLabel *labelCommStatus;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout;
    QLabel *labelAlarm;
    QLabel *labelSVRE;
    QLabel *labelBusy;
    QLabel *labelINP;
    QLabel *labelSeton;
    QGroupBox *groupBox_2;
    QLabel *labelPosition;
    QLabel *label_3;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_2;
    QToolButton *buttonJogMinus;
    QToolButton *buttonJogPlus;
    QGroupBox *groupBox_4;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *editMoveToPosition;
    QToolButton *buttonMove;

    void setupUi(QWidget *SMCdrive)
    {
        if (SMCdrive->objectName().isEmpty())
            SMCdrive->setObjectName(QString::fromUtf8("SMCdrive"));
        SMCdrive->resize(578, 464);
        buttonMotorOn = new QToolButton(SMCdrive);
        buttonMotorOn->setObjectName(QString::fromUtf8("buttonMotorOn"));
        buttonMotorOn->setGeometry(QRect(480, 80, 71, 51));
        buttonReset = new QToolButton(SMCdrive);
        buttonReset->setObjectName(QString::fromUtf8("buttonReset"));
        buttonReset->setGeometry(QRect(390, 80, 71, 51));
        buttonOrigin = new QToolButton(SMCdrive);
        buttonOrigin->setObjectName(QString::fromUtf8("buttonOrigin"));
        buttonOrigin->setGeometry(QRect(300, 80, 71, 51));
        comboBoxID = new QComboBox(SMCdrive);
        comboBoxID->setObjectName(QString::fromUtf8("comboBoxID"));
        comboBoxID->setGeometry(QRect(30, 100, 201, 22));
        label = new QLabel(SMCdrive);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(30, 80, 47, 13));
        labelCommOK = new QLabel(SMCdrive);
        labelCommOK->setObjectName(QString::fromUtf8("labelCommOK"));
        labelCommOK->setGeometry(QRect(180, 10, 111, 31));
        labelCommOK->setFrameShape(QFrame::Box);
        labelCommStatus = new QLabel(SMCdrive);
        labelCommStatus->setObjectName(QString::fromUtf8("labelCommStatus"));
        labelCommStatus->setGeometry(QRect(100, 10, 71, 31));
        groupBox = new QGroupBox(SMCdrive);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(20, 170, 351, 71));
        horizontalLayout = new QHBoxLayout(groupBox);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        labelAlarm = new QLabel(groupBox);
        labelAlarm->setObjectName(QString::fromUtf8("labelAlarm"));
        labelAlarm->setMinimumSize(QSize(60, 20));
        labelAlarm->setMaximumSize(QSize(40, 40));
        labelAlarm->setFrameShape(QFrame::Box);
        labelAlarm->setFrameShadow(QFrame::Raised);

        horizontalLayout->addWidget(labelAlarm);

        labelSVRE = new QLabel(groupBox);
        labelSVRE->setObjectName(QString::fromUtf8("labelSVRE"));
        labelSVRE->setMinimumSize(QSize(60, 20));
        labelSVRE->setMaximumSize(QSize(40, 40));
        labelSVRE->setFrameShape(QFrame::Box);
        labelSVRE->setFrameShadow(QFrame::Raised);

        horizontalLayout->addWidget(labelSVRE);

        labelBusy = new QLabel(groupBox);
        labelBusy->setObjectName(QString::fromUtf8("labelBusy"));
        labelBusy->setMinimumSize(QSize(60, 20));
        labelBusy->setMaximumSize(QSize(40, 40));
        labelBusy->setFrameShape(QFrame::Box);
        labelBusy->setFrameShadow(QFrame::Raised);

        horizontalLayout->addWidget(labelBusy);

        labelINP = new QLabel(groupBox);
        labelINP->setObjectName(QString::fromUtf8("labelINP"));
        labelINP->setMinimumSize(QSize(60, 20));
        labelINP->setMaximumSize(QSize(40, 40));
        labelINP->setFrameShape(QFrame::Box);
        labelINP->setFrameShadow(QFrame::Raised);

        horizontalLayout->addWidget(labelINP);

        labelSeton = new QLabel(groupBox);
        labelSeton->setObjectName(QString::fromUtf8("labelSeton"));
        labelSeton->setMinimumSize(QSize(60, 20));
        labelSeton->setMaximumSize(QSize(40, 40));
        labelSeton->setFrameShape(QFrame::Box);
        labelSeton->setFrameShadow(QFrame::Raised);

        horizontalLayout->addWidget(labelSeton);

        groupBox_2 = new QGroupBox(SMCdrive);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(390, 170, 141, 71));
        labelPosition = new QLabel(groupBox_2);
        labelPosition->setObjectName(QString::fromUtf8("labelPosition"));
        labelPosition->setGeometry(QRect(10, 20, 91, 31));
        labelPosition->setFrameShape(QFrame::Box);
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(110, 20, 31, 31));
        groupBox_3 = new QGroupBox(SMCdrive);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(20, 280, 181, 91));
        horizontalLayout_2 = new QHBoxLayout(groupBox_3);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        buttonJogMinus = new QToolButton(groupBox_3);
        buttonJogMinus->setObjectName(QString::fromUtf8("buttonJogMinus"));
        buttonJogMinus->setMinimumSize(QSize(71, 51));

        horizontalLayout_2->addWidget(buttonJogMinus);

        buttonJogPlus = new QToolButton(groupBox_3);
        buttonJogPlus->setObjectName(QString::fromUtf8("buttonJogPlus"));
        buttonJogPlus->setMinimumSize(QSize(71, 51));

        horizontalLayout_2->addWidget(buttonJogPlus);

        groupBox_4 = new QGroupBox(SMCdrive);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(270, 290, 191, 81));
        horizontalLayout_3 = new QHBoxLayout(groupBox_4);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        editMoveToPosition = new QLineEdit(groupBox_4);
        editMoveToPosition->setObjectName(QString::fromUtf8("editMoveToPosition"));

        horizontalLayout_3->addWidget(editMoveToPosition);

        buttonMove = new QToolButton(groupBox_4);
        buttonMove->setObjectName(QString::fromUtf8("buttonMove"));
        buttonMove->setMinimumSize(QSize(71, 51));

        horizontalLayout_3->addWidget(buttonMove);


        retranslateUi(SMCdrive);

        QMetaObject::connectSlotsByName(SMCdrive);
    } // setupUi

    void retranslateUi(QWidget *SMCdrive)
    {
        SMCdrive->setWindowTitle(QCoreApplication::translate("SMCdrive", "SMCdrive", nullptr));
        buttonMotorOn->setText(QCoreApplication::translate("SMCdrive", "Motor On", nullptr));
        buttonReset->setText(QCoreApplication::translate("SMCdrive", "reset", nullptr));
        buttonOrigin->setText(QCoreApplication::translate("SMCdrive", "Return to\n"
"Origin", nullptr));
        label->setText(QCoreApplication::translate("SMCdrive", "Select ID:", nullptr));
        labelCommOK->setText(QCoreApplication::translate("SMCdrive", "TextLabel", nullptr));
        labelCommStatus->setText(QCoreApplication::translate("SMCdrive", "com?", nullptr));
        groupBox->setTitle(QCoreApplication::translate("SMCdrive", "Status:", nullptr));
        labelAlarm->setText(QCoreApplication::translate("SMCdrive", "<html><head/><body><p><span style=\" font-size:12pt;\">ALARM</span></p></body></html>", nullptr));
        labelSVRE->setText(QCoreApplication::translate("SMCdrive", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">SVRE</span></p></body></html>", nullptr));
        labelBusy->setText(QCoreApplication::translate("SMCdrive", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">BUSY</span></p></body></html>", nullptr));
        labelINP->setText(QCoreApplication::translate("SMCdrive", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">INP</span></p></body></html>", nullptr));
        labelSeton->setText(QCoreApplication::translate("SMCdrive", "<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">SETON</span></p></body></html>", nullptr));
        groupBox_2->setTitle(QCoreApplication::translate("SMCdrive", "Current Position:", nullptr));
        labelPosition->setText(QString());
        label_3->setText(QCoreApplication::translate("SMCdrive", "mm", nullptr));
        groupBox_3->setTitle(QCoreApplication::translate("SMCdrive", "JOG:", nullptr));
        buttonJogMinus->setText(QCoreApplication::translate("SMCdrive", "JOG-", nullptr));
        buttonJogPlus->setText(QCoreApplication::translate("SMCdrive", "JOG+", nullptr));
        groupBox_4->setTitle(QCoreApplication::translate("SMCdrive", "Move To Position:", nullptr));
        buttonMove->setText(QCoreApplication::translate("SMCdrive", "MOVE", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SMCdrive: public Ui_SMCdrive {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SMCDRIVE_H

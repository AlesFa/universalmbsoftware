/****************************************************************************
** Meta object code from reading C++ file 'MBsoftware.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../MBsoftware.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MBsoftware.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MBsoftware_t {
    QByteArrayData data[32];
    char stringdata0[508];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MBsoftware_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MBsoftware_t qt_meta_stringdata_MBsoftware = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MBsoftware"
QT_MOC_LITERAL(1, 11, 9), // "ViewTimer"
QT_MOC_LITERAL(2, 21, 0), // ""
QT_MOC_LITERAL(3, 22, 12), // "OnFrameReady"
QT_MOC_LITERAL(4, 35, 14), // "OnTcpDataReady"
QT_MOC_LITERAL(5, 50, 5), // "parse"
QT_MOC_LITERAL(6, 56, 11), // "HistoryCall"
QT_MOC_LITERAL(7, 68, 7), // "station"
QT_MOC_LITERAL(8, 76, 12), // "currentPiece"
QT_MOC_LITERAL(9, 89, 22), // "OnClickedShowLptButton"
QT_MOC_LITERAL(10, 112, 22), // "OnClickedShowCamButton"
QT_MOC_LITERAL(11, 135, 28), // "OnClickedShowSmartCardButton"
QT_MOC_LITERAL(12, 164, 28), // "OnClickedShowImageProcessing"
QT_MOC_LITERAL(13, 193, 19), // "OnClickedSelectType"
QT_MOC_LITERAL(14, 213, 18), // "OnClickedEditTypes"
QT_MOC_LITERAL(15, 232, 20), // "OnClickedRemoveTypes"
QT_MOC_LITERAL(16, 253, 17), // "OnClickedAddTypes"
QT_MOC_LITERAL(17, 271, 22), // "OnClickedResetCounters"
QT_MOC_LITERAL(18, 294, 14), // "OnClickedLogin"
QT_MOC_LITERAL(19, 309, 18), // "OnClickedStatusBox"
QT_MOC_LITERAL(20, 328, 18), // "OnClickedSmcButton"
QT_MOC_LITERAL(21, 347, 20), // "OnClickedShowHistory"
QT_MOC_LITERAL(22, 368, 24), // "OnClickedGeneralSettings"
QT_MOC_LITERAL(23, 393, 16), // "OnClickedAboutUs"
QT_MOC_LITERAL(24, 410, 17), // "onClientReadyRead"
QT_MOC_LITERAL(25, 428, 21), // "OnClickedTCPconnction"
QT_MOC_LITERAL(26, 450, 13), // "SaveVariables"
QT_MOC_LITERAL(27, 464, 13), // "LoadVariables"
QT_MOC_LITERAL(28, 478, 6), // "AddLog"
QT_MOC_LITERAL(29, 485, 4), // "text"
QT_MOC_LITERAL(30, 490, 4), // "type"
QT_MOC_LITERAL(31, 495, 12) // "EraseLastLOG"

    },
    "MBsoftware\0ViewTimer\0\0OnFrameReady\0"
    "OnTcpDataReady\0parse\0HistoryCall\0"
    "station\0currentPiece\0OnClickedShowLptButton\0"
    "OnClickedShowCamButton\0"
    "OnClickedShowSmartCardButton\0"
    "OnClickedShowImageProcessing\0"
    "OnClickedSelectType\0OnClickedEditTypes\0"
    "OnClickedRemoveTypes\0OnClickedAddTypes\0"
    "OnClickedResetCounters\0OnClickedLogin\0"
    "OnClickedStatusBox\0OnClickedSmcButton\0"
    "OnClickedShowHistory\0OnClickedGeneralSettings\0"
    "OnClickedAboutUs\0onClientReadyRead\0"
    "OnClickedTCPconnction\0SaveVariables\0"
    "LoadVariables\0AddLog\0text\0type\0"
    "EraseLastLOG"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MBsoftware[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      25,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  139,    2, 0x08 /* Private */,
       3,    2,  140,    2, 0x08 /* Private */,
       4,    1,  145,    2, 0x08 /* Private */,
       6,    2,  148,    2, 0x08 /* Private */,
       9,    1,  153,    2, 0x08 /* Private */,
      10,    1,  156,    2, 0x08 /* Private */,
      11,    1,  159,    2, 0x08 /* Private */,
      12,    0,  162,    2, 0x08 /* Private */,
      13,    0,  163,    2, 0x08 /* Private */,
      14,    0,  164,    2, 0x08 /* Private */,
      15,    0,  165,    2, 0x08 /* Private */,
      16,    0,  166,    2, 0x08 /* Private */,
      17,    0,  167,    2, 0x08 /* Private */,
      18,    0,  168,    2, 0x08 /* Private */,
      19,    0,  169,    2, 0x08 /* Private */,
      20,    0,  170,    2, 0x08 /* Private */,
      21,    0,  171,    2, 0x08 /* Private */,
      22,    0,  172,    2, 0x08 /* Private */,
      23,    0,  173,    2, 0x08 /* Private */,
      24,    0,  174,    2, 0x08 /* Private */,
      25,    1,  175,    2, 0x08 /* Private */,
      26,    0,  178,    2, 0x08 /* Private */,
      27,    0,  179,    2, 0x08 /* Private */,
      28,    2,  180,    2, 0x08 /* Private */,
      31,    0,  185,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    2,    2,
    QMetaType::Void, QMetaType::Int,    5,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    7,    8,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   29,   30,
    QMetaType::Void,

       0        // eod
};

void MBsoftware::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MBsoftware *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ViewTimer(); break;
        case 1: _t->OnFrameReady((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 2: _t->OnTcpDataReady((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->HistoryCall((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: _t->OnClickedShowLptButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->OnClickedShowCamButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->OnClickedShowSmartCardButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->OnClickedShowImageProcessing(); break;
        case 8: _t->OnClickedSelectType(); break;
        case 9: _t->OnClickedEditTypes(); break;
        case 10: _t->OnClickedRemoveTypes(); break;
        case 11: _t->OnClickedAddTypes(); break;
        case 12: _t->OnClickedResetCounters(); break;
        case 13: _t->OnClickedLogin(); break;
        case 14: _t->OnClickedStatusBox(); break;
        case 15: _t->OnClickedSmcButton(); break;
        case 16: _t->OnClickedShowHistory(); break;
        case 17: _t->OnClickedGeneralSettings(); break;
        case 18: _t->OnClickedAboutUs(); break;
        case 19: _t->onClientReadyRead(); break;
        case 20: _t->OnClickedTCPconnction((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 21: _t->SaveVariables(); break;
        case 22: _t->LoadVariables(); break;
        case 23: _t->AddLog((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 24: _t->EraseLastLOG(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MBsoftware::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MBsoftware.data,
    qt_meta_data_MBsoftware,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MBsoftware::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MBsoftware::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MBsoftware.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MBsoftware::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 25)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 25;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 25)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 25;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

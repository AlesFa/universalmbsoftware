/********************************************************************************
** Form generated from reading UI file 'DlgTypes.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DLGTYPES_H
#define UI_DLGTYPES_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DlgTypes
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QDialogButtonBox *buttonBox;
    QComboBox *comboBoxTypeList;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLabel *labelCurrentType;

    void setupUi(QDialog *DlgTypes)
    {
        if (DlgTypes->objectName().isEmpty())
            DlgTypes->setObjectName(QString::fromUtf8("DlgTypes"));
        DlgTypes->setWindowModality(Qt::WindowModal);
        DlgTypes->resize(439, 373);
        horizontalLayoutWidget = new QWidget(DlgTypes);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 310, 421, 54));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        buttonBox = new QDialogButtonBox(horizontalLayoutWidget);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setEnabled(true);
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        buttonBox->setCenterButtons(true);

        horizontalLayout->addWidget(buttonBox);

        comboBoxTypeList = new QComboBox(DlgTypes);
        comboBoxTypeList->setObjectName(QString::fromUtf8("comboBoxTypeList"));
        comboBoxTypeList->setGeometry(QRect(10, 180, 421, 31));
        comboBoxTypeList->setEditable(false);
        horizontalLayoutWidget_2 = new QWidget(DlgTypes);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 50, 421, 71));
        horizontalLayout_2 = new QHBoxLayout(horizontalLayoutWidget_2);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(horizontalLayoutWidget_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font;
        font.setPointSize(14);
        label_2->setFont(font);

        horizontalLayout_2->addWidget(label_2);

        labelCurrentType = new QLabel(horizontalLayoutWidget_2);
        labelCurrentType->setObjectName(QString::fromUtf8("labelCurrentType"));
        QFont font1;
        font1.setPointSize(14);
        font1.setBold(true);
        font1.setWeight(75);
        labelCurrentType->setFont(font1);
        labelCurrentType->setFrameShape(QFrame::StyledPanel);
        labelCurrentType->setFrameShadow(QFrame::Raised);
        labelCurrentType->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(labelCurrentType);


        retranslateUi(DlgTypes);

        QMetaObject::connectSlotsByName(DlgTypes);
    } // setupUi

    void retranslateUi(QDialog *DlgTypes)
    {
        DlgTypes->setWindowTitle(QCoreApplication::translate("DlgTypes", "DlgTypes", nullptr));
        label_2->setText(QCoreApplication::translate("DlgTypes", "CurrentType:", nullptr));
        labelCurrentType->setText(QCoreApplication::translate("DlgTypes", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DlgTypes: public Ui_DlgTypes {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DLGTYPES_H

/********************************************************************************
** Form generated from reading UI file 'statusBox.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STATUSBOX_H
#define UI_STATUSBOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_StatusBox
{
public:
    QListWidget *statusList;

    void setupUi(QWidget *StatusBox)
    {
        if (StatusBox->objectName().isEmpty())
            StatusBox->setObjectName(QString::fromUtf8("StatusBox"));
        StatusBox->resize(302, 548);
        statusList = new QListWidget(StatusBox);
        statusList->setObjectName(QString::fromUtf8("statusList"));
        statusList->setGeometry(QRect(10, 10, 281, 521));

        retranslateUi(StatusBox);

        QMetaObject::connectSlotsByName(StatusBox);
    } // setupUi

    void retranslateUi(QWidget *StatusBox)
    {
        StatusBox->setWindowTitle(QCoreApplication::translate("StatusBox", "statusBox", nullptr));
    } // retranslateUi

};

namespace Ui {
    class StatusBox: public Ui_StatusBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STATUSBOX_H

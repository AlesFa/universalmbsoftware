/********************************************************************************
** Form generated from reading UI file 'types.ui'
**
** Created by: Qt User Interface Compiler version 5.13.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TYPES_H
#define UI_TYPES_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_typesUi
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_7;
    QLabel *label_6;
    QLabel *label_5;
    QLabel *label_4;
    QLabel *label_3;
    QLabel *label_2;
    QLabel *label;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *buttonOK;
    QPushButton *butttonCancel;
    QSpacerItem *horizontalSpacer;
    QPushButton *buttonAddParameter;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QLabel *label_14;
    QLabel *labelTypeName;
    QFrame *line;

    void setupUi(QWidget *typesUi)
    {
        if (typesUi->objectName().isEmpty())
            typesUi->setObjectName(QString::fromUtf8("typesUi"));
        typesUi->resize(1442, 674);
        typesUi->setMinimumSize(QSize(1024, 600));
        gridLayout = new QGridLayout(typesUi);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_7 = new QLabel(typesUi);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(40, 0));
        label_7->setMaximumSize(QSize(40, 16777215));
        QFont font;
        font.setFamily(QString::fromUtf8("Calibri"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label_7->setFont(font);

        horizontalLayout->addWidget(label_7);

        label_6 = new QLabel(typesUi);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMinimumSize(QSize(200, 0));
        label_6->setMaximumSize(QSize(200, 16777215));
        label_6->setFont(font);
        label_6->setLayoutDirection(Qt::LeftToRight);
        label_6->setFrameShape(QFrame::Box);
        label_6->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout->addWidget(label_6);

        label_5 = new QLabel(typesUi);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(120, 0));
        label_5->setMaximumSize(QSize(120, 16777215));
        label_5->setFont(font);
        label_5->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(label_5);

        label_4 = new QLabel(typesUi);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(120, 0));
        label_4->setMaximumSize(QSize(120, 16777215));
        label_4->setFont(font);
        label_4->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(label_4);

        label_3 = new QLabel(typesUi);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setMinimumSize(QSize(120, 0));
        label_3->setMaximumSize(QSize(120, 16777215));
        label_3->setFont(font);
        label_3->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(label_3);

        label_2 = new QLabel(typesUi);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setMinimumSize(QSize(120, 0));
        label_2->setMaximumSize(QSize(120, 16777215));
        label_2->setFont(font);
        label_2->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(label_2);

        label = new QLabel(typesUi);
        label->setObjectName(QString::fromUtf8("label"));
        label->setMinimumSize(QSize(120, 0));
        label->setMaximumSize(QSize(120, 16777215));
        label->setFont(font);
        label->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(label);

        label_11 = new QLabel(typesUi);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setMinimumSize(QSize(120, 0));
        label_11->setMaximumSize(QSize(120, 16777215));
        label_11->setFont(font);
        label_11->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(label_11);

        label_12 = new QLabel(typesUi);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setMinimumSize(QSize(120, 0));
        label_12->setMaximumSize(QSize(120, 16777215));
        label_12->setFont(font);
        label_12->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(label_12);

        label_8 = new QLabel(typesUi);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(120, 0));
        label_8->setMaximumSize(QSize(120, 16777215));
        label_8->setFont(font);
        label_8->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(label_8);

        label_9 = new QLabel(typesUi);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(120, 0));
        label_9->setMaximumSize(QSize(120, 16777215));
        label_9->setFont(font);
        label_9->setFrameShape(QFrame::Box);

        horizontalLayout->addWidget(label_9);

        label_10 = new QLabel(typesUi);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(25, 0));
        label_10->setMaximumSize(QSize(25, 16777215));

        horizontalLayout->addWidget(label_10);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout, 2, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        buttonOK = new QPushButton(typesUi);
        buttonOK->setObjectName(QString::fromUtf8("buttonOK"));
        buttonOK->setMinimumSize(QSize(120, 40));

        horizontalLayout_2->addWidget(buttonOK);

        butttonCancel = new QPushButton(typesUi);
        butttonCancel->setObjectName(QString::fromUtf8("butttonCancel"));
        butttonCancel->setMinimumSize(QSize(120, 40));

        horizontalLayout_2->addWidget(butttonCancel);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        buttonAddParameter = new QPushButton(typesUi);
        buttonAddParameter->setObjectName(QString::fromUtf8("buttonAddParameter"));
        buttonAddParameter->setMinimumSize(QSize(120, 40));

        horizontalLayout_2->addWidget(buttonAddParameter);


        gridLayout->addLayout(horizontalLayout_2, 4, 0, 1, 1);

        scrollArea = new QScrollArea(typesUi);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 1422, 532));
        verticalLayoutWidget = new QWidget(scrollAreaWidgetContents);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 0, 1421, 571));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        scrollArea->setWidget(scrollAreaWidgetContents);

        gridLayout->addWidget(scrollArea, 3, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        label_14 = new QLabel(typesUi);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setMinimumSize(QSize(140, 0));
        label_14->setMaximumSize(QSize(140, 16777215));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Calibri"));
        font1.setPointSize(16);
        font1.setBold(false);
        font1.setWeight(50);
        label_14->setFont(font1);

        horizontalLayout_3->addWidget(label_14);

        labelTypeName = new QLabel(typesUi);
        labelTypeName->setObjectName(QString::fromUtf8("labelTypeName"));
        labelTypeName->setMinimumSize(QSize(200, 0));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Calibri"));
        font2.setPointSize(16);
        font2.setBold(true);
        font2.setWeight(75);
        labelTypeName->setFont(font2);
        labelTypeName->setFrameShape(QFrame::Panel);
        labelTypeName->setFrameShadow(QFrame::Sunken);

        horizontalLayout_3->addWidget(labelTypeName);


        gridLayout->addLayout(horizontalLayout_3, 0, 0, 1, 1);

        line = new QFrame(typesUi);
        line->setObjectName(QString::fromUtf8("line"));
        line->setAutoFillBackground(true);
        line->setStyleSheet(QString::fromUtf8("gridline-color: rgb(0, 85, 255);\n"
"border-color: rgb(0, 85, 255);\n"
"background-color: rgb(0, 85, 255);"));
        line->setFrameShadow(QFrame::Plain);
        line->setLineWidth(10);
        line->setMidLineWidth(10);
        line->setFrameShape(QFrame::HLine);

        gridLayout->addWidget(line, 1, 0, 1, 1);


        retranslateUi(typesUi);

        QMetaObject::connectSlotsByName(typesUi);
    } // setupUi

    void retranslateUi(QWidget *typesUi)
    {
        typesUi->setWindowTitle(QCoreApplication::translate("typesUi", "Form", nullptr));
        label_7->setText(QCoreApplication::translate("typesUi", "Num:", nullptr));
        label_6->setText(QCoreApplication::translate("typesUi", "Name:", nullptr));
        label_5->setText(QCoreApplication::translate("typesUi", "Min Tolerance:", nullptr));
        label_4->setText(QCoreApplication::translate("typesUi", "Max Tolerance:", nullptr));
        label_3->setText(QCoreApplication::translate("typesUi", "Nominal:", nullptr));
        label_2->setText(QCoreApplication::translate("typesUi", "Offset:", nullptr));
        label->setText(QCoreApplication::translate("typesUi", "Correction:", nullptr));
        label_11->setText(QCoreApplication::translate("typesUi", "Min Tol. Cond.", nullptr));
        label_12->setText(QCoreApplication::translate("typesUi", "Max Tol. Cond.", nullptr));
        label_8->setText(QCoreApplication::translate("typesUi", "Is active:", nullptr));
        label_9->setText(QCoreApplication::translate("typesUi", "Conditional:", nullptr));
        label_10->setText(QString());
        buttonOK->setText(QCoreApplication::translate("typesUi", "OK", nullptr));
        butttonCancel->setText(QCoreApplication::translate("typesUi", "Cancel", nullptr));
        buttonAddParameter->setText(QCoreApplication::translate("typesUi", "Add parameter", nullptr));
        label_14->setText(QCoreApplication::translate("typesUi", "Selected Type:", nullptr));
        labelTypeName->setText(QCoreApplication::translate("typesUi", "TextLabel", nullptr));
    } // retranslateUi

};

namespace Ui {
    class typesUi: public Ui_typesUi {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TYPES_H

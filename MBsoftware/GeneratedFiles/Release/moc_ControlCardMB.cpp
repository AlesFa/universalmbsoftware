/****************************************************************************
** Meta object code from reading C++ file 'ControlCardMB.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../ControlCardMB.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ControlCardMB.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ControlCardMB_t {
    QByteArrayData data[18];
    char stringdata0[241];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ControlCardMB_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ControlCardMB_t qt_meta_stringdata_ControlCardMB = {
    {
QT_MOC_LITERAL(0, 0, 13), // "ControlCardMB"
QT_MOC_LITERAL(1, 14, 8), // "ReadData"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 9), // "testWrite"
QT_MOC_LITERAL(4, 34, 4), // "byte"
QT_MOC_LITERAL(5, 39, 9), // "WriteData"
QT_MOC_LITERAL(6, 49, 4), // "data"
QT_MOC_LITERAL(7, 54, 10), // "ChangeData"
QT_MOC_LITERAL(8, 65, 18), // "OnChangedOutputBox"
QT_MOC_LITERAL(9, 84, 16), // "ViewSignalsTimer"
QT_MOC_LITERAL(10, 101, 17), // "OnChangedCounter0"
QT_MOC_LITERAL(11, 119, 17), // "OnChangedCounter1"
QT_MOC_LITERAL(12, 137, 17), // "OnChangedCounter2"
QT_MOC_LITERAL(13, 155, 17), // "OnChangedCounter3"
QT_MOC_LITERAL(14, 173, 16), // "OnEnableCounter0"
QT_MOC_LITERAL(15, 190, 16), // "OnEnableCounter1"
QT_MOC_LITERAL(16, 207, 16), // "OnEnableCounter2"
QT_MOC_LITERAL(17, 224, 16) // "OnEnableCounter3"

    },
    "ControlCardMB\0ReadData\0\0testWrite\0"
    "byte\0WriteData\0data\0ChangeData\0"
    "OnChangedOutputBox\0ViewSignalsTimer\0"
    "OnChangedCounter0\0OnChangedCounter1\0"
    "OnChangedCounter2\0OnChangedCounter3\0"
    "OnEnableCounter0\0OnEnableCounter1\0"
    "OnEnableCounter2\0OnEnableCounter3"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ControlCardMB[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x0a /* Public */,
       3,    1,   85,    2, 0x0a /* Public */,
       5,    1,   88,    2, 0x0a /* Public */,
       7,    0,   91,    2, 0x0a /* Public */,
       8,    1,   92,    2, 0x0a /* Public */,
       9,    0,   95,    2, 0x0a /* Public */,
      10,    0,   96,    2, 0x0a /* Public */,
      11,    0,   97,    2, 0x0a /* Public */,
      12,    0,   98,    2, 0x0a /* Public */,
      13,    0,   99,    2, 0x0a /* Public */,
      14,    0,  100,    2, 0x0a /* Public */,
      15,    0,  101,    2, 0x0a /* Public */,
      16,    0,  102,    2, 0x0a /* Public */,
      17,    0,  103,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::LongLong,    4,
    QMetaType::Void, QMetaType::QByteArray,    6,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void ControlCardMB::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<ControlCardMB *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->ReadData(); break;
        case 1: _t->testWrite((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 2: _t->WriteData((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 3: _t->ChangeData(); break;
        case 4: _t->OnChangedOutputBox((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->ViewSignalsTimer(); break;
        case 6: _t->OnChangedCounter0(); break;
        case 7: _t->OnChangedCounter1(); break;
        case 8: _t->OnChangedCounter2(); break;
        case 9: _t->OnChangedCounter3(); break;
        case 10: _t->OnEnableCounter0(); break;
        case 11: _t->OnEnableCounter1(); break;
        case 12: _t->OnEnableCounter2(); break;
        case 13: _t->OnEnableCounter3(); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ControlCardMB::staticMetaObject = { {
    &QWidget::staticMetaObject,
    qt_meta_stringdata_ControlCardMB.data,
    qt_meta_data_ControlCardMB,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *ControlCardMB::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ControlCardMB::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ControlCardMB.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int ControlCardMB::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

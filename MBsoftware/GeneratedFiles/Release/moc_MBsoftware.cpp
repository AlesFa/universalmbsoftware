/****************************************************************************
** Meta object code from reading C++ file 'MBsoftware.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.13.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "stdafx.h"
#include "../../MBsoftware.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MBsoftware.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.13.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MBsoftware_t {
    QByteArrayData data[42];
    char stringdata0[652];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MBsoftware_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MBsoftware_t qt_meta_stringdata_MBsoftware = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MBsoftware"
QT_MOC_LITERAL(1, 11, 7), // "saveRef"
QT_MOC_LITERAL(2, 19, 0), // ""
QT_MOC_LITERAL(3, 20, 7), // "station"
QT_MOC_LITERAL(4, 28, 5), // "index"
QT_MOC_LITERAL(5, 34, 9), // "ViewTimer"
QT_MOC_LITERAL(6, 44, 10), // "ViewTimer2"
QT_MOC_LITERAL(7, 55, 12), // "OnFrameReady"
QT_MOC_LITERAL(8, 68, 14), // "OnTcpDataReady"
QT_MOC_LITERAL(9, 83, 5), // "parse"
QT_MOC_LITERAL(10, 89, 11), // "HistoryCall"
QT_MOC_LITERAL(11, 101, 12), // "currentPiece"
QT_MOC_LITERAL(12, 114, 22), // "OnClickedShowLptButton"
QT_MOC_LITERAL(13, 137, 22), // "OnClickedShowCamButton"
QT_MOC_LITERAL(14, 160, 28), // "OnClickedShowSmartCardButton"
QT_MOC_LITERAL(15, 189, 28), // "OnClickedShowImageProcessing"
QT_MOC_LITERAL(16, 218, 19), // "OnClickedSelectType"
QT_MOC_LITERAL(17, 238, 18), // "OnClickedEditTypes"
QT_MOC_LITERAL(18, 257, 20), // "OnClickedRemoveTypes"
QT_MOC_LITERAL(19, 278, 17), // "OnClickedAddTypes"
QT_MOC_LITERAL(20, 296, 18), // "OnClickedTeachType"
QT_MOC_LITERAL(21, 315, 22), // "OnClickedResetCounters"
QT_MOC_LITERAL(22, 338, 14), // "OnClickedLogin"
QT_MOC_LITERAL(23, 353, 18), // "OnClickedStatusBox"
QT_MOC_LITERAL(24, 372, 18), // "OnClickedSmcButton"
QT_MOC_LITERAL(25, 391, 20), // "OnClickedShowHistory"
QT_MOC_LITERAL(26, 412, 24), // "OnClickedGeneralSettings"
QT_MOC_LITERAL(27, 437, 21), // "OnClickedResetGoodBox"
QT_MOC_LITERAL(28, 459, 16), // "OnClickedAboutUs"
QT_MOC_LITERAL(29, 476, 17), // "onClientReadyRead"
QT_MOC_LITERAL(30, 494, 21), // "OnClickedTCPconnction"
QT_MOC_LITERAL(31, 516, 13), // "SaveVariables"
QT_MOC_LITERAL(32, 530, 13), // "LoadVariables"
QT_MOC_LITERAL(33, 544, 6), // "AddLog"
QT_MOC_LITERAL(34, 551, 4), // "text"
QT_MOC_LITERAL(35, 556, 4), // "type"
QT_MOC_LITERAL(36, 561, 14), // "WriteLogToFile"
QT_MOC_LITERAL(37, 576, 4), // "path"
QT_MOC_LITERAL(38, 581, 17), // "WriteGlobCounters"
QT_MOC_LITERAL(39, 599, 22), // "WriteCurrentBoxCounter"
QT_MOC_LITERAL(40, 622, 12), // "EraseLastLOG"
QT_MOC_LITERAL(41, 635, 16) // "RefPieceMeasured"

    },
    "MBsoftware\0saveRef\0\0station\0index\0"
    "ViewTimer\0ViewTimer2\0OnFrameReady\0"
    "OnTcpDataReady\0parse\0HistoryCall\0"
    "currentPiece\0OnClickedShowLptButton\0"
    "OnClickedShowCamButton\0"
    "OnClickedShowSmartCardButton\0"
    "OnClickedShowImageProcessing\0"
    "OnClickedSelectType\0OnClickedEditTypes\0"
    "OnClickedRemoveTypes\0OnClickedAddTypes\0"
    "OnClickedTeachType\0OnClickedResetCounters\0"
    "OnClickedLogin\0OnClickedStatusBox\0"
    "OnClickedSmcButton\0OnClickedShowHistory\0"
    "OnClickedGeneralSettings\0OnClickedResetGoodBox\0"
    "OnClickedAboutUs\0onClientReadyRead\0"
    "OnClickedTCPconnction\0SaveVariables\0"
    "LoadVariables\0AddLog\0text\0type\0"
    "WriteLogToFile\0path\0WriteGlobCounters\0"
    "WriteCurrentBoxCounter\0EraseLastLOG\0"
    "RefPieceMeasured"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MBsoftware[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      33,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    2,  179,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,  184,    2, 0x08 /* Private */,
       6,    0,  185,    2, 0x08 /* Private */,
       7,    2,  186,    2, 0x08 /* Private */,
       8,    1,  191,    2, 0x08 /* Private */,
      10,    2,  194,    2, 0x08 /* Private */,
      12,    1,  199,    2, 0x08 /* Private */,
      13,    1,  202,    2, 0x08 /* Private */,
      14,    1,  205,    2, 0x08 /* Private */,
      15,    0,  208,    2, 0x08 /* Private */,
      16,    0,  209,    2, 0x08 /* Private */,
      17,    0,  210,    2, 0x08 /* Private */,
      18,    0,  211,    2, 0x08 /* Private */,
      19,    0,  212,    2, 0x08 /* Private */,
      20,    0,  213,    2, 0x08 /* Private */,
      21,    0,  214,    2, 0x08 /* Private */,
      22,    0,  215,    2, 0x08 /* Private */,
      23,    0,  216,    2, 0x08 /* Private */,
      24,    0,  217,    2, 0x08 /* Private */,
      25,    0,  218,    2, 0x08 /* Private */,
      26,    0,  219,    2, 0x08 /* Private */,
      27,    0,  220,    2, 0x08 /* Private */,
      28,    0,  221,    2, 0x08 /* Private */,
      29,    0,  222,    2, 0x08 /* Private */,
      30,    1,  223,    2, 0x08 /* Private */,
      31,    0,  226,    2, 0x08 /* Private */,
      32,    0,  227,    2, 0x08 /* Private */,
      33,    2,  228,    2, 0x08 /* Private */,
      36,    3,  233,    2, 0x08 /* Private */,
      38,    0,  240,    2, 0x08 /* Private */,
      39,    0,  241,    2, 0x08 /* Private */,
      40,    0,  242,    2, 0x08 /* Private */,
      41,    2,  243,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    3,    4,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    2,    2,
    QMetaType::Void, QMetaType::Int,    9,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    3,   11,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString, QMetaType::Int,   34,   35,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::Int,   37,   34,   35,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,    3,    4,

       0        // eod
};

void MBsoftware::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MBsoftware *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->saveRef((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: _t->ViewTimer(); break;
        case 2: _t->ViewTimer2(); break;
        case 3: _t->OnFrameReady((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 4: _t->OnTcpDataReady((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->HistoryCall((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 6: _t->OnClickedShowLptButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->OnClickedShowCamButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->OnClickedShowSmartCardButton((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->OnClickedShowImageProcessing(); break;
        case 10: _t->OnClickedSelectType(); break;
        case 11: _t->OnClickedEditTypes(); break;
        case 12: _t->OnClickedRemoveTypes(); break;
        case 13: _t->OnClickedAddTypes(); break;
        case 14: _t->OnClickedTeachType(); break;
        case 15: _t->OnClickedResetCounters(); break;
        case 16: _t->OnClickedLogin(); break;
        case 17: _t->OnClickedStatusBox(); break;
        case 18: _t->OnClickedSmcButton(); break;
        case 19: _t->OnClickedShowHistory(); break;
        case 20: _t->OnClickedGeneralSettings(); break;
        case 21: _t->OnClickedResetGoodBox(); break;
        case 22: _t->OnClickedAboutUs(); break;
        case 23: _t->onClientReadyRead(); break;
        case 24: _t->OnClickedTCPconnction((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 25: _t->SaveVariables(); break;
        case 26: _t->LoadVariables(); break;
        case 27: _t->AddLog((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 28: _t->WriteLogToFile((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        case 29: _t->WriteGlobCounters(); break;
        case 30: _t->WriteCurrentBoxCounter(); break;
        case 31: _t->EraseLastLOG(); break;
        case 32: _t->RefPieceMeasured((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (MBsoftware::*)(int , int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&MBsoftware::saveRef)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MBsoftware::staticMetaObject = { {
    &QMainWindow::staticMetaObject,
    qt_meta_stringdata_MBsoftware.data,
    qt_meta_data_MBsoftware,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MBsoftware::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MBsoftware::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MBsoftware.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MBsoftware::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 33)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 33;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 33)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 33;
    }
    return _id;
}

// SIGNAL 0
void MBsoftware::saveRef(int _t1, int _t2)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t1))), const_cast<void*>(reinterpret_cast<const void*>(std::addressof(_t2))) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE

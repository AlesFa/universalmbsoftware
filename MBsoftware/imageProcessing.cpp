﻿#include "stdafx.h"
#include "imageProcessing.h"
#include "math.h"
#include "CLine.h"
#include "CPointFloat.h"
#include "CRectRotated.h"
#include "CCircle.h"
#include <conio.h>





std::vector<CCamera*>										imageProcessing::cam;
std::vector<CMemoryBuffer*>									imageProcessing::images;
CMemoryBuffer*												imageProcessing::currImage;
std::vector<Types*>											imageProcessing::types[NR_STATIONS];
Measurand*													imageProcessing::measureObject;
std::vector<Timer*>											imageProcessing::processingTimer;

Mat															imageProcessing::DrawImage;
QPoint														imageProcessing::origin;




imageProcessing::imageProcessing(QWidget *parent)
	: QWidget(parent)
{


	ui.setupUi(this);
	zoomFactor = 1;
	displayImage = 0;
	displayWindow = 0;
	prevDisplayImage = 0;
	prevDisplayWindow = 0;
	rowNum = 10;
	colNum = 4;
	currentStation = 0;
	isSceneAdded = false;


	ui.lineInsertImageIndex->setText("0");
	
	connect(ui.buttonImageUp, SIGNAL(pressed()), this, SLOT(OnPressedImageUp()));
	connect(ui.buttonImageDown, SIGNAL(pressed()), this, SLOT(OnPressedImageDown()));
	connect(ui.buttonCamDown, SIGNAL(pressed()), this, SLOT(OnPressedCamDown()));
	connect(ui.buttonCamUp, SIGNAL(pressed()), this, SLOT(OnPressedCamUp()));
	connect(ui.buttonZoomIn, SIGNAL(pressed()), this, SLOT(ZoomIn()));
	connect(ui.buttonReset, SIGNAL(pressed()), this, SLOT(ZoomReset()));
	connect(ui.buttonZoomOut, SIGNAL(pressed()), this, SLOT(ZoomOut()));
	connect(ui.buttonLoadImage, SIGNAL(pressed()), this, SLOT(OnLoadImage()));
	connect(ui.buttonLoadMultipleImages, SIGNAL(pressed()), this, SLOT(OnLoadMultipleImage()));
	connect(ui.buttonSaveImage, SIGNAL(pressed()), this, SLOT(OnSaveImage()));
	connect(ui.buttonProcessCam0, SIGNAL(pressed()), this, SLOT(OnProcessCam0()));
	connect(ui.buttonProcessCam1, SIGNAL(pressed()), this, SLOT(OnProcessCam1()));
	connect(ui.buttonProcessCam2, SIGNAL(pressed()), this, SLOT(OnProcessCam2()));
	connect(ui.buttonProcessCam3, SIGNAL(pressed()), this, SLOT(OnProcessCam3()));
	connect(ui.buttonProcessCam4, SIGNAL(pressed()), this, SLOT(OnProcessCam4()));


	connect(ui.addParameter, SIGNAL(pressed()), this, SLOT(onPressedAddParameter()));
	connect(ui.removeParameter, SIGNAL(pressed()), this, SLOT(onPressedRemoveParameters()));
	connect(ui.update, SIGNAL(pressed()), this, SLOT(onPressedUpdateParameters()));
	connect(ui.buttonSurfaceInspection, SIGNAL(pressed()), this, SLOT(onPressedSurfaceInspection()));
	connect(ui.imageView, SIGNAL(pressed()), this, SLOT(mousePressEvent()));
	connect(ui.imageView, SIGNAL(released()), this, SLOT(mouseReleaseEvent()));
	connect(ui.lineInsertImageIndex, SIGNAL(editingFinished()), this, SLOT(OnDoneEditingLineInsertImageIndex()));



	for (int i = 0; i < 7; i++)
	{
		processingTimer.push_back(new Timer());
	}
	rubberBand = new QRubberBand(QRubberBand::Rectangle, ui.imageView);//new rectangle band

	ui.imageView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	start = 0;


	variantManager = new QtVariantPropertyManager();
	

	topItem = variantManager->addProperty(QtVariantPropertyManager::groupTypeId(),
		QLatin1String("Function property"));

	variantFactory = new QtVariantEditorFactory();

	variantEditor = new QtTreePropertyBrowser();
	variantEditor->setFactoryForManager(variantManager, variantFactory);
	variantEditor->addProperty(topItem);
	variantEditor->setPropertiesWithoutValueMarked(true);
	variantEditor->setResizeMode(QtTreePropertyBrowser::ResizeMode::ResizeToContents);
	variantEditor->setRootIsDecorated(true);
	ui.ParameterLayout->addWidget(variantEditor);


	for (int i = 0; i < 50; i++) //za izpise kaj vracajo funkcije
	{
		ui.listFunctionsReturn->addItem("");
	}



}

void imageProcessing::closeEvent(QCloseEvent * event)
{
	ClearDraw();
}

imageProcessing::~imageProcessing()
{
	delete variantManager;
	delete variantFactory;
	delete variantEditor;
	qDeleteAll(scene->items());
	item.erase(item.begin(), item.end());
	cam.clear();
	delete scene;

	//delete item;
	//delete topItem;

}

void imageProcessing::ConnectImages(std::vector<CCamera*> inputCam, std::vector<CMemoryBuffer*> inputImages)
{

	cam = inputCam;
	images = inputImages;
	currImage = &cam[0]->image[0];
	displayWindow =0;
	displayImage = 0;

	ResizeDisplayRect();
	ShowImages();

}

void imageProcessing::ConnectMeasurand(Measurand * objects)
{

	measureObject = objects;
	
}

void imageProcessing::ConnectTypes(int station,std::vector<Types*> type)
{

		types[station] = type;

}


void imageProcessing::ConnectImages(std::vector<CCamera*> inputCam)
{
	cam = inputCam;

}


void imageProcessing::ShowDialog(int rights)
{	
	loginRights = rights;
	ReplaceCurrentBuffer();
	activateWindow();
	currentFunction = -1;
	for (int i = 0; i < item.size(); i++)
	{
		topItem->removeSubProperty(item[i]);
	}
	item.clear();
	//Če uporabnik nima administratorskih pravic se onemogoči nastavljanje parametrov slik
	ui.buttonProcessCam4->setEnabled(false);
	if ((rights == 1)|| (rights == 2))
	{
		if (rights == 1)
		{
			ui.addParameter->setEnabled(true);
			ui.removeParameter->setEnabled(true);
			ui.buttonProcessCam0->setEnabled(true);
			ui.buttonProcessCam1->setEnabled(true);
			ui.buttonProcessCam2->setEnabled(true);
			ui.buttonProcessCam3->setEnabled(true);

		}
		else
		{
			ui.addParameter->setEnabled(false);
			ui.removeParameter->setEnabled(false);
			ui.buttonProcessCam0->setEnabled(false);
			ui.buttonProcessCam1->setEnabled(false);
			ui.buttonProcessCam2->setEnabled(true);
			ui.buttonProcessCam3->setEnabled(false);

		}
		ui.update->setEnabled(true);
		ui.Cancel->setEnabled(true);

		
		ui.buttonSurfaceInspection->setEnabled(false);
	}

	else
	{
		ui.addParameter->setEnabled(false);
		ui.removeParameter->setEnabled(false);
		ui.update->setEnabled(false);
		ui.Cancel->setEnabled(false);
		ui.buttonProcessCam0->setEnabled(false);
		ui.buttonProcessCam1->setEnabled(false);
		ui.buttonProcessCam2->setEnabled(true);
		ui.buttonProcessCam3->setEnabled(false);
		ui.buttonSurfaceInspection->setEnabled(false);
	}
		setWindowState( Qt::WindowActive);
	show();
}

void imageProcessing::ClearDialog()
{
	int camNum = displayWindow;
	int imgNum = displayImage;;


	//Pobrišemo razporeditve parametrov
	

}


bool imageProcessing::eventFilter(QObject *obj, QEvent *event)
{
	uchar b = 0, g = 0, r = 0;
	QPoint point_mouse;
	QGraphicsLineItem * item;


	//QGraphicsScene::mousePressEvent(event);
	int x, y;

	if (event->type() == Qt::LeftButton)
	{
		int bla = 100;
	}

	if (event->type() == QEvent::MouseMove)
	{
		QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
		QPointF position = mouseEvent->pos();
		QPoint pos = QCursor::pos();


		QPointF scenePt = ui.imageView->mapToScene(mouseEvent->pos());

		//point_mouse = ui.imageView->mapFromScene(QCursor::pos());


		x = scenePt.x();
		y = scenePt.y();
		ui.labelX->setText(QString("X = %1").arg(x));
		ui.labelY->setText(QString("Y = %1").arg(y));

		if (currImage->buffer->empty())
			return false;

		if ((x < currImage->buffer[0].cols) && (y < currImage->buffer[0].rows) && (x > -1) && (y > -1))
		{
			if (currImage->buffer->channels() == 1)
			{
				b = g = r = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x)];
			}
			else
			{
				b = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x) + 0];
				g = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x) + 1];
				r = currImage->buffer[0].data[currImage->buffer[0].channels()*(currImage->buffer[0].cols*y + x) + 2];
			}


			ui.labelG->setText(QString("G = %1").arg(g));
			ui.labelB->setText(QString("B = %1").arg(b));
			ui.labelR->setText(QString("R = %1 ").arg(r));


			return false;
		}


		return false;
	}


	if (event->type() == QEvent::MouseButtonRelease)
	{
		const QGraphicsSceneMouseEvent* const me = static_cast<const QGraphicsSceneMouseEvent*>(event);
		int bla = 10;


		


		return true;
	}

	return false;
}


void imageProcessing::OnLoadImage()
{
	Mat img;
	QString fileName = QFileDialog::getOpenFileName(this, tr("Load Image"), "C://", "bmp image (*.bmp)");
	String name;

	int channel;
	if (!fileName.isEmpty())
	{
		QMessageBox::information(this, tr("file loaded"), fileName);
		name = fileName.toStdString();

		img = imread(name, -1);
		*currImage->buffer = img;
		ReplaceCurrentBuffer();
	}


	

}

void imageProcessing::OnLoadMultipleImage()
{
	QFileDialog dialog(this);
	Mat img;
	String name;
	dialog.setDirectory(QDir::homePath());
	dialog.setFileMode(QFileDialog::ExistingFiles);
	dialog.setNameFilter(trUtf8("image file(*.bmp)"));
	dialog.setDirectory("C://");
	QStringList fileNames;
	if (dialog.exec())
		fileNames = dialog.selectedFiles();

	if (!fileNames.isEmpty())
	{
		for (int i = 0; i < fileNames.size(); i++)
		{
			name = fileNames[i].toStdString();
			img = imread(name, -1);
			if (displayWindow < cam.size())
			{
				if (i < cam[displayWindow]->image.size())
				{
					cam[displayWindow]->image[i].buffer[0] = img;
				}
			}
			else
			{
				if (i < images.size())
					*images[i]->buffer = img;

			}
		}
		QMessageBox::information(this, QString("file loaded"),QString("%1 files loaded").arg(fileNames.size()));
		ReplaceCurrentBuffer();
	}





}

void imageProcessing::OnSaveImage()
{
	
	//image.fill(Qt::red); // A red rectangle.
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Image "),
		QString(),
		tr("Images (*.bmp)"));

	String name;
	name = fileName.toStdString();
	imwrite(name, *currImage->buffer);
	/*
	if (!fileName.isEmpty())
	{
	const QPixmap pixmap = ui.imageView->grab();
	pixmap.save(fileName);
	QMessageBox::information(this, tr("file saved"), fileName);

	}*/

}




void imageProcessing::ZoomOut()
{
	zoomFactor = zoomFactor - 0.1;
	if (zoomFactor > 0.09)
	{
		ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));
		//ui.imageView->fitInView(QRectF(0, 0, currImage->buffer[0].cols *zoomFactor, currImage->buffer[0].rows *zoomFactor), Qt::KeepAspectRatioByExpanding);
		ui.imageView->scale(0.9, 0.9);
	}
	else
	{
		zoomFactor = 0.1;
		ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor * 100));
	}

}

void imageProcessing::ZoomIn()
{

	zoomFactor = zoomFactor + 0.1;

	ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor*100));
	ui.imageView->scale(1.1, 1.1);

}

void imageProcessing::ZoomReset()
{

	zoomFactor = 1;
	ui.imageView->resetTransform();
	ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor*100));
	
}


void imageProcessing::ResizeDisplayRect()
{	
	ui.imageView->setGeometry(QRect(0, 0, currImage->buffer->cols, currImage->buffer->rows));
	ui.imageView->setAlignment(Qt::AlignTop | Qt::AlignLeft);
	ui.imageView->setSceneRect(QRect(0, 0, currImage->buffer->cols, currImage->buffer->rows));
}




void imageProcessing::ClearDraw()
{
	//dodamo na draw sliko 
	qDeleteAll(scene->items());
	//qDeleteAll(sceneForMainWindow->items());
	QImage qimgOriginal((uchar*)DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);
	pixmapVideo = scene->addPixmap(QPixmap::fromImage(qimgOriginal));
	pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));
}
void imageProcessing::SetCurrentBuffer(int dispWindow, int dispImage)
{
	displayWindow = dispWindow;
	displayImage = dispImage;
	//ReplaceCurrentBuffer();
}

void imageProcessing::ReplaceCurrentBuffer()
{
	//if ((displayImage != prevDisplayImage) || (displayWindow != prevDisplayWindow))
	//{
		if (displayWindow == cam.size())
		{
			currImage = images[displayImage];
		}
		else
		{
			currImage = &cam[displayWindow]->image[displayImage];
		}

	if (displayWindow == cam.size())
	{
		ui.labelLocation->setText(QString("SAVED IMAGES: %1").arg(displayImage));
	}
	else
		ui.labelLocation->setText(QString("CAM: %1, IMAGE: %2").arg(displayWindow).arg(displayImage));

	ui.lineInsertImageIndex->setText(QString("%1").arg(displayImage));

	//if((displayImage != prevDisplayImage) || ( prevDisplayWindow != displayWindow)) // zaradi cudnega pojava na dialogu
	//ResizeDisplayRect();
	
	
	ClearDraw();
	ShowImages();


	prevDisplayImage = displayImage;
	prevDisplayWindow = displayWindow;

	if(displayWindow < cam.size())
	currentStation = displayWindow;



	ui.labelCurrStation->setText(QString("station: %1").arg(currentStation));
}



void imageProcessing::OnPressedImageUp()
{
	ClearDialog();
	displayImage++;
	if (displayWindow == cam.size())
	{
		if (displayImage > images.size()-1)
			displayImage = 0;
	}
	else
	{
		if (displayImage > cam[displayWindow]->num_images - 1)
			displayImage = 0;

	}
	ReplaceCurrentBuffer();
	//ReplaceParameters();

}

void imageProcessing::OnPressedImageDown()
{
	ClearDialog();
	displayImage--;
	if (displayWindow == cam.size())
	{
		if ((displayImage > images.size()) ||(displayImage < 0))
			displayImage = images.size()-1;
	}
	else
	{
		if((displayImage >= cam[displayWindow]->num_images - 1) || (displayImage < 0))
			displayImage = cam[displayWindow]->num_images - 1;

	}

	ReplaceCurrentBuffer();
	//ReplaceParameters();
}

void imageProcessing::OnPressedCamUp()
{	

	ClearDialog();
	displayImage = 0;
	displayWindow++;

	if ((displayWindow < 0) || (displayWindow > cam.size()))
		displayWindow = 0;

	ReplaceCurrentBuffer();
	//ReplaceParameters();
}

void imageProcessing::OnPressedCamDown()
{
	ClearDialog();
	displayImage = 0;
	displayWindow--;

	if ((displayWindow < 0) || (displayWindow > cam.size()))
		displayWindow = cam.size() ;

	ReplaceCurrentBuffer();
	
	//ReplaceParameters();
}

int imageProcessing::ConvertImageForDisplay(int imageNumber)
{
	Mat test;
	//Mat DrawImage;

	if (currImage->buffer->empty())
	{
		return 0;
	}

	if(currImage->buffer->channels() == 1)
		cvtColor(*currImage->buffer, test, COLOR_GRAY2RGB);
	else
		cvtColor(*currImage->buffer, test, COLOR_BGR2RGB);


	DrawImage = test(Rect(0, 0, test.cols, test.rows));
	
	//QImage qimgOriginal((uchar*)DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);
	
	//showScene->setPixmap(QPixmap::fromImage(qimgOriginal));

		return 1;
}

void imageProcessing::ShowImages()
{
	if (ConvertImageForDisplay(0))
	{
		QImage qimgOriginal((uchar*)DrawImage.data, DrawImage.cols, DrawImage.rows, DrawImage.step, QImage::Format_RGB888);

		if (!isSceneAdded)
		{
			scene = new QGraphicsScene(this);
			sceneForMainWindow = new QGraphicsScene(this);
			ui.imageView->setScene(scene);
			setMouseTracking(true);
			ui.imageView->viewport()->installEventFilter(this);
			//scene->installEventFilter(this);
			
			pixmapVideo = scene->addPixmap(QPixmap::fromImage(qimgOriginal));
			pixmapVideo->setZValue(-100);
			isSceneAdded = true;
		}

		pixmapVideo->setPixmap(QPixmap::fromImage(qimgOriginal));
		zoomFactor = 1;
		ui.imageView->resetTransform();
		ui.labelZoom->setText(QString("Zoom:%1%").arg(zoomFactor*100));
		//ui.imageView->fitInView(QRectF(0, 0, currImage->buffer[0].cols *zoomFactor, currImage->buffer[0].rows *zoomFactor), Qt::KeepAspectRatioByExpanding);
	}
}
void imageProcessing::OnProcessCam0()
{
	currentFunction = 0;
	ClearDraw();
	PopulatePropertyBrowser(currentFunction);
	int imageNum = 0;
	bool ok;

	

	if (displayWindow < cam.size())
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera0(displayWindow, imageNum, 1);
		}
		else
		{
			imageNum = 0;
			ProcessingCamera0(displayWindow, imageNum, 1);
		}
	}


}
void imageProcessing::OnProcessCam1()//trenutno uporabljena za zdruzitev kosov
{
	int imageNum = 0;
	bool ok;
	ClearDraw();
	currentFunction = 1;
	PopulatePropertyBrowser(currentFunction);

	if (displayWindow < cam.size())
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera1(displayWindow, imageNum, 1);
		}
		else
		{
			imageNum = 0;
			ProcessingCamera1(displayWindow, imageNum, 1);
		}
	}
	else
	{
		ProcessingCamera1(displayWindow, imageNum, 1);

	}

}

void imageProcessing::OnProcessCam2()
{
	currentFunction = 2;
	ClearDraw();
	PopulatePropertyBrowser(currentFunction);

	int imageNum;
	bool ok;

	if (displayWindow < cam.size())
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera2(displayWindow, imageNum, 1);
		}
		else
		{
			/*imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, images.size() - 1, 1, &ok, NULL);

			if (ok)
				ProcessingCamera2(displayWindow, imageNum, 1);*/
		}
	}
	

}

void imageProcessing::OnProcessCam3()
{
	currentFunction = 3;
	PopulatePropertyBrowser(currentFunction);
		//ProcessingCamera3(0, 0, 1);


	int imageNum = 0;
	bool ok;

	if (displayWindow <= cam.size()-1)
	{
		if (cam[displayWindow]->image.size() > 1)
		{
			imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, cam[displayWindow]->image.size() - 1, 1, &ok, NULL);
			if (ok)
				ProcessingCamera3(displayWindow, imageNum, 1);
		}
		else
			ProcessingCamera3(displayWindow, imageNum, 1);
	}
	else
	{
		imageNum = QInputDialog::getInt(this, tr("selectImage"), tr("image:"), 0, 0, images.size()- 1, 1, &ok, NULL);

		if (ok)
			ProcessingCamera3(displayWindow, imageNum, 1);
	}


}


void imageProcessing::OnProcessCam4()
{
	currentFunction = 4;
	PopulatePropertyBrowser(currentFunction);

	ProcessingCamera4(displayWindow, displayImage, 1);
}

void imageProcessing::OnDoneEditingLineInsertImageIndex()
{
	int bla = 100;
	QString index = ui.lineInsertImageIndex->text();
	int indexnum = index.toInt();
	
	int size = images.size();
	if (displayWindow == cam.size())
	{
		if (indexnum > size - 1)
			displayImage = size - 1;
		else if (indexnum < 0)
			displayImage = 0;
		else
			displayImage = indexnum;

	}

	else
	{
		if (indexnum > cam[displayWindow]->num_images - 1)
			displayImage = cam[displayWindow]->num_images - 1;
		else if (indexnum < 0)
			displayImage = 0;
		else
			displayImage = indexnum;

	}


	
	ReplaceCurrentBuffer();

	
}
/*void imageProcessing::Addparameter(int paramNum, QString paramName, float paramValue, int row, int col)
{
	QString value;

	//Preverimo trenutno kamero in sliko na kateri se nahajamo
	int camNum = displayWindow;
	int imgNum = displayImage;

	while (gridLayout.size() <= paramNum)
		gridLayout.push_back(new QGridLayout());
	gridLayout[paramNum]->setHorizontalSpacing(5);
	gridLayout[paramNum]->setVerticalSpacing(5);
	gridLayout[paramNum]->setObjectName(paramName);
	gridLayout[paramNum]->setSizeConstraint(QLayout::SetDefaultConstraint);
	gridLayout[paramNum]->setAlignment(Qt::AlignCenter);

	//Tvorimo oznake
	while (paramLabels.size() <= paramNum)
		paramLabels.push_back(new QLabel(ui.gridLayoutWidget));
	paramLabels[paramNum]->setObjectName(paramName);
	paramLabels[paramNum]->setMinimumSize(QSize(70, 20));
	paramLabels[paramNum]->setMaximumSize(QSize(70, 20));
	paramLabels[paramNum]->setAlignment(Qt::AlignLeft);
	paramLabels[paramNum]->setText(paramName);
	gridLayout[paramNum]->addWidget(paramLabels[paramNum]);

	//Tvorimo tekstovna polja za vrednost parametrov
	while (paramLineEdit.size() <= paramNum)
		paramLineEdit.push_back(new QLineEdit(ui.gridLayoutWidget));
	paramLineEdit[paramNum]->setObjectName(paramName);
	paramLineEdit[paramNum]->setMinimumSize(QSize(50, 20));
	paramLineEdit[paramNum]->setMaximumSize(QSize(50, 20));
	paramLineEdit[paramNum]->setAlignment(Qt::AlignLeft);
	paramLineEdit[paramNum]->setText(value.setNum(paramValue));
	paramLineEdit[paramNum]->show();
	gridLayout[paramNum]->addWidget(paramLineEdit[paramNum]);

	//Oznake in tekstovna polja razporedimo na grafičnem vmesniku
	ui.ParameterLayout->addLayout(gridLayout[paramNum], row, col, -1,3, Qt::AlignLeft|Qt::AlignTop);
	ui.scrollArea->widget()->setLayout(ui.ParameterLayout);

}*/



/*void imageProcessing::RemoveParameters()
{
	int camNum = displayWindow;
	int imgNum = displayImage;
	int row, col, rowSpan, colSpan, newRow, newCol, paramNum, newParamNum, colNumber;

	QInputDialog removeParameterName;
	QString paramName;
	QString insertedName;
	//Pot do konfiguracijskih datotek
	//Potrebno je dodati odvisnost od TRENUTNIH tipov in postaj!
	QString filePath = QDir::currentPath() + QString("/%1/DynamicVariables/types%2Station%3/camera%4/img%5_parameters.ini").arg(REF_FOLDER_NAME).arg(0).arg(NR_STATIONS).arg(camNum).arg(imgNum).arg(imgNum);
	QStringList valueList;
	QSize dlgSize(250, 250);
	QSettings settings(filePath, QSettings::IniFormat);

	//Nastavitve dialoga
	removeParameterName.setWindowTitle("Remove parameter");
	removeParameterName.resize(dlgSize);
	removeParameterName.setLabelText("Insert parameter name");
	removeParameterName.setInputMode(removeParameterName.TextInput);
	//Preberemo ime parametra
	if(removeParameterName.exec() == 1);
	{
		paramName = removeParameterName.textValue();
		paramNum = CountParameters();

		//Poiščemo indeks parametra in ga izbrišemo iz tabele
		for (int i = 0; i < paramNum; i++)
		{
			insertedName = paramLabels[i]->text();
			if (insertedName == paramName)
			{
				//Izbrišemo parameter iz tabele parametrov
				ui.ParameterLayout->getItemPosition(i, &row, &col, &rowSpan, &colSpan);
				parameterTable[0][0][camNum][imgNum][row].erase(parameterTable[0][0][camNum][imgNum][row].begin() + col);
				colNumber = parameterTable[0][0][camNum][imgNum][row].size();

				//Pobrišemo oznako in okence za najden indeks
				ui.ParameterLayout->removeItem(gridLayout[i]);
				paramLabels[i]->hide();
				paramLineEdit[i]->hide();
				paramLabels.erase(paramLabels.begin() + i);
				paramLineEdit.erase(paramLineEdit.begin() + i);
				gridLayout.erase(gridLayout.begin() + i);

				//Pobrišemo parameter iz datoteke
				settings.remove(insertedName);
				settings.sync();
				//Če smo izbrisali zadnji parameter datoteko izbrišemo
				valueList = settings.allKeys();
				if (valueList.size() == 0)
				{
					QFile configFile(filePath);
					configFile.remove();
				}

				//Prerazporedimo parametre
				for (int j = col; j < colNumber; j++)
				{
					newParamNum = CountParameters();
					for (int k = 0; k < newParamNum; k++)
					{
						ui.ParameterLayout->getItemPosition(k, &newRow, &newCol, &rowSpan, &colSpan);
						if (newRow == row && newCol == j + 1)
						{
							SaveParameters(camNum, imgNum, k, newRow, j);
						}
					}
				}

				//Novo razporejene parametre prikažemo
				ClearDialog();
				ReplaceCurrentBuffer();
				ReplaceParameters();

				return;
			}
		}
	}
}*/

/*void imageProcessing::UpdateParameters()
{
	int camNum = displayWindow;
	int imgNum = displayImage;
	int paramNum = paramLineEdit.size();
	int row, col, rowSpan, colSpan;

	//Pot do konfugiracijskih datotek
	QString dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/types%2Station%3/camera%4").arg(REF_FOLDER_NAME).arg(0).arg(NR_STATIONS).arg(camNum).arg(imgNum);
	QDir dir;

	ImageParameter imgParameter;

	dir.setPath(dirPath);
	if (dir.exists())
	{	
		for (int i = 0; i < paramNum; i++)
		{
			//Posodobimo tabelo parametrov in shranimo posodobljene vrednosti
			ui.ParameterLayout->getItemPosition(i, &row, &col, &rowSpan, &colSpan);
			parameterTable[0][0][camNum][imgNum][row][col] = paramLineEdit[i]->text().toDouble();
			//SaveParameters(camNum, imgNum, i, row, col);
		}
	}
}*/



void imageProcessing::onPressedAddParameter()
{
	bool ok;
	int insertNum = 0;
	
	if (currentFunction > -1)
	{
		insertNum = QInputDialog::getInt(this, tr("QInputDialog::setNumParam()"),
			tr("insert num:"), item.size(), 0, item.size(), 1, &ok);
		if (ok)
		{

			QStringList selectableTypes;
			selectableTypes << tr("bool parameter") << tr("Int Parameter") << tr("Double Parameter") << tr("rectangle");
			int propType;

			QString parameterType = QInputDialog::getItem(this, tr("parameter Type select"),
				tr("Select:"), selectableTypes, 0, false, &ok);

			if (ok)
			{
				QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),
					tr("Parameter Name:"), QLineEdit::Normal, tr("name"), &ok);
				if (ok)
				{
					for (int i = 0; i < 4; i++)
					{
						if (parameterType.compare(selectableTypes[i]) == 0)
						{
							if (i == 0)//bool value
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Bool, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction][insertNum]->boolValue);
								item[insertNum]->setEnabled(true);
							}
							else if (i == 1)//int value
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Int, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->intValue);
								item[insertNum]->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[currentFunction].back()->intMinValue);
								item[insertNum]->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[currentFunction].back()->intMaxValue);
								item[insertNum]->setAttribute(QLatin1String("singleStep"), 10);
								item[insertNum]->setEnabled(true);



							}
							else if (i == 2)
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Double, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->doubleValue);
								item[insertNum]->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[currentFunction].back()->doubleMinValue);
								item[insertNum]->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[currentFunction].back()->doubleMaxValue);
								item[insertNum]->setAttribute(QLatin1String("singleStep"), 10);
								item[insertNum]->setEnabled(true);

							}
							else if (i == 3)
							{
								types[currentStation][currentType]->prop[currentFunction].insert(types[currentStation][currentType]->prop[currentFunction].begin() + insertNum, new CProperty());
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propName = text;
								types[currentStation][currentType]->prop[currentFunction][insertNum]->propType = i;
								item.insert(item.begin() + insertNum, variantManager->addProperty(QVariant::Double, QString("%1 ").arg(insertNum) + types[currentStation][currentType]->prop[currentFunction][insertNum]->propName));
								item[insertNum]->setValue(types[currentStation][currentType]->prop[currentFunction].back()->doubleValue);
								item[insertNum]->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[currentFunction].back()->doubleMinValue);
								item[insertNum]->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[currentFunction].back()->doubleMaxValue);
								item[insertNum]->setAttribute(QLatin1String("singleStep"), 10);
								item[insertNum]->setEnabled(true);

							}


						}
					}
					//item.insert(item.begin() + 2, item.back());
					//topItem->addSubProperty(item[insertNum]);
					PopulatePropertyBrowser(currentFunction);
				}
			}
		}
	}
	else
	{
		 QMessageBox::information(this, tr("information"), "No function is selected");
	}
		
	//! [2]

	/*QInputDialog addParameterName, addParameterValue, addRow, addColumn;
	QSize dlgSize(250, 250);
	QString paramName;
	ImageParameter parameter;

	int cam_num = displayWindow;
	int img_num = displayImage;
	int row, col, param_num;
	float paramValue;

	//Nastavimo parametre prvega dialoga
	addParameterName.setWindowTitle("Add new parameter");
	addParameterName.resize(dlgSize);
	addParameterName.setLabelText("Insert parameter name");
	addParameterName.setInputMode(addParameterName.TextInput);

	//Preverimo je uporabnik potrdil vnos imena novega parametra
	if (addParameterName.exec() == 1)
	{
		//Preverimo če ime že obstaja
		paramName = addParameterName.textValue();
		if (!CheckParameterName(paramName))
		{
			QMessageBox msg;

			msg.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
			msg.setIcon(QMessageBox::Warning);
			msg.setText("<center>Invalid paramater name<center>");
			msg.setText("<p align='center'>Invalid paramter name</p>");
			msg.exec();

			return;
		}
		// Nastavimo parametre drugega dialoga
		addParameterValue.setWindowTitle("Add new parameter");
		addParameterValue.resize(dlgSize);
		addParameterValue.setLabelText("Insert parameter value");
		addParameterValue.setInputMode(addParameterValue.DoubleInput);
		addParameterValue.setDoubleRange(0.0, 10000.0);

		//Preverimo ali je uporabnik potrdil vnos vrednosti novega parametra
		if(addParameterValue.exec() == 1);
		{
			//Vrstica v kateri se bo parameter prikazal
			addRow.setWindowTitle("Row number");
			addRow.resize(dlgSize);
			addRow.setLabelText("Insert row number");
			addRow.setInputMode(addParameterValue.IntInput);
			addRow.setIntRange(0, rowNum - 1);

			if (addRow.exec() == 1)
			{

				//Prebermo vrstico in stolpec novega parametra
				row = addRow.intValue();
				col = parameterTable[0][0][cam_num][img_num][row].size();
				paramValue = addParameterValue.doubleValue();
				//Preverimo če je vrsta že zasedena
				if (col == colNum)
				{
					do
					{
						row++;
						col = parameterTable[0][0][cam_num][img_num][row].size();

					} while (col == colNum);
				}
				//Dodamo nov parameter in ga shranimo
				parameterTable[0][0][cam_num][img_num][row].push_back(paramValue);
				param_num = CountParameters();
				Addparameter(param_num - 1, paramName, paramValue, row, col);
				col = parameterTable[0][0][cam_num][img_num][row].size() - 1;
				SaveParameters(cam_num, img_num, param_num - 1, row, col);
			}
		}
	}*/
}

void imageProcessing::onPressedRemoveParameters()
{
	int deleteNum = 0;
	bool ok;
	if (currentFunction > -1)
	{
		deleteNum = QInputDialog::getInt(this, tr("QInputDialog::setNumParam()"),
			tr("delete param:"), item.size()-1, 0, item.size(), 1, &ok);
		if (ok)
		{
			types[currentStation][currentType]->prop[currentFunction].erase(types[currentStation][currentType]->prop[currentFunction].begin() + deleteNum);
			//item.erase(item.begin() + deleteNum);

		}


		PopulatePropertyBrowser(currentFunction);

	}
	else
	{
		QMessageBox::information(this, tr("information"), "No function is selected");
	}





}

void imageProcessing::onPressedUpdateParameters()
{
	SaveFunctionParameters(currentType,currentFunction);
	//UpdateParameters();
}

void imageProcessing::onPressedSurfaceInspection()
{
	currentFunction = 8;
	ClearDraw();
	PopulatePropertyBrowser(currentFunction);
	

	if(displayWindow == cam.size())
	CheckDefects(displayImage, 1, 1);

}

void imageProcessing::mousePressEvent(QMouseEvent *event)
{
	origin = event->pos();

	if (!rubberBand)
		rubberBand = new QRubberBand(QRubberBand::Rectangle, this);

	rubberBand->setGeometry(QRect(origin, QSize()));
	rubberBand->show();
}

void imageProcessing::mouseReleaseEvent(QMouseEvent * event)
{
	rubberBand->setGeometry(QRect(origin, event->pos()).normalized());
	rubberBand->hide();
}

void imageProcessing::PopulatePropertyBrowser(int function)
{

	for (int i = 0; i < item.size(); i++)
	{
		topItem->removeSubProperty(item[i]);
	}

	item.clear();

	
	for (int i = 0; i < types[currentStation][currentType]->prop[function].size(); i++)
	{
		if (types[currentStation][currentType]->prop[function][i]->propType == 0)	//bool
		{
			item.push_back(variantManager->addProperty(QVariant::Bool, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->boolValue);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 1)	//int
		{
			item.push_back(variantManager->addProperty(QVariant::Int, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->intValue);
			item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->intMinValue);
			item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->intMaxValue);
			item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 2)	//float
		{
			item.push_back(variantManager->addProperty(QVariant::Double, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->doubleValue);
			item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->doubleMinValue);
			item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->doubleMaxValue);
			item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else if (types[currentStation][currentType]->prop[function][i]->propType == 3)	//float
		{
			item.push_back(variantManager->addProperty(QVariant::Rect, QString("%1 ").arg(i) + types[currentStation][currentType]->prop[function][i]->propName));
			item.back()->setValue(types[currentStation][currentType]->prop[function][i]->rectValue);
			//item.back()->setAttribute(QLatin1String("minimum"), types[currentStation][currentType]->prop[function][i]->doubleMinValue);
			//item.back()->setAttribute(QLatin1String("maximum"), types[currentStation][currentType]->prop[function][i]->doubleMaxValue);
			//item.back()->setAttribute(QLatin1String("singleStep"), 10);
			item.back()->setEnabled(true);
			topItem->addSubProperty(item.back());
		}
		else
		{

		}
		if ((loginRights == 1)||(loginRights == 2))
			item.back()->setEnabled(true);
		else
			item.back()->setEnabled(false);
	}

}

void imageProcessing::SaveFunctionParameters(int typeNr, int function)
{
	QStringList list;

	int type = -1;
	int intSize;
	if (function > -1)
	{
		for (int i = 0; i < item.size(); i++)
		{
			if (i < 10)
				intSize = 2;
			else if (i < 100)
				intSize = 3;
			type = item[i]->valueType();
			switch (type)
			{
			case QVariant::Bool: //bool
				types[currentStation][typeNr]->prop[function][i]->propType = 0;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->boolValue = item[i]->value().toBool();
				break;
			case QVariant::Int: //int
				types[currentStation][typeNr]->prop[function][i]->propType = 1;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->intValue = item[i]->value().toInt();

				break;
			case QVariant::Double: //double
				types[currentStation][typeNr]->prop[function][i]->propType = 2;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->doubleValue = item[i]->value().toDouble();
				break;
			case QVariant::Rect: //rectangle
				types[currentStation][typeNr]->prop[function][i]->propType = 3;
				types[currentStation][typeNr]->prop[function][i]->propName = item[i]->propertyName().remove(0, intSize);
				types[currentStation][typeNr]->prop[function][i]->rectValue = item[i]->value().toRect();
				//types[currentStation][typeNr]->prop[function][i]->rectValue = item[i]->value().toInt();
				break;
			}
		}

		//Pot do konfugiracijskih datotek

			QString dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/types_station%2/").arg(REF_FOLDER_NAME).arg(currentStation);
			QDir dir;

			dir.setPath(dirPath);
			if (!dir.exists())
				dir.mkdir(dirPath);

			dirPath = dirPath + QString("parameters_%1/").arg(types[0][typeNr]->typeName);


			dir.setPath(dirPath);
			if (!dir.exists())
				dir.mkdir(dirPath);

			QString filePath = dirPath + QString("function_%1.ini").arg(function);

			QFile file(filePath);
			file.remove();

			QSettings settings(filePath, QSettings::IniFormat);

			for (int i = 0; i < types[currentStation][typeNr]->prop[function].size(); i++)
			{
				switch (types[currentStation][typeNr]->prop[function][i]->propType)
				{
				case 0://bool
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->boolValue);
					break;
				case 1://integer
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->intValue)
						<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->intMinValue) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->intMaxValue);
					break;
				case 2://double
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->doubleValue)
						<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->doubleMinValue) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->doubleMaxValue);
					break;

				case 3://rectangle
					list << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.x())
						<< QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.y()) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.width()) << QString("%1").arg(types[currentStation][typeNr]->prop[function][i]->rectValue.height());
					break;


				}
				settings.setValue(QString("parameter%1").arg(i), list);
				list.clear();
			}

			QMessageBox::information(this, tr("information"), "Parameters saved");
		
	}
	else
		QMessageBox::information(this, tr("information"), "No function is selected");
	
		/*list << QString("%1").arg(toleranceHigh[i]) << QString("%1").arg(toleranceLow[i]) << QString("%1").arg(correctFactor[i]) << QString("%1").arg(offset[i]) << QString("%1").arg(nominal[i]) << QString("%1").arg(isActive[i]) << name[i];
		settings.setValue(QString("parameter%1").arg(i), list);
		list.clear();
	*/
	/*		for (int i = 0; i < paramNum; i++)
			{
				//Posodobimo tabelo parametrov in shranimo posodobljene vrednosti
				ui.ParameterLayout->getItemPosition(i, &row, &col, &rowSpan, &colSpan);
				parameterTable[0][0][camNum][imgNum][row][col] = paramLineEdit[i]->text().toDouble();
				SaveParameters(camNum, imgNum, i, row, col);
			}
		}
	}*/

}

void imageProcessing::SaveFunctionParametersOnNewType(int station, int typeNr, int function)
{

	QStringList list;
			//Pot do konfugiracijskih datotek

			QString dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/types_station%2/").arg(REF_FOLDER_NAME).arg(station);
			QDir dir;

			dir.setPath(dirPath);
			if (!dir.exists())
				dir.mkdir(dirPath);

			dirPath = dirPath + QString("parameters_%1/").arg(types[station][typeNr]->typeName);


			dir.setPath(dirPath);
			if (!dir.exists())
				dir.mkdir(dirPath);

			QString filePath = dirPath + QString("function_%1.ini").arg(function);

			QSettings settings(filePath, QSettings::IniFormat);

			for (int i = 0; i < types[station][typeNr]->prop[function].size(); i++)
			{
				switch (types[station][typeNr]->prop[function][i]->propType)
				{
				case 0://bool
					list << QString("%1").arg(types[station][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[station][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[station][typeNr]->prop[function][i]->boolValue);
					break;
				case 1://integer
					list << QString("%1").arg(types[station][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[station][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[station][typeNr]->prop[function][i]->intValue)
						<< QString("%1").arg(types[station][typeNr]->prop[function][i]->intMinValue) << QString("%1").arg(types[station][typeNr]->prop[function][i]->intMaxValue);
					break;
				case 2://double
					list << QString("%1").arg(types[station][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[station][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[station][typeNr]->prop[function][i]->doubleValue)
						<< QString("%1").arg(types[station][typeNr]->prop[function][i]->doubleMinValue) << QString("%1").arg(types[station][typeNr]->prop[function][i]->doubleMaxValue);
					break;

				case 3://rectangle
					list << QString("%1").arg(types[station][typeNr]->prop[function][i]->propType) << QString("%1").arg(types[station][typeNr]->prop[function][i]->propName) << QString("%1").arg(types[station][typeNr]->prop[function][i]->rectValue.x())
						<< QString("%1").arg(types[station][typeNr]->prop[function][i]->rectValue.y()) << QString("%1").arg(types[station][typeNr]->prop[function][i]->rectValue.width()) << QString("%1").arg(types[station][typeNr]->prop[function][i]->rectValue.height());
					break;

				}
				settings.setValue(QString("parameter%1").arg(i), list);
				list.clear();
			}


}

void imageProcessing::DeleteTypeProperty(QString  typeName)
{




}

void imageProcessing::LoadTypesProperty()
{
	QStringList values;
	
	QString filePath;
	QString dirPath;
	QDir dir;
	QString functionName;
	int functionNr;
	int n = 0;


	for (int i = 0; i < NR_STATIONS; i++)
	{

		//filePath = "D:\\Git_MBvision\\MBsoftware32bit\\MBsoftware32Bit\\Win32\\Reference\\signali\\ControlCardMB0.ini";
		dirPath = QDir::currentPath() + QString("/%1/DynamicVariables/types_Station%2").arg(REF_FOLDER_NAME).arg(i);
		QSettings settings(dirPath, QSettings::IniFormat);

		if (!QDir(dirPath).exists())
			QDir().mkdir(dirPath);


		dir.setPath(dirPath);
		dir.setNameFilters(QStringList() << "parameters*");
		int count = dir.count();
		QStringList dirList = dir.entryList();
		for (int j = 0; j < count; j++)
		{

			filePath = dirPath + QString("/%1").arg(dirList.at(j));


			dir.setPath(filePath);
			dir.setNameFilters(QStringList() << "*.ini");
			int count2 = dir.count();
			QStringList list = dir.entryList();
			//inputs
			for (int k = 0; k < count2; k++)
			{
				n = 0;
				functionName = list[k];
				functionName.chop(4);
				functionName.remove(0, 9);
				functionNr = functionName.toInt();

				QString path = filePath + QString("/%1").arg(list.at(k));
				QSettings settings(path, QSettings::IniFormat);

				do {
					values.clear();
					values = settings.value(QString("parameter%1").arg(n)).toStringList();

					if (values.size() > 1)
					{
						types[i][j]->prop[functionNr].push_back(new CProperty());

						switch (values[0].toInt())
						{
						case 0://bool
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->boolValue = values[2].toInt();
							break;
						case 1://integer
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->intValue = values[2].toInt();
							types[i][j]->prop[functionNr].back()->intMinValue = values[3].toInt();
							types[i][j]->prop[functionNr].back()->intMaxValue = values[4].toInt();
							break;
						case 2://double
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->doubleValue = values[2].toDouble();
							types[i][j]->prop[functionNr].back()->doubleMinValue = values[3].toDouble();
							types[i][j]->prop[functionNr].back()->doubleMaxValue = values[4].toDouble();
							break;
						case 3://rectangle
							types[i][j]->prop[functionNr].back()->propType = values[0].toInt();
							types[i][j]->prop[functionNr].back()->propName = values[1];
							types[i][j]->prop[functionNr].back()->rectValue.setX(values[2].toInt());
							types[i][j]->prop[functionNr].back()->rectValue.setY(values[3].toInt());
							types[i][j]->prop[functionNr].back()->rectValue.setWidth(values[4].toInt());
							types[i][j]->prop[functionNr].back()->rectValue.setHeight(values[5].toInt());

							break;
						}
					}
					n++;
				} while (values.size() > 0);
			}
		}
	}
}

int imageProcessing::ProcessingCamera0(int nrCam, int imageIndex, int draw) 
{


	return 0;
}
int imageProcessing::ProcessingCamera1(int nrCam, int imageIndex, int draw)
{
	

	return 0;
}

int imageProcessing::ProcessingCamera2(int NrCam, int imageIndex, int draw)
{
	return 0;
}




int imageProcessing::ProcessingCamera3(int id, int imageIndex, int draw)
{


	return 0;
}

int imageProcessing::ProcessingCamera4(int id, int imageIndex, int draw) //nova funkcija za poteg karakteristike  modula X os 
{
	

	return 0;
}

int imageProcessing::CheckDefects(int nrCam, int piece,int draw)
{
	return 0;
}






int imageProcessing::InspectSurface(CMemoryBuffer *image, Rect region,int  imageIndex, int nrPiece, int draw)
{


	return 0;
}





#include "stdafx.h"
#include "SMCdrive.h"





SMCdrive::SMCdrive(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
}

SMCdrive::~SMCdrive()
{
}

SMCdrive::SMCdrive(QString port, int baudRate)
{
	ui.setupUi(this);

	

	if (port == "COM1")
		this->port = 0;
	else if (port == "COM2")
		this->port = 1;
	else if (port == "COM3")
		this->port = 2;
	else if (port == "COM4")
		this->port = 3;
	else if (port == "COM5")
		this->port = 4;
	else if (port == "COM6")
		this->port = 5;
	else if (port == "COM7")
		this->port = 6;
	else if (port == "COM8")
		this->port = 7;
	else if (port == "COM9")
		this->port = 8;
	else if (port == "COM10")
		this->port = 9;
	else if (port == "COM11")
		this->port = 10;
	else if (port == "COM12")
		this->port = 11;


	ui.setupUi(this);

	isOpen = false;
	currentId = -1;
	motorLive = false;
	code = -1;
	//this->baudRate = baudRate;
	int Version;

	code = SioKeyCode(1117990899);
	SioReset(-1, 1, 1); // Set DTR & RTS at port initialization.
	code = SioReset(this->port, 1024, 1024);
	if (code < 0)
	{
		isOpen = false;
	}
	Version = SioInfo('V');

	code = SioBaud(this->port, baudRate);
	// clear receive buffer
	code = SioRxClear(this->port);
	code = SioDTR(this->port, 'S');
	code = SioRTS(this->port, 'S');
	code = SioParms(this->port, 0, 1, 8);

	if (code >= 0)
		isOpen = true;
	


	viewTimer = new QTimer(this);
	connect(viewTimer, SIGNAL(timeout()), this, SLOT(ViewTimerFunction()));
	connect(ui.buttonMotorOn, SIGNAL(pressed()), this, SLOT(MotorOnFunction()));
	connect(ui.buttonOrigin, SIGNAL(pressed()), this, SLOT(GoToOriginFunction()));
	connect(ui.buttonReset, SIGNAL(pressed()), this, SLOT(ResetFunction()));
	connect(ui.comboBoxID, SIGNAL(activated(int)), this, SLOT(IdChanged(int)));
	connect(ui.buttonJogPlus, SIGNAL(pressed()), this, SLOT(JogPlusPressed()));
	connect(ui.buttonJogPlus, SIGNAL(released()), this, SLOT(JogPlusReleased()));
	connect(ui.buttonJogMinus, SIGNAL(pressed()), this, SLOT(JogMinusPressed()));
	connect(ui.buttonJogMinus, SIGNAL(released()), this, SLOT(JogMinusReleased()));
	connect(ui.buttonMove, SIGNAL(released()), this, SLOT(MoveFunction()));
}


void SMCdrive::closeEvent(QCloseEvent *event)
{
	viewTimer->stop();
	ui.comboBoxID->clear();

}


void SMCdrive::ShowDialog()
{
	QString name;
	ui.comboBoxID->setStyleSheet("font: 75 10pt MS Shell Dlg 2");
	
	if (!isVisible())
	{
		for (int k = 0; k < MAX_MOT; k++)
		{

			if (motorSettingsList[k].size() > 0)
			{
				name = QString("ID:%1 NAME:%2").arg(k).arg(motorSettingsList[k][0]);

				if (currentId == -1)
				{
					currentId = k;

					//ui.comboBoxID->Cu
				}
			}
			else
				name = ("N.C");

			ui.comboBoxID->addItem(name);

		}
	}
	else
		activateWindow();
	ui.comboBoxID->setCurrentIndex(currentId);
	
	viewTimer->start(300);
	show();
}

int SMCdrive::Init(int Id)
{
		int result;
		int  start[10];
		QByteArray send;
		char dump[50];
		char read[10];
	

		//enable sending serial bytes to motor
		start[0] = Id;//02;//01;
		start[1] = 0x05;
		start[2] = 0x00;
		start[3] = 0x30;
		start[4] = 0xff;
		start[5] = 0x00;
	
		//crc16(start, 6);


		CalcualteCRC(start, 6, send);
		if (isOpen == true)
		{
			WriteData(send);

		}
		Sleep(50);


		start[0] = Id;
		start[1] = 0x08;
		start[2] = 0x00;
		start[3] = 0x00;
		start[4] = 0x12;
		start[5] = 0x34;
	
		
		CalcualteCRC(start, 6, send);

		if (isOpen == true)
		{
			WriteData(send);
		}
		Sleep(50);

		MotorOn(Id);
	


		Sleep(50);

		//Reset(Id);

		//ReturnToOrigin(Id);
	
		return true;
}





void SMCdrive::Reset(int Id)
{
	int input[10];
	QByteArray send;
	input[0] = Id;
	input[1] = 0x05;
	input[2] = 0x00;
	input[3] = 0x45;
	input[4] = 0xff;
	input[5] = 0x00;


	CalcualteCRC(input, 6, send);

	if (isOpen == true)
	{
		WriteData(send);
	}

	Sleep(50);



	//RESET command DRIVE
	input[0] = 0x01;
	input[1] = 0x05;
	input[2] = 0x00;
	input[3] = 0x1a;
	input[4] = 0x00;
	input[5] = 0x00;

	CalcualteCRC(input, 6, send);

	if (isOpen == true)
		WriteData(send);

	Sleep(50);

}

QByteArray SMCdrive::ReadStatus(int Id)
{
	char dump[100];
	//char send[10];
	int start[10];
	char read[10];
	unsigned char e;
	int result;
	//QByteArray getBuffer;
	QByteArray send;
	
	//preverimo ce je vse uredu
	//SioGets(port, dump, 50);
	//serial->waitForReadyRead();

	FlushData();
	Sleep(50);

	


	start[0] = Id;//02;//01;
	start[1] = 0x02;
	start[2] = 0x00;
	start[3] = 0x40;
	start[4] = 0x00;
	start[5] = 0x10;
	//crc16(start, 6);

	CalcualteCRC(start, 6, send);


	if (isOpen == true)
	{
		WriteData(send);
		//serial->waitForBytesWritten();
	}
	Sleep(50);
	if (isOpen == true)
	{
		ReadData();
	}

	/*0->busy
1->SVRE
2->SETON
3->INP
4->AREA
5->WAREA
6->ESTOP
7->ALARM
*/

	e = getBuffer[4];

	return 0;
}

void SMCdrive::ReturnToOrigin(int Id)
{
	int start[10];
	QByteArray send;
	//return to origin flag  SETUP 1
	start[0] = Id;//02;//01;
	start[1] = 0x05;
	start[2] = 0x00;
	start[3] = 0x1c;
	start[4] = 0xff;
	start[5] = 0x00;

	CalcualteCRC(start, 6, send);


	if (isOpen == true)
	{
		WriteData(send);
	}

	Sleep(200);

	//set flag  SETUP 0
	start[0] = Id;//02;//01;
	start[1] = 0x05;
	start[2] = 0x00;
	start[3] = 0x1c;
	start[4] = 0x00;
	start[5] = 0x00;
	

	CalcualteCRC(start, 6, send);


	if (isOpen == true)
	{
		WriteData(send);
	}
	Sleep(200);

}

void SMCdrive::CalcualteCRC(int * input, int lenght, QByteArray& sendData)
{
	int Uk[50];
	int i, j, n;
	int Ab = 0xa001;
	int A, B, C, D;
	for (i = 0; i < lenght; i++)
	{
		Uk[i] = input[i];
	}
	

	C = 0xFFFF;
	for (j = 0; j < lenght; j++)
	{
		B = Uk[j] & 0x00ff;
		C = C ^ B;

		for (i = 0; i < 8; i++)
		{
			if ((C & 1) == 1)
			{
				C = C >> 1;
				C = C ^ Ab;

			}
			else
			{
				C = C >> 1;
			}

		}
		A = C;
	}
	B = C;


	Uk[lenght + 1] = C >> 8;  // Izracunani kontrolni bit 1
	Uk[lenght] = C & 0xff;    // Izracunani kontrolni bit 2 
//	Smc1in[lenght] = Uk[lenght];
//	Smc1in[lenght + 1] = Uk[lenght + 1];

	for (int i = 0; i < lenght + 2; i++)
	{
		sendData[i] = Uk[i];
	}
		

}

void SMCdrive::MotorOn(int id)
{
	int start[20];
	QByteArray send;
	//turn motor on

//servo motor = ON
	start[0] = id;
	start[1] = 0x05;
	start[2] = 0x00;
	start[3] = 0x19;
	start[4] = 0xff;
	start[5] = 0x00;
	
	int size = send.size();


	CalcualteCRC(start, 6, send);

	size = send.size();

	if (isOpen == true)
	{
		WriteData(send);
	}

	motorLive = true;
}

void SMCdrive::MotorOff(int Id)
{
	int turnOff[10];
	QByteArray send;

	turnOff[0] = Id;
	turnOff[1] = 05;
	turnOff[2] = 00;
	turnOff[3] = 0x19;
	turnOff[4] = 0x00;
	turnOff[5] = 0x00;
	//crc16(turnOff, 6);


	CalcualteCRC(turnOff, 6, send);
	if (isOpen == true)
	{
		WriteData(send);
	}
	motorLive = false;
}

void SMCdrive::GoToPositionInMM(int ID, float mm, int speed)
{

			int input[50];
			unsigned char speed1 = 0, speed2 = 0, position1 = 0, position2 = 0;

			int  pozicija = mm * 100;
			position2 = pozicija;
			position1 = pozicija >> 8;

			int  Sspeed = speed;
			speed2 = Sspeed;
			speed1 = Sspeed >> 8;
			QByteArray send;

			input[0] = ID;
			input[1] = 0x10;
			input[2] = 0x91;
			input[3] = 0x02;
			input[4] = 0x00;
			input[5] = 0x10;
			input[6] = 0x20;
			input[7] = 0x00;	//mode
			input[8] = 0x01;	//mode
			input[9] = speed1;	//speed
			input[10] = speed2;	//speed
			input[11] = 0x00;	//position
			input[12] = 0x00;	//position
			input[13] = position1;	//position
			input[14] = position2;	//position
			input[15] = 0x13;	//acceleration
			input[16] = 0x88;	//acceleration
			input[17] = 0x13;	//deceleration
			input[18] = 0x88;	//deceleration
			input[19] = 0x00;	//pushing force
			input[20] = 0x00;	//pushing force
			input[21] = 0x00;	//trigger level
			input[22] = 0x00;	//trigger level
			input[23] = 0x00;	//pushing speed
			input[24] = 0x14;	//pushing speed
			input[25] = 0x00;	//moving force
			input[26] = 0x64;	//moving force
			input[27] = 0x00;
			input[28] = 0x00;
			input[29] = 0x00;
			input[30] = 0x00;
			input[31] = 0x00;
			input[32] = 0x00;
			input[33] = 0x00;
			input[34] = 0x00;
			input[35] = 0x00;
			input[36] = 0x00;
			input[37] = 0x00;
			input[38] = 0x64;
		

			CalcualteCRC(input, 39, send);
			if (isOpen == true)
			{
				WriteData(send);
			}
			Sleep(50);

			send.clear();

			input[0] = ID;//02;//01; 
			input[1] = 0x10;
			input[2] = 0x91;
			input[3] = 0x00;
			input[4] = 0x00;
			input[5] = 0x01;
			input[6] = 0x02;
			input[7] = 0x01;
			input[8] = 0x00;
			CalcualteCRC(input, 9, send);
			if (isOpen == true)
			{
				WriteData(send);
			}
			Sleep(50);



}

float SMCdrive::ReadPosition(int Id)
{
	int status = 0;
	int input[100];
	char output[100], dump[100], outputMM[100];
	QByteArray send;
	QByteArray get;
	unsigned	char a, b, c, d, e;
	int rezultat;
	char tmpString[10];


	FlushData();



	//vrne pozicijo v milimetrih
	input[0] = Id & 0xff; //id
	input[1] = 0x03; //fix ukaz za posiljanje pozicij	
	input[2] = 0x90;
	input[3] = 0x00;
	input[4] = 0x00;
	input[5] = 0x02;
	CalcualteCRC(input, 6, send);
	//	Smc1in[6] = 0xE9;
	// Smc1in[7] = 0x0b;

		WriteData(send);
		Sleep(50);
		//serial->waitForReadyRead();
		ReadData();

	a = getBuffer[3];

	b = getBuffer[4];

	c = getBuffer[5];

	d = getBuffer[6];


	int  	 i = ((a * 256 * 256 * 256) + (b * 256 * 256) + (c * 256) + d);


	float z = (float)i;

	z /= 100;

	float j;
	j = (float)(c * 256 + d) / 100;
	//temp.Format(_T("Motor 1: Status:%s \n Pozicija:%0.3f"), temp1, j);
	//dc.TextOut(1300, 540, temp);
	//StatusPozM1 = j;




	return j;


}

void SMCdrive::ReadInputSignals(int id)
{

	int status = 0;
	int input[100];
	char output[100], dump[100], outputMM[100];
	QByteArray send;
	QByteArray get;
	unsigned	char a, b, c, d, e;
	int rezultat;
	char tmpString[10];


	FlushData();



	//vrne pozicijo v milimetrih
	input[0] = id & 0xff; //id
	input[1] = 0x02; //fix ukaz za posiljanje pozicij	
	input[2] = 0x00;
	input[3] = 0x40;
	input[4] = 0x00;
	input[5] = 0x10;
	CalcualteCRC(input, 6, send);
	int ret;

		WriteData(send);
		Sleep(50);
		//serial->waitForReadyRead();
		ret = ReadData();
		
		if (ret > 0)
		{
			int bla = 100;
		}

		getBuffer[4];
		getBuffer[5];


}

void SMCdrive::GoToSavedPosition(int Id, int position)
{
	int input[10];
	QByteArray send;


	input[0] = Id; //tukaj ID za krmilnik
	input[1] = 0x0f; //fix ukaz za posiljanje pozicij	
	input[2] = 0x00;
	input[3] = 0x10;
	input[4] = 0x00;
	input[5] = 0x08;
	input[6] = 0x01;
	input[7] = position & 0x3f; //st pozicije vnesemo tukaj;

	//crc16(input, 8);
	crcLO;
	crcHI;

	CalcualteCRC(input, 8, send);

		WriteData(send);



	Sleep(50);

	//drive
	input[0] = Id;
	input[1] = 0x05;
	input[2] = 0x00;
	input[3] = 0x1a;
	input[4] = 0xff;
	input[5] = 0x00;
	

	CalcualteCRC(input, 6, send);


		WriteData(send);


	Sleep(50);

	//reset command drive
	input[0] = Id;
	input[1] = 0x05;
	input[2] = 0x00;
	input[3] = 0x1a;
	input[4] = 0x00;
	input[5] = 0x00;


	CalcualteCRC(input, 6, send);

		WriteData(send);

	Sleep(50);




}

void SMCdrive::Jog(int id, bool forward)
{
	int input[10];
	QByteArray send;
	int smer;
	if (forward == true)
		smer = 0x40 |0x02;
	else
		smer = 0x20 | 0x02;

	input[0] = id; //tukaj ID za krmilnik
	input[1] = 0x0f; //fix ukaz za posiljanje pozicij	
	input[2] = 0x00;
	input[3] = 0x10;
	input[4] = 0x00;
	input[5] = 0x10;
	input[6] = 0x02;
	input[7] = 0x00; //st pozicije vnesemo tukaj;
	input[8] = smer; //st pozicije vnesemo tukaj;


	CalcualteCRC(input, 9, send);

	WriteData(send);

	Sleep(50);


}
void SMCdrive::StopJog(int id)
{

	int input[10];
	QByteArray send;

	input[0] = id; //tukaj ID za krmilnik
	input[1] = 0x0f; //fix ukaz za posiljanje pozicij	
	input[2] = 0x00;
	input[3] = 0x10;
	input[4] = 0x00;
	input[5] = 0x10;
	input[6] = 0x02;
	input[7] = 0x00; //st pozicije vnesemo tukaj;
	input[8] = 0x02; //st pozicije vnesemo tukaj;



	CalcualteCRC(input, 9, send);

	WriteData(send);

}

void SMCdrive::WriteData(QByteArray data)
{

	char param[1000];

	for (int i = 0; i < data.size(); i++)
	{
		param[i] = data[i];

	}
	code = -1;

	if (isOpen)
	{
		code = SioPuts(port, (char*)param, data.size());
	}


 


}
void SMCdrive::FlushData()
{
	char get[1024];
	code = SioGets(port, get, 1024);
	for (int i = 0; i < 1024; i++)
	{
		getBuffer[i] = '0';
	}
}
int  SMCdrive::ReadData()
{
	char get[1024];
	code = SioGets(port, get, 1024);
	for (int i = 0; i < code; i++)
	{
		getBuffer[i] = get[i];
	}
	return code;
}
void SMCdrive::ViewTimerFunction()
{
	float pos;
	QByteArray status;
	pos = ReadPosition(currentId);


	ui.labelPosition->setText(QString("%1").arg(pos));

	status = ReadStatus(currentId);
	
	unsigned char buffer = getBuffer[4];


	if((buffer & 0x01) == 0x01)
		ui.labelBusy->setStyleSheet("QLabel { background-color : rgb(46, 112, 255); color : black; }");
	else
		ui.labelBusy->setStyleSheet("QLabel { background-color : rgb(200, 199, 197); color : black; }");
	if((buffer & 0x02) == 0x02)
		ui.labelSVRE->setStyleSheet("QLabel { background-color : rgb(46, 112, 255); color : black; }");
	else
		ui.labelSVRE->setStyleSheet("QLabel { background-color : rgb(200, 199, 197); color : black; }");
	if((buffer & 0x04) == 0x04)
		ui.labelSeton->setStyleSheet("QLabel { background-color : rgb(46, 112, 255); color : black; }");
	else
		ui.labelSeton->setStyleSheet("QLabel { background-color : rgb(200, 199, 197); color : black; }");

	if((buffer & 0x08) == 0x08)
		ui.labelINP->setStyleSheet("QLabel { background-color : rgb(46, 112, 255); color : black; }");
	else
		ui.labelINP->setStyleSheet("QLabel { background-color : rgb(200, 199, 197); color : black; }");
///	if((buffer & 0x10) == 1)
//	if((buffer & 0x20 )== 1)
//	if((buffer & 0x40) == 1)
	if((buffer & 0x80) == 0x80)
		ui.labelAlarm->setStyleSheet("QLabel { background-color : red; color : black; }");
	else
		ui.labelAlarm->setStyleSheet("QLabel { background-color : rgb(200, 199, 197); color : black; }");


	/*0->busy
	1->SVRE
	2->SETON
	3->INP
	4->AREA
	5->WAREA
	6->ESTOP
	7->ALARM
	*/
	if(motorLive == true)
	ui.buttonMotorOn->setText("Motor Off");
	else
		ui.buttonMotorOn->setText("Motor On");
	/*if (e == 0x8e)
		result = -1;
	//temp1 = "alarm!! Reset Motor";
	if (e == 0x0e)
		result = 1;
	//temp1 = "OK";
	if (e == 0x0c)
		result = -2;
	//temp1 = "Servo is OFF!";
	if (e == 0x04)
		result = -3;
	//temp1 = "Servo is OFF!";
	if (e == 0x06)
		result = -4;
	//temp1 = "Return to HOME";
	if (e == 0x07)
		result = -5;
	//temp1 = "Busy";
	if (e == 0x80)
		result = -6;
	if (e == 0x82)
		result = -7;
	if (e == 0x02)
		result = 2;
	//temp1 = "Busy";*/



	if (status.size() > 0)
	{

	}

}

void SMCdrive::IdChanged(int  num)
{
	currentId = num;
	
}

void SMCdrive::JogMinusPressed()
{
	Jog(currentId, 0);
}

void SMCdrive::JogMinusReleased()
{
	StopJog(currentId);
}

void SMCdrive::MoveFunction()
{
	QString text;
	text = ui.editMoveToPosition->text();
	
	
		GoToPositionInMM(currentId, text.toFloat(), 20);
}

void SMCdrive::JogPlusPressed()
{
	Jog(currentId, 1);
}

void SMCdrive::JogPlusReleased()
{
	StopJog(currentId);
	
}

void SMCdrive::MotorOnFunction()
{
	if (motorLive == true)
	{
		
		MotorOff(currentId);
	}
	else
	{
	
		MotorOn(currentId);
	}

}

void SMCdrive::GoToOriginFunction()
{
	ReturnToOrigin(currentId);
}

void SMCdrive::ResetFunction()
{
	Reset(currentId);
}



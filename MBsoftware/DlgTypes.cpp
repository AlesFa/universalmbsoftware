#include "stdafx.h"
#include "DlgTypes.h"


DlgTypes::DlgTypes(QWidget * parent)
{
}

DlgTypes::DlgTypes(QWidget *parent, int nTypes)
	: QDialog(parent)
{
	ui.setupUi(this);
	connect(ui.buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
	connect(ui.buttonBox, SIGNAL(accepted()),this, SLOT(accept()));



	for (int i = 0; i < nTypes; i++)
	{
		types.push_back((""));
	}
}

DlgTypes::~DlgTypes()
{
}

int DlgTypes::ShowDialogSelectType(int currType)
{
	QString selected;
	for (int i = 0; i < types.size(); i++)
	{
		ui.comboBoxTypeList->addItem(types[i]);
	}
	ui.labelCurrentType->setText(types[currType]);
	
	if (exec() == IDOK)
	{
		selected = ui.comboBoxTypeList->currentText();

		for (int i = 0; i < types.size(); i++)
		{
			if (selected == types[i])
			{
				return i;
			}
		}
	}
	else
	{
		return -1;
	}
	return 0;
}



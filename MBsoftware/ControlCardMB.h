#pragma once

#include "ui_MBserialCard.h"
#include "qt_windows.h"
#include "Signals.h"
#include "stdafx.h"


class ControlCardMB : public QWidget
{
	Q_OBJECT
public:
	ControlCardMB();
	ControlCardMB(QString port, int baudRate);
	void closeEvent(QCloseEvent * event);
	~ControlCardMB();

	
	

private:
	Ui::ControlCard ui;
	int port;
	char getBuffer[1024];
	QByteArray parseBuffer;
	int parseCounter;
	
	static int			objectCount; //how many created objects


	

public:
	int bytesWaiting;

	bool isOpen;
	Signals input[16];
	Signals output[16];
	int id;
	int counterFreq[4];
	int counterEn[4];
	int isSending = 0;
	int currFrame;
	QGroupBox			*groupBox[3];
	QCheckBox *outputCheckBox[16];
	QCheckBox *inputCheckBox[16];
	BYTE		outputReg0;
	BYTE		outputReg1;
	int intArray[1024];
	int intCounter;
	UCHAR inputReg0;
	UCHAR inputReg1;
	QTimer  *viewTimer;
	int code;
	//QCheckBox *inputCheckBox[16];





	public slots:
	void ReadData();
	void testWrite(qint64 byte);
	void WriteData(QByteArray data);
	void ChangeData();
	void OnChangedOutputBox(int);
	void ViewSignalsTimer();
	void OnChangedCounter0();
	void OnChangedCounter1();
	void OnChangedCounter2();
	void OnChangedCounter3();
	void OnEnableCounter0();
	void OnEnableCounter1();
	void OnEnableCounter2();
	void OnEnableCounter3();




public:
	void CreateDisplay();
	void SetDisplay();
	void ShowDialog();
	int ReadFile(void);
	bool InitCard();
	void StartSend(int freq); //funkcija ki sprozi prozenje interupta za posiljanje freqvenca je 
	void StartSendInput(int port); //sprozi ciklicno posiljanje vrednosti inputov port 0-> 0 do 7 port 1->8 do 15
	void StartSendMotorStep(int timerNum); //posilja korake motorja 
	void StopSendInput(int port); //ustavi posiljanje counterja
	void StopSendMotorStep(int timerNum); //ustavi posiljanje inputov
	void StopSend();//funkcija ustavi prozenja interupt za posiljanje

	void SetInputvalue();//funkcijo klici v tajmerju uporabi se za nastavljanje input vrednosti signalov
	void SetOutputValue(int output, int value);//prizge output za vsak bit posebaj upocasni komunikacijo 
	void SetOutputByte(int port, int value); // port 0 -> output 0-7 1->output 8-15  value -> 0-255 hex
	void SetPortByte(int port); //prebere vse outpute.value na pportu in jih vpi�e v serijski kom. Funkcijo klicemo samo enkrat na koncu timerja

	void SetTimer(int timerNum, int freq, int enable);

	void SetTimerFreq(int timerNum, int freq); //nastavimo zelejno frekvenco zeljenega timerja 0-3

	void StartTimer(int timerNum);

	void StartTimer(int timerNum, int steps);//zagon timerje ki traja naredi st. korakov	

	void StopTimer(int timerNum);

	void SetSoftStartStopTimer(int timerNum, int  rampStep);//mehak zagon motorja  funkcijo poklicat pred funkcijo start Timer;
	
	
	///za trenutni projekt 
	public:
		int motorCounterArray[2000];
};


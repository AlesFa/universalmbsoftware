#include "stdafx.h"
#include "LoginWindow.h"
#include "MBsoftware.h"



LoginWindow::LoginWindow(QWidget *parent)
	: QWidget(parent)
{
	currentRights = 0;
	rightsChanged = false;
	ui.setupUi(this);
	clikedCounter = 0;
	clickedPrevCounter = 0;
	LogoutTimerCounter = 0;

	ui.comboBox_prijava->addItem("Administrator");
	ui.comboBox_prijava->addItem("Operator");
	ui.comboBox_prijava->addItem("Worker");
	userName.push_back(ui.comboBox_prijava->itemText(0));
	userName.push_back(ui.comboBox_prijava->itemText(1));
	userName.push_back(ui.comboBox_prijava->itemText(2));
	password.push_back("12312");
	password.push_back("8080");
	password.push_back("123");

	SetRights();

	connect(ui.pushButton_prijava, SIGNAL(clicked()), this, SLOT(AcceptLogin()));
	connect(ui.buttonLogout, SIGNAL(clicked()), this, SLOT(Logout()));
	//connect(this, SIGNAL(currentRights_changed()), this, SLOT(MBsoftware32Bit->User_status_changed()));
	timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(LogoutTimer()));
	
}

LoginWindow::~LoginWindow()
{
}

void LoginWindow::ShowDialog()
{
	if (CheckRights() == 0)
	{
		//�e nismo prijvaljeni onemogo�imo gumb Logout
		ui.buttonLogout->setEnabled(false);
	}
	else
		ui.buttonLogout->setEnabled(true);

	setWindowModality(Qt::ApplicationModal);
	show();
}

//Postavi vektor uporabnikov
/*
* 1 - Delavec
* 2 - Operater
* 3 - Administrator
*/
void LoginWindow::SetUsernameList()
{
	for (int i = 0; i < ui.comboBox_prijava->count(); i++)
	{
		userName.push_back(ui.comboBox_prijava->itemText(i));
	}

}

/*
Trenutno je nastavljeno geslo password za vse uporabnike

** Delavec - 123
** Operater - 123
** Administrator - 123

*/
//Postavi vektor gesel, ki pripadajo razlicnim uporabnikom
void LoginWindow::SetPasswordList()
{
	for (int i = 0; i < ui.comboBox_prijava->count(); i++)
	{
		password.push_back("123");
	}
}

//Postavi vektor pravic uporabnika
//PRAVICE SE ZACNEJO S STEVILKO 1!!!
void LoginWindow::SetRights()
{
	for (int i = 0; i < ui.comboBox_prijava->count(); i++)
	{
		rights.push_back(i + 1);
	}
}

//Preveri �e se uporabni�ko ime in geslo skladata
//Vrne pravice uporabnika
int LoginWindow::CheckRights()
{
	for (int i = 0; i < ui.comboBox_prijava->count(); i++)
	{
		if (currentUser == userName[i])
		{
			if (currentPassword == password[i])
			{
				clikedCounter++;
				return rights[i];
			}
		}
	}

	return 0;
}

//Sprejme ali zavrne uporabnika oz. geslo
//Zapre Login okno, �e sta uporabnik oz. geslo sprejeta
void LoginWindow::AcceptLogin()
{

	currentUser = ui.comboBox_prijava->currentText();
	currentPassword = ui.lineEdit_prijava_geslo->text();

	currentRights = CheckRights();
	clikedCounter++;

	if (currentRights > 0)
	{
		close();
		rightsChanged = true;
		timer->start(500);
		clikedCounter = 0;
	}

}

void LoginWindow::Logout()
{
	currentRights = 0;
	currentUser = "";
	currentPassword = "";
	rightsChanged = true;
	ui.lineEdit_prijava_geslo->clear();
	hide();
}

void LoginWindow::LogoutTimer()
{
	if (clikedCounter != clickedPrevCounter)
	{
		clickedPrevCounter = clikedCounter;
		LogoutTimerCounter = 0;
	}
	else
		LogoutTimerCounter++;


	if (LogoutTimerCounter * 0.5 > LOGIN_TIMEOUT * 60)
	{
		Logout();
		timer->stop();
		LogoutTimerCounter = 0;
	}

}


#pragma once
#include "stdafx.h"
#include "ui_LoginWindow.h"




class LoginWindow :
	public QWidget
{
	Q_OBJECT;

public:
	LoginWindow(QWidget *parent = 0);
	~LoginWindow();

private:
	Ui::LoginWindow ui;
	QTimer  *timer;
	int clikedCounter;
	int clickedPrevCounter;
	int LogoutTimerCounter;

public:
	std::vector<QString>		userName;
	std::vector<QString>		password;

	std::vector<int>			rights;

	QString						currentUser;
	QString						currentPassword;

	/*
	* 0 - Brez pravic
	* 3 - Delavec
	* 2 - Operater
	* 1 - Administrator
	*/
	int							currentRights;
	bool						rightsChanged;


public:
	void ShowDialog();
	void SetUsernameList();
	void SetPasswordList();
	void SetRights();
	int CheckRights();

public slots:
	void AcceptLogin();
	void Logout();
	void LogoutTimer(); //timer ki automatsko izpise 
};


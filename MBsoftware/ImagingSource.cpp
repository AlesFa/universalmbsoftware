// ImagingSource.cpp : implementation file
//

#include "stdafx.h"
#include "ImagingSource.h"
//#include "direct. h"


ImagingSource::ImagingSource()
{
	 
	InitLibrary();

	if (!grabberInitialized)
	{
		pGrabber = new DShowLib::Grabber();
		//pcListener = new GrabberListener();


		//VERIFY_INFORMATION( pGrabber );
		//VERIFY( pcListener );
		pGrabber->addListener(this, DShowLib::GrabberListener::eDEVICELOST);

		pGrabber->addListener(this, DShowLib::GrabberListener::eDEVICELISTCHANGED);

		grabberInitialized = true;
	}
}
//copy constructor
ImagingSource::ImagingSource(const ImagingSource& camera)
{

	pGrabber = camera.pGrabber;
	m_pBuffer = camera.m_pBuffer;
	pSink = camera.pSink;
	pCollection = camera.pCollection;
	pFilter = camera.pFilter;
	//pcListener = camera.pcListener;	
}

ImagingSource::~ImagingSource()
{
	if (grabberInitialized)
	{
		//zapremo se listenerje za padlo kamero in spremenjen spisek :D
		if (pGrabber->isListenerRegistered(this, DShowLib::GrabberListener::eDEVICELOST))
		{
			pGrabber->removeListener(this, DShowLib::GrabberListener::eDEVICELOST);
			// Second, unregister it for the overlay callback event.

			// Wait until all remove requests have been processed
			while (pGrabber->isListenerRegistered(this, DShowLib::GrabberListener::eDEVICELOST))
			{
				Sleep(0); // Wait
			}
			// Now, the application can be sure that the CListener methods
			// are no longer called by the Grabber. It is now safe to delete the
			// CListener object.
		}

		if (pGrabber->isListenerRegistered(this, DShowLib::GrabberListener::eDEVICELISTCHANGED))
		{
			pGrabber->removeListener(this, DShowLib::GrabberListener::eDEVICELISTCHANGED);
			// Second, unregister it for the overlay callback event.

			// Wait until all remove requests have been processed
			while (pGrabber->isListenerRegistered(this, DShowLib::GrabberListener::eDEVICELISTCHANGED))
			{
				Sleep(0); // Wait
			}
			// Now, the application can be sure that the CListener methods
			// are no longer called by the Grabber. It is now safe to delete the
			// CListener object.
		}
		delete pGrabber;
		//delete pcListener;
		grabberInitialized = false;
	}
}

// ImagingSource message handlers
bool ImagingSource::InitLibrary()
{
	if (!libraryInitialized)
	{
		if (DShowLib::InitLibrary())
		{
			libraryInitialized = true;
		}
		atexit(DShowLib::ExitLibrary);
	}
	return libraryInitialized;
}

int ImagingSource::Init(int selectGrabber, QString cameraName, int VideoFormat, QString VideoFormatS)
{
	int i = 0, n;
	__int64 iSerial;

	QString format, dim, dimW, dimH, tmp;
	QStringList parse[2];
	std::string a;
	InitLibrary();

	grabberInitialized = true;
	vendorName = "Imagingsource";
	frameReadyCounter = 0;
	DShowLib::Grabber::tVidCapDevListPtr pVidCapDevList = pGrabber->getAvailableVideoCaptureDevices();
	if (pVidCapDevList == 0 || pVidCapDevList->empty() || pVidCapDevList->size() <= cameraOpened)
	{
		//tmp.Format(_T("Available %d cameras. %d already opened."), pVidCapDevList->size(), cameraOpened);
		//g_pMainFrame->OnWriteErrorMessage(tmp);
		return 0; // No device available.
	}

	if (pVidCapDevList != 0)
	{
		//	dc.TextOut(420,590," AVALIBAL GRABBERS");

		for (DShowLib::Grabber::tVidCapDevListPtr::value_type::iterator itdev =
			pVidCapDevList->begin();
			itdev != pVidCapDevList->end();
			itdev++)
		{

			tmp = (((DShowLib::Grabber::tVideoCaptureDeviceItem)*itdev).toString()).c_str();

			//tmp = ((DShowLib::Grabber::tVideoCaptureDeviceItem)*itdev).toString();
			if (cameraName.compare(tmp) == 0)
			{
				if (selectGrabber != i)
					selectGrabber = i;

				((DShowLib::Grabber::tVideoCaptureDeviceItem)*itdev).getSerialNumber(iSerial);
				serialNumber = (TEXT("%I64X"), iSerial);
			}
			//	sprintf_s(testb," i=%1d %s",i,((DShowLib::Grabber::tVideoCaptureDeviceItem) *itdev).c_str());
			//	dc.TextOut(420,490+i*20,testb);
			if (i == selectGrabber)
				this->cameraName = tmp;
			i++;
		}
	}

	if (selectGrabber >= i)
	{
		selectGrabber = i - 1;
		this->cameraName = tmp;
	}

	if (selectGrabber >= 0 && selectGrabber < (int)pVidCapDevList->size())
	{
		pGrabber->openDev(pVidCapDevList->at(selectGrabber));//((*pVidCapDevList)[1]);//
	}
	else
	{
		//delete pGrabber;
		return 0; // No Grabber selected, exit the example.
	}
	// Check whether video norms are availble with the current video capture device.
	if (pGrabber->isVideoNormAvailableWithCurDev())
	{
		// Query for all available video norms.
		DShowLib::Grabber::tVidNrmListPtr pVidNrmList = pGrabber->getAvailableVideoNorms();
		if (pVidNrmList == 0)
		{
			// fprintf( stderr, "Error: %s\n", pGrabber->getLastError() );
			//delete pGrabber;
			return 0;
		}
		while (true)
		{
			if (selectGrabber >= 0 && selectGrabber < (int)pVidNrmList->size())
			{
				// Set the selected video norm to the video capture device before
				// retrieving the according video formats.
				pGrabber->setVideoNorm(pVidNrmList->at(selectGrabber));
				// printf( "\n\nVideo Formats available for %s: \n", pVidNrmList->at( selectGrabber ).c_str() );
			}
		}
	}
	else
	{
		// If the current video capture device does not support video norms,
		// the available video formats can be retrieved immediately.
		// printf( "\n\nVideo Formats available: \n" );
		DShowLib::Grabber::tVidFmtListPtr pVidFmtList = pGrabber->getAvailableVideoFormats();


		if (pVidFmtList != 0) // No video formats available?
		{
			i = 0;
			// Display all available video formats on the screen.	
			for (DShowLib::Grabber::tVidFmtListPtr::value_type::iterator it =
				pVidFmtList->begin();
				it != pVidFmtList->end();
				++it)
			{
				//	sprintf_s(testb,"i=%1d %s",i,it->c_str() );
				//	dc.TextOut(420,200+i*20,testb);
				if (VideoFormatS.isEmpty() == 0) //ce string ni prazen
				{
					tmp = (it->toString()).c_str();
					if (VideoFormatS.compare(tmp) == 0) //ce se ujemata po imenu vecja prioriteta
					{
						VideoFormat = i;
						this->videoFormat = tmp;
					}
					else if (i == VideoFormat) //ce se ujemata
						this->videoFormat = tmp;

				}
				else if (i == VideoFormat)
					this->videoFormat.append(tmp);

				i++;
			}
		}

		if (VideoFormat >= i)
		{
			VideoFormat = i - 1;
			this->videoFormat = tmp;

		}

		if (pVidFmtList == 0) // No video formats available?
		{
			// fprintf( stderr, "Error: %s\n", pGrabber->getLastError() );
			//tmp.Format(_T("Error:  No video formats available"));
			//g_pMainFrame->OnWriteErrorMessage(tmp);
		}
		else
		{
			//set video format
			pGrabber->setVideoFormat((*pVidFmtList)[VideoFormat]);
		}
	}

		//dobimo izbran format in dimezijo	
	parse[0] = videoFormat.split(" "); // splits format 

	if (parse[0].size() > 1)
	{
		format = videoFormat[0];


		parse[1] = parse[0][1].split(QRegExp("(x)")); // splits format 

												//	  	doloci dimenzijo in barvne formate	
		width = parse[1][0].toInt();
		height = parse[1][1].toInt();

		videoFormat = format;

		if (format.compare("Y800") == 0)
		{
			depth = 1;
			formatType = CV_8UC1;
			conversionCode = COLOR_GRAY2RGB;

		}
		if (format.compare("Y16") == 0)
		{
			depth = 2;
			formatType = CV_16UC1;
			conversionCode = COLOR_GRAY2RGB;

		}
		if (format.compare("RGB24") == 0)
		{
			depth = 3;
			formatType = CV_8UC3;
			conversionCode = COLOR_BGR2RGB;

		}

		if (format.compare("RGB32") == 0)
		{
			depth = 4;
			formatType = CV_8UC4;
			conversionCode = COLOR_BGRA2RGB;
		}
		return 1;

	}
	return 0;
}
int ImagingSource::Init(QString serialNumber, QString VideoFormatS)
{
	int i = 0, n;
	int VideoFormat = 0;
	int selectGrabber;
	QString format, dim, dimW, dimH, tmp;
	std::string a;
	__int64 iSerial;
	QString serial;
	char buffer[256];
	QStringList parse[2];

	InitLibrary();

	grabberInitialized = true;
	vendorName = "Imagingsource";
	selectGrabber = -1;

	this->serialNumber = serialNumber;
	this->videoFormat = VideoFormatS;

	Grabber::tVidCapDevListPtr pVidCapDevList = pGrabber->getAvailableVideoCaptureDevices();
	if (pVidCapDevList == 0 || pVidCapDevList->empty())
	{
		//tmp.Format(_T("Available %d cameras."), pVidCapDevList->size());
		//g_pMainFrame->OnWriteErrorMessage(tmp);
		return 0; // No device available.
	}

	if (pVidCapDevList != 0)
	{
		//	dc.TextOut(420,590," AVALIBAL GRABBERS");

		for (Grabber::tVidCapDevListPtr::value_type::iterator itdev =
			pVidCapDevList->begin();
			itdev != pVidCapDevList->end();
			itdev++)
		{

			((Grabber::tVideoCaptureDeviceItem)*itdev).getSerialNumber(iSerial);

			sprintf(buffer, "%I64x", iSerial);
			serial.append(buffer);

			tmp = (((DShowLib::Grabber::tVideoCaptureDeviceItem)*itdev).toString()).c_str();

			if (serial.compare(serialNumber) == 0)
			{
				selectGrabber = i;
				this->serialNumber = serial;
				cameraName = tmp;

				break;
			}
			serial.clear();

			i++;
		}

	}

	if (selectGrabber > -1)
	{
		if (!pGrabber->openDev(pVidCapDevList->at(selectGrabber)))
		{
			return 0; // No Grabber selected, exit the example.
		}
	}
	else
		return 0;


	if (!pGrabber->setVideoFormat(videoFormat.toStdString()))
	{
		return 0;
	}


	//	dobimo izbran format in dimezijo	
	parse[0] = VideoFormatS.split(" "); // splits format 

	if (parse[0].size() > 1)
	{
		format = parse[0][0];

		parse[0][1].remove("(");
		parse[0][1].remove(")");
		parse[1] = parse[0][1].split(QRegExp("x")); // splits format 

												//		doloci dimenzijo in barvne formate	
		width = parse[1][0].toInt();
		height = parse[1][1].toInt();

		//videoFormat = format;

		if (format.compare("Y800") == 0)
		{
			depth = 1;
			formatType = CV_8UC1;
			conversionCode = COLOR_GRAY2RGB;

		}
		if (format.compare("Y16") == 0)
		{
			depth = 2;
			formatType = CV_16UC1;
			conversionCode = COLOR_GRAY2RGB;

		}
		if (format.compare("RGB24") == 0)
		{
			depth = 3;
			formatType = CV_8UC3;
			conversionCode = COLOR_BGR2RGB;

		}

		if (format.compare("RGB32") == 0)
		{
			depth = 4;
			formatType = CV_8UC4;
			conversionCode = COLOR_BGRA2RGB;
		}
		return 1;

	}
	return 0;
}
int ImagingSource::Init()
{
	//bere podatke iz svojih vrednosti
	return Init(serialNumber, videoFormat);
}

int ImagingSource::Start()
{
	if (trigger)
		listener = true; //ce je aktiviran trigger mora biti nujno tudi listener

	return Start(FPS, trigger, listener, rotatedVertical, rotadedHorizontal);
}


int ImagingSource::Start(double FPS, bool trigger, bool listener)
{
	return Start(FPS, trigger, listener, false, false);
}

int ImagingSource::Start(double FPS, bool trigger, bool listener, bool flipV, bool flipH)
{
	bool statusB;
	double statusD;
	QString tmp;
	SIZE dim;
	long lAngle = 180;
	long lNewAngle = 180;
	bool bFlipVertical = flipV;
	bool bFlipHorizontal = flipH;

	this->listener = listener;
	this->trigger = trigger;

#ifdef _DEBUG
	pFilter = FilterLoader::createFilter("Rotate Flip",   // Filter name.
		"stdfiltersd.ftf"); // Module file.
#else
	pFilter = FilterLoader::createFilter("Rotate Flip",   // Filter name.
		"stdfilters.ftf"); // Module file.
#endif

	rotatedVertical = flipV;
	rotadedHorizontal = flipH;

	if (pGrabber->isLive())
	{
		// This error should never happen.
		//MessageBox( _T("Grabber 1 already in live-mode!"), 0, 0 );
		return 0;
	}

	//	status = pGrabber->setFrameRate(Framerate); 
	statusB = pGrabber->setFPS(FPS);
	statusD = pGrabber->getFPS();
	this->FPS = (float)statusD;
	statusB = pGrabber->setExternalTrigger(trigger);


	//flip filter
	pFilter->getParameter("Rotation Angle", lAngle);
	pFilter->getParameter("Flip V", bFlipVertical);
	pFilter->getParameter("Flip H", bFlipHorizontal);
	bool res = pFilter->getParameter("Flip H", bFlipHorizontal);
	bool re2s = pFilter->getParameter("Flip V", bFlipVertical);
	

	if (pGrabber->isFlipHAvailable())
	{
		bool er = pGrabber->getFlipH();
		pGrabber->setFlipH(false);
	}

	if (pGrabber->isFlipVAvailable())
	{
		bool er2 = pGrabber->getFlipV();
		pGrabber->setFlipV(false);
	}

	// Check wether the filter could be loaded successfully.
	if (pFilter == NULL)
	{
		//tmp.Format(_T("Failed to load the \"Rotate Flip\" frame filter!"));
		//	g_pMainFrame->OnWriteErrorMessage(tmp);
	}
	else
	{

		//if( pGrabber->isLive() == false )
		//{
		//	pFilter->setParameter( "Rotation Angle", (lNewAngle));
		//}

		// The frame filter has been loaded successfully. Now it should be added
		// to the Grabber object.
		if (!pGrabber->setDeviceFrameFilters(pFilter.get()))
		{
			//	tmp.Format(_T("Failed to load the \"Rotate Flip\" frame filter!"));
			//	g_pMainFrame->OnWriteErrorMessage(tmp);
		}
		else
		{
			pFilter->setParameter("Flip V", flipV);
			pFilter->setParameter("Flip H", flipH);
		}
	}
	
	//FrameTypeInfoArray types = FrameTypeInfoArray::createRGBArray();
	if (depth == 3)
		pSink = FrameHandlerSink::create(eRGB24);
	//pSink = FrameHandlerSink::create(types, 1);// eRGB24);//, num_buff);			
	else if (depth == 4)
		pSink = FrameHandlerSink::create(eRGB32);//, num_buff);
	else
		pSink = FrameHandlerSink::create(eY800);//, num_buff);


												// Disable snap mode - GRAB MODE.
	pSink->setSnapMode(false);

	pGrabber->setSinkType(pSink);

	if (!pGrabber->prepareLive(false))
	{
		//	tmp.Format(_T("Error: %s"), pGrabber->getLastError());
		//	g_pMainFrame->OnWriteErrorMessage(tmp);
		return 0;
	}

	FrameTypeInfo info;
	pSink->getOutputFrameType(info);
	bufferSize = info.buffersize;
	dim = info.dim;

	//sliko aktiviramo ko naredimo bufferje
	activeImage = 0;


	//pCollection = MemBufferCollection::create(info, num_buff, imageRing);
	pCollection = MemBufferCollection::create(info, num_buff);

	if (pCollection == 0 || !pSink->setMemBufferCollection(pCollection))
	{
		//tmp.Format(_T("Could not set the new MemBufferCollection, because types do not match."));
		//	g_pMainFrame->OnWriteErrorMessage(tmp);

		return 0;
	}

	pSink->setMemBufferCollection(pCollection);

	if (pGrabber->getLastError().getVal())
	{
		//	tmp.Format(_T("Error: %s"), pGrabber->getLastError());
		//	g_pMainFrame->OnWriteErrorMessage(tmp);
		return 0;
	}

	pGrabber->startLive(false);			// Start the grabber->

	if (listener) //vklop listenerja
	{

		setBufferSize(num_buff);
		pGrabber->addListener(this, DShowLib::GrabberListener::eFRAMEREADY);
	}

	//pGrabber->addListener(this, DShowLib::GrabberListener::eOVERLAYCALLBACK);
	//pGrabber->getOverlay(ePP_DEVICE)->setEnable(true);

	//smart_ptr<DShowLib::OverlayBitmap> pOverlayBitmap;
	//pOverlayBitmap = pGrabber->getOverlay(ePP_DEVICE);

	if (!pGrabber->isDevValid())
	{
		//pGrabber->saveDeviceStateToFile(".\\device.xml");
		return 0;
	}

	InitImages(width, height, depth);

	GrabImage();

	frameReadyFreq.SetStart();

	cameraOpened++;
	GetGainAbsolute();
	GetExposureAbsolute();
	GetPartialOffsetY();
	GetPartialOffsetX();

	isLive = 1;
	return 1;

}


int ImagingSource::GrabImage()
{
	if (isLive)
	{
		if (pGrabber->isDevValid())
		{
			frameCounter = pSink->getFrameCount();
			while ((prevFrameCounter == frameCounter) && (noFrameCounter < 100000)) //waiting for new frame
			{
				noFrameCounter++;
			}
			m_pBuffer = pSink->getLastAcqMemBuffer();

			if (imageIndex >= image.size())
				imageIndex = 0;

			for (int i = 0; i < bufferSize; i++)
		//		image[imageIndex].data[i] = *(m_pBuffer->getPtr() + i);

			prevFrameCounter = frameCounter;
			noFrameCounter = 0;

			return 1;
		}
	}
	return 0;
}

int ImagingSource::GrabImage(int imageNr)
{
	//shrani sliko na lokacijo imageNr
	int i;
	BYTE* byteData;
	if (isLive)
	{
		if (pGrabber->isDevValid())
		{
			frameCounter = pSink->getFrameCount();
			while ((prevFrameCounter == frameCounter) && (noFrameCounter < 100000)) //waiting for new frame
			{
				frameCounter = pSink->getFrameCount();
				noFrameCounter++;
			}
			m_pBuffer = pSink->getLastAcqMemBuffer();

			byteData = m_pBuffer->getPtr();
			std::copy(&byteData[0], &byteData[0] + bufferSize, &image[imageIndex].buffer[0]);

			//for(i = 0; i < bufferSize; i++)
			//	image[imageNr].buffer[i] = *(m_pBuffer->getPtr() + i);

			prevFrameCounter = frameCounter;
			noFrameCounter = 0;

			return 1;
		}
	}
	return 0;
}

int ImagingSource::MemoryUnlock(int imageNr)
{
	DShowLib::Grabber::tMemBufferPtr tmp_pBuffer;//[num_images];
	int buffNumber;
	int result = 0;

	buffNumber = pCollection->getBufferCount();
	if (imageNr < buffNumber)
	{
		tmp_pBuffer = pCollection->getBuffer(imageNr);
		if (tmp_pBuffer->isLocked())
		{
			tmp_pBuffer->unlock();
			result = 1;
			lockCounter = 0;
			lockedImagesTable[imageNr] = 0;
		}
	}

	return result;
}

void ImagingSource::MemoryUnlockAll()
{
	DShowLib::Grabber::tMemBufferPtr tmp_pBuffer;//[num_images];
	int buffNumber;

	buffNumber = pCollection->getBufferCount();

	for (int i = 0; i<buffNumber; i++)
	{
		tmp_pBuffer = pCollection->getBuffer(i);
		tmp_pBuffer->forceUnlock();

	}

	ClearLockingTable();
}


int ImagingSource::MemoryLock()
{
	int result = 0;		// Vrne stevilko bufferja, ki ga je zaklenil

	m_pBuffer->lock(); //This method increments the lock count of this buffer by 1, so that the grabber cannot overwrite the data in this buffer. 
	result = m_pBuffer->getCollectionPos();

	lockCounter++;
	lockedImagesTable[result] = lockCounter;

	return result;
}

//////////////////////////////////////////////////////////////////////////
/*! The overlayCallback() method draws the number of the current frame. The
frame count is a member of the tsMediaSampleDesc structure that is passed
to overlayCallback() by the Grabber.*/

void ImagingSource::overlayCallback(Grabber& caller, smart_ptr<OverlayBitmap> pBitmap,
	const tsMediaSampleDesc& MediaSampleDesc)
{
	//ce listener ni omogocen potem overlay steje frame;
	//	if(!listener)
	//		frameCounter++;

	//char szText[25];
	if (pBitmap->getEnable() == true) // Draw only, if the overlay bitmap is enabled.
	{
		//sprintf_s( szText,"%04d ", MediaSampleDesc.FrameNumber);
		//pBitmap->drawText( RGB(255,0,0), 0, 0, szText );
	}
}

//////////////////////////////////////////////////////////////////////////
//! The frameReady() method calls the saveImage method to save the image buffer to disk.

void ImagingSource::frameReady(Grabber& caller, smart_ptr<MemBuffer> pBuffer, DWORD currFrame)
{
	//racuna frekvenco
	if (frameReadyFreq.timeCounter >= FPS)
	{
		frameReadyFreq.SetStop();

		frameReadyFreq.CalcFrequency(frameReadyFreq.timeCounter);
		frameReadyFreq.timeCounter = 0;
		frameReadyFreq.SetStart();
	}
	frameReadyFreq.timeCounter++;
	
	testCounter++;
	testCounter = testCounter & 0xffff;
	//m_pBuffer = pBuffer;

	//BYTE* byteData = m_pBuffer->getPtr();
	
	if (isLive)
	{
		BYTE* byteData = pBuffer->getPtr();

		if (enableLive) //kadar imamo ziv prikaz, je ziva slika na sliki 0
		{
			//imageIndex = 0;
			
			std::copy(&byteData[0], &byteData[0] + bufferSize, &image[imageIndex].buffer->data[0]);
			imageReady[imageIndex] = 1;
		}

		else if (trigger)
		{
			//imageIndex++;
			std::copy(&byteData[0], &byteData[0] + bufferSize, &image[imageIndex].buffer->data[0]);

			imageReady[imageIndex] = 1;
		}
		else if (grabImage)
		{
			//dokler je grabimage dvignjen shranjujemo direktno v image[] buffer
			//std::copy(&byteData[0], &byteData[0] + bufferSize, &image[imageIndex].data[0]);
			//imageIndex++;
			imageReady[imageIndex] = 1;

			if (imageIndex >= maxGrabbedImages)
			{
				grabImage = false;
				imageIndex = 0;
			}
		}

		else if (lockImage)
		{
			MemoryLock();
			imageReady[imageIndex] = 1;

			if (imageIndex >= maxGrabbedImages)
			{
				lockImage = false;
				imageIndex = 0;
			}
		}

		frameCounter = currFrame;
		//frameCounter = //frameCounter & 0xffff;
		isFrameReady = true;
		
		if(realTimeProcessing)
		emit frameReadySignal(id, imageIndex);

		if ((FPS > 200) && (FPS < 2000))
		{
			if(frameReadyCounter % 30 == 0)
				emit showImageSignal(imageIndex);
		}
		else if (FPS >= 2000)
		{
			if (frameReadyCounter % 50 == 0)
				emit showImageSignal(imageIndex);
		}
		else
			emit showImageSignal(imageIndex);
		//if (!enableLive)
		imageIndex++;
		//else imageIndex = 0;
		//ce hocemo sliko zakleniti postavimo zastavico lockImage
		if (imageIndex >= image.size())
			imageIndex = 0;


		frameReadyCounter++;

		grabSucceeded = true;
	}
}

void ImagingSource::deviceLost(Grabber& caller)
{
	if (isLive)
	{
		CloseGrabber();
		triggerCounter = 0;
		frameCounter = 0;
	}

}

void ImagingSource::deviceListChanged(Grabber& caller, const DeviceListChangeData& reserved)
{
	//isDeviceListChanged = 1;
	__int64 iSerial;
	QString serial;

	if (!isLive)
	{
		if (Init())
		{
			Start();
			//isDeviceListChanged = 0;
		}
	}
}
//////////////////////////////////////////////////////////////////////////
/*! Initialize the array of bools that is used to memorize, which buffers were processed in
the frameReady() method. The size of the array is specified by the parameter NumBuffers.
It should be equal to the number of buffers in the FrameHandlerSink.
All members of m_BufferWritten are initialized to false.
This means that no buffers have been processed.*/


void	ImagingSource::setBufferSize(unsigned long NumBuffers)
{
	m_BufferWritten.resize(NumBuffers, false);
}

//////////////////////////////////////////////////////////////////////////
//! The image passed by the MemBuffer pointer is saved to a BMP file.

void ImagingSource::saveImage(smart_ptr<MemBuffer> pBuffer, DWORD currFrame)
{
	char filename[MAX_PATH];

	if (currFrame < m_BufferWritten.size())
	{
		sprintf_s(filename, ".\\image%02i.bmp", currFrame);

		saveToFileBMP(*pBuffer, filename);

		m_BufferWritten.at(currFrame) = true;
	}
}

bool ImagingSource::SetWhiteBalanceAbsolute(long red, long green, long blue)
{
	bool bOK = false;
	DShowLib::tIVCDRangePropertyPtr pBalanceRange;
	DShowLib::tIVCDSwitchPropertyPtr pBalanceAuto;

	pBalanceRange = NULL;
	pBalanceAuto = NULL;


	tIVCDPropertyItemsPtr pItems = pGrabber->getAvailableVCDProperties();
	if (pItems != 0)
	{
		// Try to find the exposure item. 
		tIVCDPropertyItemPtr pBalanceItem = pItems->findItem(VCDID_WhiteBalance);
		if (pBalanceItem != 0)
		{
			// Try to find the value and auto elements 
			tIVCDPropertyElementPtr pBalanceAutoElement = pBalanceItem->findElement(VCDElement_Auto);
			tIVCDPropertyElementPtr pBalanceBlueValueElement = pBalanceItem->findElement(VCDElement_WhiteBalanceBlue);
			tIVCDPropertyElementPtr pBalanceGreenValueElement = pBalanceItem->findElement(VCDElement_WhiteBalanceGreen);
			tIVCDPropertyElementPtr pBalanceRedValueElement = pBalanceItem->findElement(VCDElement_WhiteBalanceRed);


			// If an auto element exists, try to acquire a switch interface 
			// sklopimo avto nastavitev
			if (pBalanceAutoElement != 0)
			{
				pBalanceAutoElement->getInterfacePtr(pBalanceAuto);
				pBalanceAuto->setSwitch(false); // Disable auto, otherwise we can not set exposure.
			}


			// If a value element exists, try to acquire a range interface 
			// nastavimo modro
			if (pBalanceBlueValueElement != 0)
			{
				pBalanceBlueValueElement->getInterfacePtr(pBalanceRange);

				double min = pBalanceRange->getRangeMin();
				double max = pBalanceRange->getRangeMax();

				if (blue < min)
					blue = (long)min;
				else if (blue > max)
					blue = (long)max;

				// Here we set the the exposure value.
				pBalanceRange->setValue(blue);
				bOK = true;
			}

			// nastavimo zeleno
			if (pBalanceGreenValueElement != 0)
			{
				pBalanceGreenValueElement->getInterfacePtr(pBalanceRange);

				double min = pBalanceRange->getRangeMin();
				double max = pBalanceRange->getRangeMax();

				if (green < min)
					green = (long)min;
				else if (green > max)
					green = (long)max;

				// Here we set the the exposure value.
				pBalanceRange->setValue(green);
				bOK = true;
			}

			// nastavimo rdeco
			if (pBalanceRedValueElement != 0)
			{
				pBalanceRedValueElement->getInterfacePtr(pBalanceRange);

				double min = pBalanceRange->getRangeMin();
				double max = pBalanceRange->getRangeMax();

				if (red < min)
					red = (long)min;
				else if (red > max)
					red = (long)max;

				// Here we set the the exposure value.
				pBalanceRange->setValue(red);
				bOK = true;
			}
		}
	}
	return bOK;
}


bool ImagingSource::SetExposureAbsolute(double dExposure)
{
	bool bOK = false;
	DShowLib::tIVCDAbsoluteValuePropertyPtr pExposureRange;
	DShowLib::tIVCDSwitchPropertyPtr pExposureAuto;

	pExposureRange = NULL;
	pExposureAuto = NULL;

	if (currentExposure != dExposure)
	{
		tIVCDPropertyItemsPtr pItems = pGrabber->getAvailableVCDProperties();
		if (pItems != 0)
		{
			// Try to find the exposure item. 
			tIVCDPropertyItemPtr pExposureItem = pItems->findItem(VCDID_Exposure);
			if (pExposureItem != 0)
			{
				// Try to find the value and auto elements 
				tIVCDPropertyElementPtr pExposureValueElement = pExposureItem->findElement(VCDElement_Value);
				tIVCDPropertyElementPtr pExposureAutoElement = pExposureItem->findElement(VCDElement_Auto);

				// If an auto element exists, try to acquire a switch interface 
				if (pExposureAutoElement != 0)
				{
					pExposureAutoElement->getInterfacePtr(pExposureAuto);
					pExposureAuto->setSwitch(false); // Disable auto, otherwise we can not set exposure.
				}


				// If a value element exists, try to acquire a range interface 
				if (pExposureValueElement != 0)
				{
					pExposureValueElement->getInterfacePtr(pExposureRange);

					//double min = pExposureRange->getRangeMin();
					//double max = pExposureRange->getRangeMax();

					if (dExposure < minExpo)
					{
						dExposure = minExpo;
					}
					else if (dExposure > maxExpo)
					{
						dExposure = maxExpo;
					}

					// Here we set the the exposure value.
					pExposureRange->setValue(dExposure/1000);
					currentExposure = dExposure;
					bOK = true;
				}
			}
		}
	}
	return bOK;
}

double ImagingSource::GetExposureAbsolute()
{
	DShowLib::tIVCDAbsoluteValuePropertyPtr pExposureRange;
	DShowLib::tIVCDSwitchPropertyPtr pExposureAuto;

	pExposureRange = NULL;
	pExposureAuto = NULL;

	tIVCDPropertyItemsPtr pItems = pGrabber->getAvailableVCDProperties();
	if (pItems != 0)
	{
		// Try to find the exposure item. 
		tIVCDPropertyItemPtr pExposureItem = pItems->findItem(VCDID_Exposure);
		if (pExposureItem != 0)
		{
			// Try to find the value and auto elements 
			tIVCDPropertyElementPtr pExposureValueElement = pExposureItem->findElement(VCDElement_Value);
			tIVCDPropertyElementPtr pExposureAutoElement = pExposureItem->findElement(VCDElement_Auto);

			// If an auto element exists, try to acquire a switch interface 
			if (pExposureAutoElement != 0)
			{
				pExposureAutoElement->getInterfacePtr(pExposureAuto);
				pExposureAuto->setSwitch(false); // Disable auto, otherwise we can not set exposure.
			}


			// If a value element exists, try to acquire a range interface 
			if (pExposureValueElement != 0)
			{
				pExposureValueElement->getInterfacePtr(pExposureRange);

				double min = pExposureRange->getRangeMin();
				double max = pExposureRange->getRangeMax();
				minExpo = min*1000; //da fobimo mili sekunde zaradi prikaza
				maxExpo = max*1000;

				// Here we set the the exposure value.
				currentExposure = pExposureRange->getValue()*1000;
			}
		}
	}
	return currentExposure;
}

bool ImagingSource::SetGainAbsolute(double dGain)
{
	bool bOK = false;

	DShowLib::tIVCDRangePropertyPtr pGainRange;
	DShowLib::tIVCDSwitchPropertyPtr pGainAuto;

	pGainRange = NULL;
	pGainAuto = NULL;
	if (dGain != currentGain)
	{
		tIVCDPropertyItemsPtr pItems = pGrabber->getAvailableVCDProperties();
		if (pItems != 0)
		{
			// Try to find the exposure item. 
			tIVCDAbsoluteValuePropertyPtr pAbsval = 0;

			pItems->findInterfacePtr(VCDID_Gain, VCDElement_Value, pAbsval);//
			tIVCDPropertyItemPtr pGainItem = pItems->findItem(VCDID_Gain);
			if (pGainItem != 0)
			{

				// Try to find the value and auto elements 
				tIVCDPropertyElementPtr pGainValueElement = pGainItem->findElement(VCDElement_Value);
				tIVCDPropertyElementPtr pGainAutoElement = pGainItem->findElement(VCDElement_Auto);

				// If an auto element exists, try to acquire a switch interface 
				if (pGainAutoElement != 0)
				{
					pGainAutoElement->getInterfacePtr(pGainAuto);
					pGainAuto->setSwitch(false); // Disable auto, otherwise we can not set exposure.
				}


				// If a value element exists, try to acquire a range interface 
				if (pGainValueElement != 0)
				{
					pGainValueElement->getInterfacePtr(pGainRange);

					int min = pGainRange->getRangeMin();
					double max = pGainRange->getRangeMax();

					if (dGain < min)
					{
						dGain = min;
					}
					else if (dGain > max)
					{
						dGain = max;
					}
				
					// Here we set the the exposure value.
					pGainRange->setValue((long)dGain);

					currentGain = dGain;
					bOK = true;
				}
			}
		}
	}
	return bOK;
}


int ImagingSource::GetGainAbsolute()
{
	DShowLib::tIVCDRangePropertyPtr pGainRange;
	DShowLib::tIVCDSwitchPropertyPtr pGainAuto;

	pGainRange = NULL;
	pGainAuto = NULL;

	tIVCDPropertyItemsPtr pItems = pGrabber->getAvailableVCDProperties();
	if (pItems != 0)
	{
		// Try to find the exposure item. 
		tIVCDAbsoluteValuePropertyPtr pAbsval = 0;

		pItems->findInterfacePtr(VCDID_Gain, VCDElement_Value, pAbsval);//
		tIVCDPropertyItemPtr pGainItem = pItems->findItem(VCDID_Gain);
		if (pGainItem != 0)
		{

			// Try to find the value and auto elements 
			tIVCDPropertyElementPtr pGainValueElement = pGainItem->findElement(VCDElement_Value);
			tIVCDPropertyElementPtr pGainAutoElement = pGainItem->findElement(VCDElement_Auto);

			// If an auto element exists, try to acquire a switch interface 
			if (pGainAutoElement != 0)
			{
				pGainAutoElement->getInterfacePtr(pGainAuto);
				pGainAuto->setSwitch(false); // Disable auto, otherwise we can not set exposure.
			}


			// If a value element exists, try to acquire a range interface 
			if (pGainValueElement != 0)
			{
				pGainValueElement->getInterfacePtr(pGainRange);

				minGain = pGainRange->getRangeMin();
				maxGain = pGainRange->getRangeMax();

				currentGain = pGainRange->getValue();

			}
		}
	}
	return currentGain;
}




void ImagingSource::CloseGrabber(void)
{
	if (isLive == 1)
	{
		if (pGrabber->isListenerRegistered(this, DShowLib::GrabberListener::eFRAMEREADY))
		{
			pGrabber->removeListener(this, DShowLib::GrabberListener::eFRAMEREADY);
			// Second, unregister it for the overlay callback event.

			// Wait until all remove requests have been processed
			while (pGrabber->isListenerRegistered(this, DShowLib::GrabberListener::eFRAMEREADY))
			{
				Sleep(0); // Wait
			}
			// Now, the application can be sure that the CListener methods
			// are no longer called by the Grabber. It is now safe to delete the
			// CListener object.
		}

		if (pGrabber->isListenerRegistered(this, DShowLib::GrabberListener::eOVERLAYCALLBACK))
		{
			pGrabber->removeListener(this, DShowLib::GrabberListener::eOVERLAYCALLBACK);
			// Second, unregister it for the overlay callback event.

			// Wait until all remove requests have been processed
			while (pGrabber->isListenerRegistered(this, DShowLib::GrabberListener::eOVERLAYCALLBACK))
			{
				Sleep(0); // Wait
			}
			// Now, the application can be sure that the CListener methods
			// are no longer called by the Grabber. It is now safe to delete the
			// CListener object.
		}



		pGrabber->stopLive();            // Stop the grabber
		if (pGrabber->isDevOpen())
			pGrabber->closeDev();

		isLive = false;

	}
}


bool ImagingSource::OnSettingsImage()
{
	//pGrabber->showVCDPropertyPage( m_wndView, pGrabber->getDev().getName());

	// Save the new property settings of the video capture device in the registry, 
	// so are restored automatically at next program start.
	//CString cGrabberState = pGrabber->saveDeviceStateW().c_str();

	// Get the last used video capture device from the registry.
	//AfxGetApp()->WriteProfileString( REG_SECTION_DEVICE, REG_DEVICE, cGrabberState );

	return SaveReferenceSettings();
}

bool ImagingSource::SaveReferenceSettings()
{
	QString filePath;
	QStringList values;
	QVariant var;

	filePath = QDir::currentPath() + QString("/%1/nastavitveKamer/ImagingSource_%2.ini").arg(REF_FOLDER_NAME).arg(serialNumber);
	QSettings settings(filePath, QSettings::IniFormat);


	//inputs
	for (int i = 0; i < 1; i++)
	{
		values = settings.value(QString("EXPO%1").arg(i)).toStringList();

		if (values.size() > 0)
		{
			settings.setValue(QString("EXPO%1").arg(i), QString("%1").arg(currentExposure));
			settings.sync();
			values.clear();
		}

		values = settings.value(QString("GAIN%1").arg(i)).toStringList();
		if (values.size() > 0)
		{
			settings.setValue(QString("GAIN%1").arg(i), QString("%1").arg(currentGain));
			settings.sync();
			values.clear();
		}

		values = settings.value(QString("OFFSETX%1").arg(i)).toStringList();
		if (values.size() > 0)
		{
			settings.setValue(QString("OFFSETX%1").arg(i), QString("%1").arg(offsetX));
			settings.sync();
			values.clear();
		}

		values = settings.value(QString("OFFSETY%1").arg(i)).toStringList();
		if (values.size() > 0)
		{
			settings.setValue(QString("OFFSETY%1").arg(i), QString("%1").arg(offsetY));
			settings.sync();
			values.clear();
		}

	}
	/*for (int i = 0; i < num_images; i++)
	{
		values = settings.value(QString("GAIN%1").arg(i)).toStringList();

		if (values.size() > 0)
		{
			settings.setValue(QString("GAIN%1").arg(i), QString("%1").arg(currentGain));

			settings.sync();
		}

	}*/

	return 0;

}

bool ImagingSource::LoadReferenceSettings()
{
	QString filePath;
	QStringList values;
	QVariant var;

	filePath = QDir::currentPath() + QString("/%1/nastavitveKamer/ImagingSource_%2.ini").arg(REF_FOLDER_NAME).arg(serialNumber);
	QSettings settings(filePath, QSettings::IniFormat);


	//inputs
	for (int i = 0; i < 1; i++)
	{
		values = settings.value(QString("EXPO%1").arg(i)).toStringList();

		if (values.size() > 0)
		{
			exposure.push_back(values[0].toFloat());
			values.clear();
		}
		else
		{
			exposure.push_back(0);
			settings.setValue(QString("EXPO%1").arg(i), QString("0"));
			settings.sync();
		}

		values = settings.value(QString("GAIN%1").arg(i)).toStringList();
		if (values.size() > 0)
		{
			gain.push_back(values[0].toInt());
			values.clear();
		}
		else
		{
			gain.push_back(0);
			settings.setValue(QString("GAIN%1").arg(i), QString("0"));
			settings.sync();
		}

		values = settings.value(QString("OFFSETX%1").arg(i)).toStringList();
		if (values.size() > 0)
		{
			savedOffsetX.push_back(values[0].toInt());
			values.clear();
		}
		else
		{
			savedOffsetX.push_back(0);
			settings.setValue(QString("OFFSETX%1").arg(i), QString("0"));
			settings.sync();
		}

		values = settings.value(QString("OFFSETY%1").arg(i)).toStringList();
		if (values.size() > 0)
		{
			savedOffsetY.push_back(values[0].toInt());
			values.clear();
		}
		else
		{
			savedOffsetY.push_back(0);
			settings.setValue(QString("OFFSETY%1").arg(i), QString("0"));
			settings.sync();
		}


	}
	/*for (int i = 0; i < num_images; i++)
	{
		values = settings.value(QString("GAIN%1").arg(i)).toStringList();

		if (values.size() > 0)
		{
			gain.push_back(values[0].toInt());
			values.clear();
		}
		else
		{
			gain.push_back(0);
			settings.setValue(QString("GAIN%1").arg(i), QString("0"));
			settings.sync();
		}


	}*/
	

	return 0;
}




/*bool ImagingSource::IsDeviceValid()
{
	bool res;

	if (isLive)
	{
		if (trigger)
		{
			if (abs(triggerCounter - (int)frameCounter) > 2)
			{
				if (!triggerTest) //ce smo v testu (iz lpt dialoga ali kjerkoli drugje trgamo kamero in ne pristevamo triggerCounterja
				{
					CloseGrabber();
					triggerCounter = 0;
					frameCounter = 0;

					if (!isLive) //probamo takoj povezati
					{
						if (Init())
						{
							Start();
							//isDeviceListChanged = 0;
						}
					}

					return false;
				}
				else
				{
					triggerCounter = frameCounter;
				}

			}
		}
		else
		{

			/*if (frameReadyFreq.frequency == 0) //frekvenca v frameready ne sme biti 0
			{
			CloseGrabber();
			return false;
			}
			frameReadyFreq.frequency = 0.0;
		}
	}

	return true;
}
*/

bool ImagingSource::EnableLive() //funkcija omogoci live prikaz slike
{
	bool res;
	if (!enableLive) //vklopimo samo ce je enable live ni aktiven
	{
		pGrabber->stopLive();
		res = pGrabber->setExternalTrigger(false);
		enableLive = true;

		pGrabber->startLive(false);
		if (!listener) //vklop listenerja, �e ni bil �e omogo�en
		{
			setBufferSize(num_buff);
			pGrabber->addListener(this, GrabberListener::eFRAMEREADY);
		}
	}
	return true;
}

bool ImagingSource::DisableLive() //funkcija onemogoci live prikaz slike
{
	bool res;

	if (enableLive) //izklopimo samo ce je enable live aktiven
	{
		pGrabber->stopLive();
		res = pGrabber->setExternalTrigger(trigger);
		enableLive = false;
		pGrabber->startLive(false);

		if (!listener) //izklop listenerja, �e ni bil prej onemogo�en
		{
			pGrabber->removeListener(this, GrabberListener::eFRAMEREADY);
		}
	}
	return true;
}

bool ImagingSource::SetPartialOffsetX(int xOff)
{
	bool bOK = false;
	bool pAutoCenterItem = false;
	bool pOffsetItem = false;
	DShowLib::tIVCDRangePropertyPtr pOffRange;
	DShowLib::tIVCDSwitchPropertyPtr pOffAuto;	// auto center

	pOffRange = NULL;
	pOffAuto = NULL;	// auto center

	tIVCDPropertyItemsPtr pItems = pGrabber->getAvailableVCDProperties();
	if (pItems != 0)
	{
		// Ali je auto center vklopljen 
		if (pItems->findInterfacePtr(VCDID_PartialScanOffset, VCDElement_PartialScanAutoCenter, pOffAuto) != 0)		// auto center
			pAutoCenterItem = true;

		if (pItems->findInterfacePtr(VCDID_PartialScanOffset, VCDElement_PartialScanOffsetX, pOffRange) != 0)		// x offset
			pOffsetItem = true;

		if (pAutoCenterItem != 0)		// item exists
		{
			if (pOffAuto != 0)			// auto center
				pOffAuto->setSwitch(false);		// Sklopimo auto center, ce je vklopljen

			// If a value element exists, try to acquire a range interface 
			if (pOffsetItem != 0)
			{
				int min = pOffRange->getRangeMin();
				int max = pOffRange->getRangeMax();

				if (xOff < min)
					xOff = min;
				else if (xOff > max)
					xOff = max;

				offsetX = xOff;
				// Here we set the the exposure value.
				pOffRange->setValue((long)xOff);
				bOK = true;
			}
		}
	}
	return bOK;
}

int ImagingSource::GetPartialOffsetX()
{
	int xOff = 0;
	bool pAutoCenterItem = false;
	bool pOffsetItem = false;
	DShowLib::tIVCDRangePropertyPtr pOffRange;
	DShowLib::tIVCDSwitchPropertyPtr pOffAuto;	// auto center

	pOffRange = NULL;
	pOffAuto = NULL;	// auto center

	tIVCDPropertyItemsPtr pItems = pGrabber->getAvailableVCDProperties();
	if (pItems != 0)
	{
		// Ali je auto center vklopljen
		if (pItems->findInterfacePtr(VCDID_PartialScanOffset, VCDElement_PartialScanAutoCenter, pOffAuto) != 0)		// auto center
			pAutoCenterItem = true;

		if (pItems->findInterfacePtr(VCDID_PartialScanOffset, VCDElement_PartialScanOffsetX, pOffRange) != 0)		// x offset
			pOffsetItem = true;

		if (pAutoCenterItem != 0)		// item exists
		{
			if (pOffAuto != 0)			// auto center
				pOffAuto->setSwitch(false);		// Sklopimo auto center, �e je vklopljen

			// If a value element exists, try to acquire a range interface
			if (pOffsetItem != 0)
			{
				// Here we set the the exposure value.
				minOffsetX = pOffRange->getRangeMin();
				maxOffsetX = pOffRange->getRangeMax();
				offsetX = pOffRange->getValue();
			}
		}
	}
	return offsetX;
}

bool ImagingSource::SetPartialOffsetY(int yOff)
{
	bool bOK = false;
	bool pAutoCenterItem = false;
	bool pOffsetItem = false;
	DShowLib::tIVCDRangePropertyPtr pOffRange;
	DShowLib::tIVCDSwitchPropertyPtr pOffAuto;	// auto center

	pOffRange = NULL;
	pOffAuto = NULL;	// auto center

	tIVCDPropertyItemsPtr pItems = pGrabber->getAvailableVCDProperties();
	if (pItems != 0)
	{
		// Ali je auto center vklopljen 
		if (pItems->findInterfacePtr(VCDID_PartialScanOffset, VCDElement_PartialScanAutoCenter, pOffAuto) != 0)		// auto center
			pAutoCenterItem = true;

		if (pItems->findInterfacePtr(VCDID_PartialScanOffset, VCDElement_PartialScanOffsetY, pOffRange) != 0)		// x offset
			pOffsetItem = true;

		if (pAutoCenterItem != 0)		// item exists
		{
			if (pOffAuto != 0)			// auto center
				pOffAuto->setSwitch(false);		// Sklopimo auto center, ce je vklopljen

			// If a value element exists, try to acquire a range interface 
			if (pOffsetItem != 0)
			{
				int min = pOffRange->getRangeMin();
				int max = pOffRange->getRangeMax();

				if (yOff < min)
					yOff = min;
				else if (yOff > max)
					yOff = max;

				offsetY = yOff;

				// Here we set the the exposure value.
				pOffRange->setValue((long)yOff);
				bOK = true;
			}
		}
	}
	return bOK;
}

int ImagingSource::GetPartialOffsetY()
{
	int yOff = 0;
	bool pAutoCenterItem = false;
	bool pOffsetItem = false;
	DShowLib::tIVCDRangePropertyPtr pOffRange;
	DShowLib::tIVCDSwitchPropertyPtr pOffAuto;	// auto center

	pOffRange = NULL;
	pOffAuto = NULL;	// auto center

	tIVCDPropertyItemsPtr pItems = pGrabber->getAvailableVCDProperties();
	if (pItems != 0)
	{
		// Ali je auto center vklopljen
		if (pItems->findInterfacePtr(VCDID_PartialScanOffset, VCDElement_PartialScanAutoCenter, pOffAuto) != 0)		// auto center
			pAutoCenterItem = true;

		if (pItems->findInterfacePtr(VCDID_PartialScanOffset, VCDElement_PartialScanOffsetY, pOffRange) != 0)		// x offset
			pOffsetItem = true;

		if (pAutoCenterItem != 0)		// item exists
		{
			if (pOffAuto != 0)			// auto center
				pOffAuto->setSwitch(false);		// Sklopimo auto center, �e je vklopljen

			// If a value element exists, try to acquire a range interface
			if (pOffsetItem != 0)
			{
				// Here we set the the exposure value.
				minOffsetY = pOffRange->getRangeMin();
				maxOffsetY = pOffRange->getRangeMax();
				offsetY = pOffRange->getValue();
			}
		}
	}
	return offsetY;
}

int ImagingSource::EnableStrobe(int polarity)
{
	DShowLib::tIVCDPropertyItemsPtr		ITEM;
		////dodano za test outputa////
		ITEM = pGrabber->getAvailableVCDProperties();

		DShowLib::tIVCDSwitchPropertyPtr pBalanceAuto;
		DShowLib::tIVCDButtonPropertyPtr button;
		pBalanceAuto = NULL;
		DShowLib::IVCDPropertyElements **elementi;

		tIVCDPropertyItemPtr pBalanceItem = ITEM->findItem(VCDID_Strobe);
		if (pBalanceItem != 0)
		{
			//pBalanceItem->get_Elements(elementi);

			VARIANT_BOOL bla;

			tIVCDPropertyElementPtr pStrobeDelay = pBalanceItem->findElement(VCDElement_Value);
			if (pStrobeDelay != 0)
			{
				pStrobeDelay->getInterfacePtr(pBalanceAuto);
				pBalanceAuto->setSwitch(true);

			}

			tIVCDPropertyElementPtr pPolar = pBalanceItem->findElement(VCDElement_StrobePolarity);


			if (pPolar != 0)
			{
				pPolar->getInterfacePtr(pBalanceAuto);
				//	pBalanceAuto->setSwitch(false); // Disable auto, otherwise we can not set exposure.
				pBalanceAuto->setSwitch(polarity);

			}

			tIVCDPropertyElementPtr TriggerPolar = pBalanceItem->findElement(VCDElement_TriggerPolarity);


			if (TriggerPolar != 0)
			{
				pPolar->getInterfacePtr(pBalanceAuto);
				//	pBalanceAuto->setSwitch(false); // Disable auto, otherwise we can not set exposure.
				//pBalanceAuto->setSwitch(triggerPolarity);

			}


		}




		return 0;

}

int ImagingSource::DisableStrobe()
{
	DShowLib::tIVCDPropertyItemsPtr		ITEM;
	////dodano za test outputa////
	ITEM = pGrabber->getAvailableVCDProperties();

	DShowLib::tIVCDSwitchPropertyPtr pBalanceAuto;
	DShowLib::tIVCDButtonPropertyPtr button;
	pBalanceAuto = NULL;
	DShowLib::IVCDPropertyElements **elementi;

	tIVCDPropertyItemPtr pBalanceItem = ITEM->findItem(VCDID_Strobe);
	if (pBalanceItem != 0)
	{
		//pBalanceItem->get_Elements(elementi);

		VARIANT_BOOL bla;

		tIVCDPropertyElementPtr pStrobeDelay = pBalanceItem->findElement(VCDElement_Value);
		if (pStrobeDelay != 0)
		{
			pStrobeDelay->getInterfacePtr(pBalanceAuto);
			pBalanceAuto->setSwitch(false);

		}

		tIVCDPropertyElementPtr pPolar = pBalanceItem->findElement(VCDElement_StrobePolarity);


		


	}




	return 0;
}

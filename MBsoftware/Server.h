#pragma once

#include <QtNetwork/qtcpsocket.h>
#include <QtNetwork/qtcpserver.h>
#include <QtNetwork/qhostaddress.h>

using namespace std;

class Server :
	public QTcpServer
{
	Q_OBJECT

public:
	Server();
	~Server();

public:
	static vector<QTcpSocket*>				tcpConnections;
	static QTimer*							connectionTimer;
	QString address;
	QString recievedData;
	quint16 port;

	int numOfConnections;
	//�as �akanja na povezavo v milisekundah
	int waitTime;

private:
	QHostAddress addr;

public:
	void StartListening();
	//�as �akanja na povezavo v milisekundah

private slots:
	void onNewConnection();
	void onConnectionTimeout();
	void onServerReadyRead();

signals:
	void connectionTimeout();
	void dataReady();
};

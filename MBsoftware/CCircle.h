﻿#pragma once
#include "CPointFloat.h"
#include  "CLine.h"

// CCircle
using namespace std;

class CCircle : public CPointFloat
{
	

public:
	CCircle();
	CCircle(CPointFloat center, float radius);
	CCircle(CPointFloat p1, CPointFloat p2, CPointFloat p3);
	CCircle(const CCircle& circle);
	

	virtual ~CCircle();
	std::vector< CPointFloat> circlePoints;
	std::vector< float> centerDistances;
	std::vector< float> averageIntensity;
	std::vector< float> averageIntensityRed;
	std::vector< float> averageIntensityGreen;
	std::vector< float> averageIntensityBlue;


public:
	int count;
	int type;
	float radius;
	float perimeter; //obseg
	float area;		//ploscina
	float minRadius; //searches for deviations on the circle - closes to center
	float maxRadius; //searches for deviations on the circle - 
	float averageRadius;

	float radiousDeviation;  //Dodal Martin: standardna deviacija radija



/*	float averageIntensity;
	float averageIntensityBlue;
	float averageIntensityRed;
	float averageIntensityGreen;*/
	int gravityIndex1;
	int gravityIndex2;

public:
	CCircle operator=(CCircle circle);
	bool operator ==(CCircle circle);
	bool operator !=(CCircle circle);
	void SetCircle(CPointFloat p1, CPointFloat p2, CPointFloat p3);
	void SetCircle(std::vector<CPointFloat> points);
	void SetCenter(QPoint center);
	void SetCenter(CPointFloat center);

	void SetRadius(float r);
	void ComputeArea();
	void ComputePerimeter();
	CPointFloat GetCenter() const;
	float GetX() const;
	float GetY() const;
	float GetRadius() const;
	float GetArea() const;
	float GetPerimeter() const;
	void GetDistancesAroundRadius(unsigned char *memoryBuffer, int radius, int width, int height, int depth, int filterBand, int F, int whiteThreshold, int blackThreshold, bool backWard);
	//void GetDistancesAroundRadius(CMemoryBuffer &memoryBuffer, int radius, int filterBand, int F, int whiteThreshold, int blackThreshold, bool backWard);	
	//float GetDistancesAroundRadius(CMemoryBuffer &memoryBuffer, int radius, int filterBand, int degree, int F, int whiteThreshold, int blackThreshold, bool backWard);	
	//float GetDistancesAroundRadiusGreen(CMemoryBuffer &memoryBuffer, int radius, int filterBand, int degree, int F, int whiteThreshold, int blackThreshold, bool backWard);
	//void GetAverageIntensity(/*CDC* pDC*/ CMemoryBuffer &memoryBuffer, int r1, int r2, int F);
	//RGBQUAD GetAverageIntensity(CDC* pDC, bool draw, CMemoryBuffer &memoryBuffer, int r1, int r2, int F, int degree);



	void Presecisce(CCircle c2, CPointFloat T[2]);

	//Napisal Martin
	//Izračuna krožnico skozi vektor točk. Zavrže trojice točk z radijem pod Rmin ali nad Rmax.
	bool SetCircleNpoints(std::vector<CPointFloat> points, float Rmin, float Rmax);

	//Napisal Martin:
	//Za točen izračun krožnice: izvede SetCircleNpoints, odstrani točke ki odstopajo od krožnice za več kot maxRerror, nato ponovi SetCircleNpoints
	//nastavi badPointsShare na delež točk, ki odstopajo od krožnice za več kot maxRerror
	//stDevRall ter stDevRgood hranita standardno deviacijo radija za vse točke in za dobre točke (napaka R pod maxRerror)
	bool SetCircleNpointsExact(std::vector<CPointFloat> points, float Rmin, float Rmax, float maxRerror, float* badPointsShare, float* stDevRall, float* stDevRgood);

	QGraphicsEllipseItem* DrawCircle(QPen pen, QBrush brush, int crossSize);
	
};



#pragma once
#include <QtNetwork/qtcpsocket.h>
#include <QtNetwork/qtcpserver.h>
#include <QtNetwork/qhostaddress.h>
#include <QObject>
#include "ui_Tcp.h"
#define REQUEST_SIZE 1024

class TCP : public QWidget
{
	Q_OBJECT

public:
	TCP(QString ipAddress, int port, bool isServer);
	~TCP();
private: 
	Ui::TcpForm ui;

public:
	static vector<QTcpSocket*>				tcpConnections;
	static QTimer*							connectionTimer;
	QString address;
	QString recievedData;
	quint16 port;
	QTcpServer*								server;
	QTcpSocket*								client;
	bool									clientConnected;
	bool									isServer;
	char									dataBuf[REQUEST_SIZE];
	int										dataBufCounter;
	int										returnIntArray[REQUEST_SIZE];
	char									returnCharArray[REQUEST_SIZE];



	int numOfConnections;
	//�as �akanja na povezavo v milisekundah
	int waitTime;

private:
	QHostAddress addr;

public:
	void OnShowDialog();
	void StartServer();
	void StartClient();
	void WriteData(QByteArray text);
	void Disconnect();

	//�as �akanja na povezavo v milisekundah

private slots:
	void OnNewConnection();
	void OnConnectionTimeout();
	void OnServerReadyRead();
	void OnClientReadyRead();
	void OnClientConnected();
	void OnClientDisconnected();
	void OnClickedSend();
	void OnAppendText(QString text);


signals:
	void connectionTimeout();
	void dataReady(int parseCounter);
	void appendText(QString text);
	
};

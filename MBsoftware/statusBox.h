#pragma once

#include <QWidget>
#include "ui_statusBox.h"

class StatusBox : public QWidget
{
	Q_OBJECT

public:
	StatusBox(QWidget *parent = Q_NULLPTR);
	~StatusBox();
	 void ShowDialog();
	 void AddItem(int index, QString text);

private:
	Ui::StatusBox ui;


};

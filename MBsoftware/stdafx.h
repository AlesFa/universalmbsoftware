﻿/********************************************************************************
** Vsebuje knjižnice, ki jih uporablja vecina razredov v programu!
**
** Klicana je v .cpp datotekah in jo vedno vnesemo kot prvo z #include ukazom!
**
** Pospeši prevajanje celotnega programa!
********************************************************************************/

#pragma once

#include "windows.h"
#pragma comment(lib, "ws2_32.lib")

//Qt knjižnica
#include <QtWidgets>
#include <QWidget>
#include <QGraphicsSceneMouseEvent>
#include "math.h"
#include"qtconcurrentrun.h"
//OpenCV knjižnica
#include "opencv/cv.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/videoio/videoio.hpp"
#include "CPointFloat.h"
#include "CCircle.h"

//matematične vrednosti
#define MIN_VAL 0.0000001

#define LOGIN_TIMEOUT		5 //cas v minutah za avto logout
#define	TAB_BUTTTON_SIZE	80
#define TAB_ICON_SIZE		60
#define REF_FOLDER_NAME		"Reference"  //direktorij kjer shranjujemo nastavitve programa
#define NR_STATIONS			1
#define NR_TYPES			1
#define MAX_CAM				8			//max stevilo kamer uporabljano za izris cameraStatus Widgeta
#define MAX_PROP			20			//stevilo parametrov za vsako funkcijo 
#define ST_IMAGES			500			//projekt palcke barva stevilo slik za vsako kamero
#define HISTORY_SIZE		100		//velikost historys oken. Zgodovina s tabelo in slikami za History_size st kosov.